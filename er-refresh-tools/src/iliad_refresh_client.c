#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <unistd.h>
#include <errno.h>
#include <ctype.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <sys/un.h>

#include "iliad_refresh.h"

int main (void)
{
  int client = 0, ret_val = 0;
  struct sockaddr_un remote;
  char buffer = IR_SETUP_APPENV;

  printf(PACKAGE_STRING " - client\n");
  printf("This is free software released under the GNU GPL. See COPYING for details.\n");

#if (defined HAVE_LIBERDM) && (defined HAVE_LIBERIPC)
  printf("liberdm and liberipc support compiled in.\n");
#endif

  client = socket(AF_UNIX, SOCK_STREAM, 0);
  if (client == -1)
  {
    perror("error creating socket");
    exit(1);
  }

  remote.sun_family = AF_UNIX;
  strcpy(remote.sun_path, ILIAD_REFRESH);

  ret_val = connect(client, (struct sockaddr *) &remote,
                    strlen(remote.sun_path) + sizeof(remote.sun_family));
  if (ret_val == -1)
  {
    perror("Connection failed. Is daemon running?");
    close(client);
    exit(1);
  }

  ret_val = write(client, &buffer, 1);
  if (ret_val < 0)
    perror("Writing to daemon failed");
  else
    printf("Screen refresh initiated\n");

  close(client);
}
