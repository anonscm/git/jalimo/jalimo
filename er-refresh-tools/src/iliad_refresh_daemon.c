#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <stdio.h>
#include <stdlib.h>

#include <sys/socket.h>
#include <sys/select.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/un.h>

#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <ctype.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#ifndef FIONREAD
#include <sys/filio.h>
#endif

#include "iliad_refresh.h"

long default_timeout;

/** ER refresh tools - daemon.
 *
 * Creates a listening socket on Unix domain socket /tmp/ILIAD_REFRESH.
 *
 * If a connection is established on that socket the daemon waits for data with
 * a timeout of an hour (if there was no previous request).
 *
 * Depending on the read data an initialization is performed (read 1) and a
 * screen refresh is scheduled (read 1 or else) (The screen refresh will be
 * performed after a default timeout expired).
 *
 * After scheduling the screen refresh another read attempt is done with a
 * lower default timeout (usually ~100 ms). If that timeout expires without any
 * data arriving the screen refresh is performed. In case new data arrives the
 * screen refresh is postponed for another read cycle.
 * This way consecutive screen refreshes are merged into one.
 *
 */
int main (void)
{
  int server_socket = 0;
  struct sockaddr_un local;

  int fd = 0, ret_val = 0;
  char buffer[BUFFER_SIZE];
  int pending = 0;

  printf(PACKAGE_STRING " - daemon\n");
  printf("This is free software released under the GNU GPL. See COPYING for details.\n");

#if (defined HAVE_LIBERDM) && (defined HAVE_LIBERIPC)
  printf("liberdm and liberipc support compiled in.\n");
#endif

  server_socket = socket (AF_UNIX, SOCK_STREAM, 0);
  if (server_socket == -1) {
    perror("Error creating socket");
    exit (1);
  }
  fcntl(server_socket, F_SETFL, O_NONBLOCK);

  local.sun_family = AF_UNIX;
  strcpy(local.sun_path, ILIAD_REFRESH);
  unlink (local.sun_path);

  ret_val = bind (server_socket,
                  (struct sockaddr *) &local, strlen(local.sun_path)
                   + sizeof(local.sun_family));

  if (ret_val == -1)
  {
    perror("Error binding socket");
    exit (1);
  }

  if (listen (server_socket, 1) == -1)
  {
    perror("Error listening on socket");
    exit (1);
  }

  ir_init(&default_timeout);

  while (1)
  {
  	fd_set set;
  	struct timeval tm;
    int ret_val = 0;
    int nc = 0;
    int max_fd = 0;

    FD_ZERO(&set);
    max_fd = (fd == 0) ? server_socket : fd;
		FD_SET(max_fd, &set);

    // If a refresh was scheduled wait for another request within
    // default_timeout milliseconds. This allows merging consecutive requests
    // into one and minimizes the amount of hardware accesses.
    if (pending)
    {
      tm.tv_sec = default_timeout / 1000;
			tm.tv_usec = (default_timeout % 1000) * 1000;
    }
    else
    {
			tm.tv_sec = 3600;
			tm.tv_usec = 0;
    }

    ret_val = select(max_fd + 1, &set, NULL, NULL, &tm);

    if (ret_val == -1)
    {
      perror("select error");
      if (fd)
        close(fd);
      break;
    }
    else if (ret_val == 0)
    {
      // Timeout reached.
      if (pending)
      {
        ir_refresh();
        pending = 0;
      }
    }
    else if (ret_val > 0)
    {
      if (fd == 0)
      {
        // New connection available on server_socket.
        fd = accept(server_socket, NULL, NULL);
      }
      else
      {
        // New data available on fd.
        ret_val = read(fd, buffer, 1);

        if (ret_val == 0)
        {
          // Connection closed.
          close(fd);
          fd = 0;
        }
        else if (ret_val > 0)
        {
          if (buffer[0] == IR_SETUP_APPENV)
            ir_setup_appenv();

          pending = 1;
        }
        else
        {
          perror("error while reading");
          break;
        }

      }

    }

  } // while(1)

  close(server_socket);

}

