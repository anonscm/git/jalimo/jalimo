/*
 * iliad_refresh.c
 *
 * gcc -c iliad_refresh.c 
 *
 * gcc -o program program_main.c iliad_refresh.o -lerdm -leripc
 * 
 * History:
 *         18/11/06 Creation
 *         30/11/06 Add debug information. Fix FDRD define. Move
 *                  erDm/erIpc to child. Close unneeded opened files
 *                  on child.
 *          3/12/06 Add more credits. Add the possibility of using Full
 *                  refresh. Add the configuration file iliad_refresh.conf
 *                  Stop the busy led after refresh.
 *       2007-09-17 Make compilable without ER libraries when DUMMY_LIB is
 *                  defined. Released under GNU GPL v2 or, at your option, any
 *                  later version.
 *
 * Author: Dario Rodriguez dario@softhome.net, darirf@yahoo.es
 *         Iliad interfacing code from Scotty1024's posts on MobileRead.com
 * This program is in the public domain. (<- valid for versions before
 * 2007-09-17)
 * This program is free software released under the GNU GENERAL PUBLIC LICENSE
 * version 2 or, at your option, any later version. See COPYING for details.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

/* header files */
#include <stdio.h>
#include <stdlib.h>

#include <sys/time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#if (defined HAVE_LIBERDM) && (defined HAVE_LIBERIPC)
#include <liberdm/erdm.h>
#include <liberipc/eripcviewer.h>
#include <liberipc/eripctoolbar.h>
#include <liberipc/eripcbusyd.h>
#endif

#define CONF_FILE "iliad_refresh.conf"

#if 0
#include <stdio.h>
#define DEBUG_PRINTF(a) printf a , fflush(stdout)
#define DUMMY_LIB
#else
#define DEBUG_PRINTF(a)
#endif

#if (defined HAVE_LIBERDM) && (defined HAVE_LIBERIPC)
#define MSG_DONE "done\n"

/* globals */
erClientChannel_t erbusyChannel;
erClientChannel_t ertoolbarChannel;
#else
#define MSG_DONE "done (faked)\n"
#endif

int is_full = 0;

/* prototypes of local functions */
void iliad_config_read(char *conf_file, int *is_full,  long *default_timeout);

void
ir_init(long *default_timeout)
{
	iliad_config_read(CONF_FILE, &is_full, default_timeout);

  printf("setting up communication channels ... ");
#if defined HAVE_LIBERIPC
  erIpcStartClient(ER_BUSYD_CHANNEL, &erbusyChannel);
  erIpcStartClient(ER_TOOLBAR_CHANNEL, &ertoolbarChannel);
#endif
  printf(MSG_DONE);
}

void
ir_setup_appenv()
{
  printf("turning off busy LED ... ");
  // Turn off busy LED
#if (defined HAVE_LIBERDM) && (defined HAVE_LIBERIPC)
  busySetBusy(erbusyChannel, ccBusyState_Off);
#endif
  printf(MSG_DONE);

  // Set-up comms with toolbar
  printf("communicating with toolbar ... ");
#if (defined HAVE_LIBERDM) && (defined HAVE_LIBERIPC)
  tbSelectIconSet(ertoolbarChannel, ER_PDF_VIEWER_UA_ID);
  tbClearIconSet(ertoolbarChannel, ER_PDF_VIEWER_UA_ID);
#endif
  printf(MSG_DONE);

  // Turn off trashcan
  printf("turning off trashcan ... ");
#if (defined HAVE_LIBERDM) && (defined HAVE_LIBERIPC)
  tbAppendPlatformIcon(ertoolbarChannel, ER_PDF_VIEWER_UA_ID, iconID_trashcan, -1);
  tbSetStatePlatformIcon(ertoolbarChannel, ER_PDF_VIEWER_UA_ID, iconID_trashcan, iconState_grey );
#endif
  printf(MSG_DONE);

  // Enable keyboard
  printf("enabling keyboard ... ");
#if (defined HAVE_LIBERDM) && (defined HAVE_LIBERIPC)
  tbAppendPlatformIcon(ertoolbarChannel, ER_PDF_VIEWER_UA_ID, iconID_keyboard, -1);
#endif
  printf(MSG_DONE);
}

void
ir_refresh()
{
  // code to force an update
  printf("doing refresh and LED update ... ");
#if (defined HAVE_LIBERDM) && (defined HAVE_LIBERIPC)
  dmDisplay(dmCmdPriorUrgent, (is_full != 0) ? dmQFull : dmQTyping); 
  busySetBusy(erbusyChannel, ccBusyState_Off);
#endif
  printf(MSG_DONE);
}

void 
iliad_config_read(char *conf_file, int *is_full,  long *default_timeout)
{
	int fd;
	int len;
	char buf[64]={0};
	static char str_full[]={"Full "};
	static char str_typing[]={"Typing "};
	if((fd=open(conf_file,O_RDONLY))<0) {
		if((fd=open(conf_file,O_WRONLY|O_TRUNC|O_CREAT,0777))>=0) {
			len=sprintf(buf,"%s%li\n",(*is_full!=0)?str_full:str_typing,*default_timeout);
			write(fd,buf,len);
			close(fd);

		}

	} else {
		read(fd,buf,sizeof(buf)-1);
		if(memcmp(buf,str_full,sizeof(str_full)-1)==0) {
			*is_full=1;
			*default_timeout=atol(buf+sizeof(str_full)-1);
		} else if(memcmp(buf,str_typing,sizeof(str_typing)-1)==0) {
			*is_full=0;
			*default_timeout=atol(buf+sizeof(str_typing)-1);
		} else {
			// we ignore configuration errors.
			fprintf(stderr,"%s: unrecognized refresh type: %s\n",conf_file,buf); 
		}
		close(fd);
	}
	if(*default_timeout<=0)
		*default_timeout=100;
}
