#ifndef __ILIAD_REFRESH_H
#define __ILIAD_REFRESH_H

#define ILIAD_REFRESH "/tmp/ILIAD_REFRESH"
#define BUFFER_SIZE 1

#define IR_SETUP_APPENV 1
#define IR_REFRESH 2

/** Initializes the communication channels. */
void ir_init(long *);

/** Sets up the initial environment for an application. */
void ir_setup_appenv();

/** Performs a screen refresh. */
void ir_refresh();

#endif
