# Simple script which copies the files from an OpenMoko build
# into the repository directory.
#

SRC_BASEDIR=`pwd`
DST_BASEDIR="/halde/jalimo_repositories/openmoko"

ARCHES="all armv4t"

# libjava-gnome*
FILES_all="\
	dbus-java* \
	libdbus-java* \
	libkxml2* \
	libmatthew* \
	libswt3.4-gtk-java* \
	libxmlpull* \
	"

FILES_armv4t="\
	cacao* \
	classpath* \
	jamvm* \
	libltdl* \
	libswt3.4-gtk-jni* \
	midpath* \
	libsdl-gfx11* \
	"

# ${1} - type
copyFiles()
{
  type=$1

  if [ $type = "all" ]
  then
    files=$FILES_all
  else
    files=$FILES_armv4t
  fi

  for F in $files
  do
    cp -v $SRC_BASEDIR/$type/$F $DST_BASEDIR/$type
  done
}


for A in $ARCHES
do
	copyFiles $A
done
