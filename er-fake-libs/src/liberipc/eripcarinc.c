
#include "eripcarinc.h"

int
arincParseCommand(char *szCommand, erIpcCmd_t *pCmd)
{
  return -1;
}

int
arincSignal(erClientChannel_t channel, int uaID, int signal)
{
  return -1;
}

int
arincJumpToPage(erClientChannel_t channel, int uaID, int page)
{
  return -1;
}

int arincBookmarkPage(erClientChannel_t channel, int uaID)
{
  return -1;
}

int arincInvokeFindDialog(erClientChannel_t channel, int uaID)
{
  return -1;
}

int arincZoomIn(erClientChannel_t channel, int uaID)
{
  return -1;
}

int arincZoomOut(erClientChannel_t channel, int uaID)
{
  return -1;
}

int arincStore(erClientChannel_t channel, int uaID, int category, char* itemID)
{
  return -1;
}
