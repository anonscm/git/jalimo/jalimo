#include "eripcpagebar.h"

int
pbSetPageCount(erClientChannel_t channel, int uaID, int pagecount)
{
  return -1;
}  

int
pbSetCurrentPage(erClientChannel_t channel, int uaID, int page)
{
  return -1;
}  

int
pbSetCurrentPageOffset(erClientChannel_t channel, int uaID, int pageoffset)
{
  return -1;
}  

int
pbSetZoomMode(erClientChannel_t channel, int uaID, int zoomMode)
{
  return -1;
}  

int
pbSetDrawAreaOrientation(erClientChannel_t channel, int uaID, int orientation)
{
  return -1;
}  

int
pbSetDrawAreaHOrigin(erClientChannel_t channel, int uaID, int origin)
{
  return -1;
}  

int
pbSetDrawAreaVOrigin(erClientChannel_t channel, int uaID, int origin)
{
  return -1;
}  

int
pbSetDrawAreaHSize(erClientChannel_t channel, int uaID, int size)
{
  return -1;
}  

int
pbSetDrawAreaVSize(erClientChannel_t channel, int uaID, int size)
{
  return -1;
}  

int
pbSetBarFontType(erClientChannel_t channel, int uaID, int font)
{
  return -1;
}  

int
pbSetBarFontHeight(erClientChannel_t channel, int uaID, int size)
{
  return -1;
}  

int
pbAddBookmark(erClientChannel_t channel, int uaID, int value)
{
  return -1;
}  

int
pbRemoveBookmark(erClientChannel_t channel, int uaID, int value)
{
  return -1;
}  

int
pbSetBookmarkMax(erClientChannel_t channel, int uaID, int max)
{
  return -1;
}  

int
pbAddNote(erClientChannel_t channel, int uaID, int value)
{
  return -1;
}  

int
pbRemoveNote(erClientChannel_t channel, int uaID, int value)
{
  return -1;
}  

int
pbSetNotesMax(erClientChannel_t channel, int uaID, int max)
{
  return -1;
}  

int
pbReset(erClientChannel_t channel, int uaID)
{
  return -1;
}  

int
pbShow(erClientChannel_t channel, int uaID, int show)
{
  return -1;
}  

int
pbRedraw(erClientChannel_t channel, int uaID)
{
  return -1;
}  

int
pbSynchronise(erClientChannel_t channel, int uaID)
{
  return -1;
}  

int
pbReportSynchronise(erClientChannel_t channel, int uaID)
{
  return -1;
}  

int
pbParseCommand(char *szCommand, erIpcCmd_t * pCmd)
{
  return -1;
}  
