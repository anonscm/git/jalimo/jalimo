#include "eripcviewer.h"

int
vwrParseCommand(char *szCommand, erIpcCmd_t *pCmd)
{
  return -1;
}

int
vwrJumpToPage(erClientChannel_t channel, int uaID, int page)
{
  return -1;
}

int
vwrBookmarkPage(erClientChannel_t channel, int uaID)
{
  return -1;
}

int
vwrInvokeFindDialog(erClientChannel_t channel, int uaID)
{
  return -1;
}

int
vwrZoomIn(erClientChannel_t channel, int uaID)
{
  return -1;
}

int
vwrZoomOut(erClientChannel_t channel, int uaID)
{
  return -1;
}

int
vwrStore(erClientChannel_t channel, int uaID, int category, char* itemID)
{
  return -1;
}
