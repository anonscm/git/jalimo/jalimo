#include "eripc.h"

int
erIpcGetVersion()
{
  return -1;
}

int
erIpcStartServer(int channelIdx, erMessageCB callback, char *szBuffer, int *nBuf, void *data)
{
  return -1;
}

int
erIpcOpenServerChannel(int channelIdx, erServerChannel_t * channel)
{
  return -1;
}

int
erIpcGetServerFd(erServerChannel_t channel)
{
  return -1;
}

int
erIpcGetMessage(erServerChannel_t channel, char *szBuffer, int *nBuf)
{
  return -1;
}

int
erIpcStartClient(int channelIdx, erClientChannel_t * channel)
{
  return -1;
}

int
erIpcInitServer(int channelIdx, int *sockfd, int local)
{
  return -1;
}

