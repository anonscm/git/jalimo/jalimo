#include "eripctoolbar.h"

int
tbParseCommand(char *szCommand, erIpcCmd_t *pCmd)
{
  return -1;
}

int
tbReportIconClicked(erClientChannel_t channel, int signal, int iconID, int iconState)
{
  return -1;
}

int
tbReportSynchronised(erClientChannel_t channel, int signal)
{
  return -1;
}

int
tbAppendPlatformIcon(erClientChannel_t channel, int uaID, int iconID, int signal)
{
  return -1;
}

int
tbRemovePlatformIcon(erClientChannel_t channel, int uaID, int iconID)
{
  return -1;
}

int
tbSetStatePlatformIcon(erClientChannel_t channel, int uaID, int iconID, int iconState)
{
  return -1;
}

int
tbDisableUpdate(erClientChannel_t channel, int uaID)
{
  return -1;
}

int
tbEnableUpdate(erClientChannel_t channel, int uaID)
{
  return -1;
}

int
tbSelectIconSet(erClientChannel_t channel, int uaID)
{
  return -1;
}

int
tbClearIconSet(erClientChannel_t channel, int uaID)
{
  return -1;
}

int
tbSynchronise(erClientChannel_t channel, int uaID, int signal)
{
  return -1;
}
