#include "eripceba.h"

int
ebaParseCommand(char *szCommand, erIpcCmd_t *pCmd)
{
  return -1;
}

int
ebaJumpToPage(erClientChannel_t channel, int uaID, int page)
{
  return -1;
}

int
ebaBookmarkPage(erClientChannel_t channel, int uaID)
{
  return -1;
}

int
ebaInvokeFindDialog(erClientChannel_t channel, int uaID)
{
  return -1;
}

int
ebaZoomIn(erClientChannel_t channel, int uaID)
{
  return -1;
}

int
ebaZoomOut(erClientChannel_t channel, int uaID)
{
  return -1;
}

int
ebaStore(erClientChannel_t channel, int uaID, int category, char* itemID)
{
  return -1;
}
