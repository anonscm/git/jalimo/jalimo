#include "eripcbusyd.h"
#include "eripccontentlister.h"

int
clParseCommand(char *szCommand, erIpcCmd_t *pCmd)
{
  return -1;
}

int
clStore(erClientChannel_t channel, int uaID, int category, char* itemID, char* itemLocation)
{
  return -1;
}

int
MSDiskSetConnected(erClientChannel_t channel, int connected)
{
  return -1;
}

int
clGotoPage(erClientChannel_t channel, int page)
{
  return -1;
}

int
clBatteryLow(erClientChannel_t channel, eCcClBatteryAction action, int percentage, int time_left)
{
  return -1;
}

int
clStoragePresent(erClientChannel_t channel, char* storageType, int storagePresent)
{
  return -1;
}

int
clBusyIndicator(erClientChannel_t channel, eCcBusyState status)
{
  return -1;
}

int
clDisplayUpdated(erClientChannel_t channel)
{
  return -1;
}

int
clNewContent(erClientChannel_t channel)
{
  return -1;
}
