Cacao Package Maintainer guide
------------------------------

This guide gives instructions and hints on how to maintain the cacao 
package for Jalimo.

TODO: This guide needs to be developed further.

Integrating a new release
-------------------------
 - download & extract source
 - export this repository inside the source folder
 - fix & update patches
 - copy all changes back into this sandboxed version of the packaging 
   files
 - commit all changes
 - build & release cacao

Updating a patch:
-----------------
 - extract cacao sources twice, eg. cacao-0.98 and cacao-0.98.orig
 - apply old patch against cacao-0.98
 - fix problems
 - create diff from inside cacao-0.98 folder against ../cacao-0.98.orig
 - that diff is your new patch
 - put that patch in the package files sandbox, add it to CVS, remove 
   the old one

Fixing patch date:
------------------
If you build cacao and get an error message that your patch has a 
modification time in the future fix the file's modification time like 
this:
	touch -d19700101 debian/patches/cacao-0.98-libdir.diff

Fixing configure date:
----------------------
If you get this error message after running
"dpkg-buildpackage -uc -us -rfakeroot":

checking whether build environment is sane... configure:
   error: newly created file is older than distributed files!

Run "touch configure" and "touch src/mm/boehm-gc/configure" to fix it.

Fixing configure.host date:
---------------------------
If you see a line saying

"make[5]: Warning: File `configure.host' has modification time 1.3e+04 s
 in the future"

and the build looping over and over again. Stop the build run
"touch -d1970101 src/mm/boehm-gc/configure.host" to fix it.

Build a cacao snapshot from Mercurial (hg)
------------------------------------------
- export files from Mercurial, remember the export date
- name a directory according to the export date:

	cacao-<last release>+<HG export date>

  Eg. for a cacao snapshot that was done before the 0.99
  release a proper folder name would be cacao-0.98+hg20071012.
- set SBOX_DEFAULT_AUTOMAKE=1.9
- run ./autogen.sh
- run automake -acf
- run libtoolize -c -f
- tar.gz the directory
- follow the instructions under 'Integrating a new release'
