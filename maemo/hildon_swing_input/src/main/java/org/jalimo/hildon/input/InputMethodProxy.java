package org.jalimo.hildon.input;

/**
 *
 *
 * Created: Tue Dec  4 15:12:28 2007
 *
 * @author Sebastian Mancke, tarent GmbH
 * @version 1.0
 */

public class InputMethodProxy {

    protected long imContextPointer;
    protected InputMethodListener inputMethodListener;

    protected InputMethodProxy() {
    }

    /**
     * Returns a new InputMethodProxy for a given gtk window.
     * This proxy holds the associated input context.
     */
    public static InputMethodProxy createProxy(long nativeGtkWindowPointer) {
	InputMethodProxy proxy = new InputMethodProxy();	
	long contextPointer = proxy.create_im_context(nativeGtkWindowPointer);
	if (contextPointer == 0l)
	    return null;
	proxy.imContextPointer = contextPointer;
	return  proxy;
    }

    protected native long create_im_context(long nativeGtkWindowPointer);


    /**
     * Raises the keyboard.
     */
    public void raiseKeyboard() {
	raise_keyboard(imContextPointer);
    }
    protected native void raise_keyboard(long imContextPointer);


    /**
     * Lowers the keyboard.
     */
    public void lowerKeyboard() {
	lower_keyboard(imContextPointer);	
    }    
    protected native void lower_keyboard(long imContextPointer);

    public void setInputMethodListener(InputMethodListener newListener) {
	inputMethodListener = newListener;
    }

    /**
     * Callback method for insertion of strings over the input framework
     */
    protected void stringInserted(String string) {
	System.out.println("string: "+string);
	inputMethodListener.stringInserted(string);
    }
}
