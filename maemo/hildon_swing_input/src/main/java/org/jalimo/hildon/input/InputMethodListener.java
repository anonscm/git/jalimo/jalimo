package org.jalimo.hildon.input;


/**
 * Listener interface for inputs from the hildon input method.
 */
public interface InputMethodListener {


    /**
     * Notify about the insertion of a string
     */
    public void stringInserted(String string);


}