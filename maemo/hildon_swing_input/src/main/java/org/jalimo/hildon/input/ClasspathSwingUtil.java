package org.jalimo.hildon.input;

import java.awt.Component;
import java.awt.Container;
import java.awt.Window;
import java.awt.Robot;
import java.awt.event.ContainerListener;
import java.awt.event.FocusListener;
import java.awt.event.ContainerEvent;
import java.awt.event.FocusEvent;
import java.awt.event.WindowStateListener;
import java.awt.event.WindowEvent;
import java.awt.event.InputMethodEvent;
import java.awt.AWTException;

import java.text.AttributedString;

import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JDialog;
import javax.swing.text.JTextComponent;

import gnu.java.awt.peer.gtk.GtkGenericPeer;


/**
 * Helper class for acceccing the parts of the native gtk peer from GNU Classpath
 */
public class ClasspathSwingUtil implements ContainerListener, FocusListener, InputMethodListener {

    InputMethodProxy imProxy;
    Window topLevelComponent;
    Robot awtRobot;
    JTextComponent selectedComponent;

    public ClasspathSwingUtil(JFrame frame) {
	init(frame);
    }

    public ClasspathSwingUtil(JDialog dialog) {
	init(dialog);
    }

    protected void init(Window window) {
	try {
	    awtRobot = new Robot();
	} catch (AWTException e) {
	    throw new RuntimeException(e);
	}
	topLevelComponent = window;
	

	// start observing after the window has opened the first time
	long framePointer = getNativeWidgetPointer(topLevelComponent);
	imProxy = InputMethodProxy.createProxy(framePointer);
	imProxy.setInputMethodListener(ClasspathSwingUtil.this);
	observe(topLevelComponent);

// 	System.out.println("add window listener");

// 	window.addWindowStateListener(new WindowStateListener() {
// 		public void windowStateChanged(WindowEvent e) {
// 		    System.out.println("window state changed");
	
// 		    if (e.getNewState() == WindowEvent.WINDOW_OPENED) {
// 			System.out.println("window opened");

// 			// start observing after the window has opened the first time
// 			long framePointer = getNativeWidgetPointer(topLevelComponent);
// 			imProxy = InputMethodProxy.createProxy(framePointer);
// 			imProxy.setInputMethodListener(ClasspathSwingUtil.this);
// 			observe(topLevelComponent);
// 		    }
// 		}
// 	    });
    }
    
    /**
     * Returns the native pointer of the gtk widget associated with a particular component.
     */
    public static long getNativeWidgetPointer(Component component) {
	GtkGenericPeer peer = (GtkGenericPeer)component.getPeer();
	System.out.println("generic peer is: "+ peer);
	
	return getPointerForPeer(peer);
    }

    protected static native long getPointerForPeer(GtkGenericPeer peer);

    protected void observe(Component component) {
	if (component instanceof Container) {
	    for (Component child : ((Container)component).getComponents()) {
		observe(child);
	    }
	    ((Container)component).addContainerListener(this);
	} 
	if (component instanceof JTextComponent) {
	    component.addFocusListener(this);
	}
    }

    public void componentAdded(ContainerEvent e) {
	observe(e.getChild());
    }

    public void componentRemoved(ContainerEvent e) {
    }

    public void focusGained(FocusEvent e) {
	selectedComponent = (JTextComponent)e.getSource();	    
	imProxy.raiseKeyboard();
    }

    public void focusLost(FocusEvent e) {
	selectedComponent = null;	    
	imProxy.lowerKeyboard();
    }

    public void stringInserted(String string) {
	System.out.println("got string: "+string);
	if (selectedComponent == null) {
	    System.out.println("no component selected");
	    return;
	}
	InputMethodEvent imEvent  = new InputMethodEvent(topLevelComponent,
							 InputMethodEvent.INPUT_METHOD_FIRST,
							 new AttributedString(string).getIterator(),
							 string.length(),
							 null,
							 null);
	
	selectedComponent.enableInputMethods(true);
	selectedComponent.getInputContext().dispatchEvent(imEvent);
	
// 	for (int i=0; i<string.length(); i++) {
// 	    char c = string.charAt(i);
// 	    System.out.println("simulate key: "+c);
	    
//  	    awtRobot.keyPress(c);
//  	    awtRobot.keyRelease(c);
// 	}
    }
}