
#include "jni.h"


/**
 * The Pointer class.
 */
static jclass pointerClass;

/**
 * The field ID of the data field in the Pointer class.
 */
static jfieldID pointerDataFID;

/**
 * The field ID of the widget field in the GtkGenericPeer class.
 */
static jfieldID widgetFID;



/**
 * Initializes the field IDs for the widget reference.
 *
 * @param env the JNI environment
 */
void classpath_swing_util_init_IDs(JNIEnv *env) {
  if (widgetFID != NULL)
    return;

  /* Find the data field ID in Pointer32 */
  pointerClass = (*env)->FindClass(env, "gnu/classpath/Pointer32");
  pointerDataFID = (*env)->GetFieldID(env, pointerClass, "data", "I");
  
  /* Find the widget field ID in GtkGenericPeer. */
 jclass cls = (*env)->FindClass(env, "gnu/java/awt/peer/gtk/GtkGenericPeer");
  widgetFID = (*env)->GetFieldID(env, cls, "widget", "Lgnu/classpath/Pointer;");
}

/**
 * Retrieves the GTK widget reference from a GtkGenericPeer object.
 * This method was taken from gnu classpath.
 *
 * @param genericPeer the actual peer object
 *
 * @return the widget reference
 */
JNIEXPORT jlong JNICALL Java_org_jalimo_hildon_input_ClasspathSwingUtil_getPointerForPeer (JNIEnv *env, jclass cls, jobject genericPeer) {
  jobject obj;
  void *widget;

  classpath_swing_util_init_IDs(env);

  /* Fetch the widget field from the peer object. */
  obj = (*env)->GetObjectField(env, genericPeer, widgetFID);

  /* Fetch actual widget pointer. */
  widget = (void*) (*env)->GetIntField(env, obj, pointerDataFID);
  return widget;
}
