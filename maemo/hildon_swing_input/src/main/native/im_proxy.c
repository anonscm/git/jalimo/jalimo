/**
 * Copyright by Sebastian Mancke, Tarent GmbH
 * This code is licensend under the GPL v2.
 *
 * Some mehtods (java_getEnv()) are taken from java-gnome.sourceforge.net.
 */

#include <jni.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <glib.h>
#include <gtk/gtk.h>
#include <hildon/hildon-program.h>

#include "jni_include/org_jalimo_hildon_input_InputMethodProxy.h"

static JavaVM*  cachedJavaVM;
GtkWindow *window;

/**
 * Here we cache the JavaVM pointer so that we can use it to retrieve the
 * JNIEnv interface pointer whenever necessary.
 */
/*
 * A symbol by this name is automatically called when the library containing it
 * is loaded by a System.loadLibrary().
 */
JNIEXPORT jint JNICALL JNI_OnLoad(JavaVM *jvm, void *reserved) {
  cachedJavaVM = jvm;
  return JNI_VERSION_1_4;
}

/**
 * Since the JNIEnv pointer is specific to each thread, it is necessary to
 * retrieve it from the VM directly when we are dealing with arbitrary events
 * as opposed to local JNI calls. Returns NULL on failure.
 */
JNIEnv* java_getEnv() {
  JNIEnv* env;
  jint result;

  result = (*cachedJavaVM)->GetEnv(cachedJavaVM, (void **) &env, JNI_VERSION_1_4);
  if (env == NULL) {
    switch (result) {
    case JNI_EDETACHED:
      g_critical("Tried to get JNIEnv but this thread detached");
      break;
    case JNI_EVERSION:
      g_error("Trying to get JNIEnv resulted in version error.");
      break;
    }
    return NULL;
  }
  return env;
}

static gboolean commit_text(GtkIMContext *context, const gchar *str, gpointer *data) {  
  g_printf("text: %s\n", str);

  // get the jni environment
  JNIEnv* env = java_getEnv();

  // get the reference to the associated java class
  jobject self = (jobject)data;

  // TODO: resolve the mid only once
  jclass cls = (*env)->GetObjectClass(env, self);
  jmethodID mid = (*env)->GetMethodID(env, cls, "stringInserted", "(Ljava/lang/String;)V");
  jstring string = (*env)->NewStringUTF(env, str);
  (*env)->CallVoidMethod(env, self, mid, string);
  
  return TRUE;
}


JNIEXPORT jlong JNICALL Java_org_jalimo_hildon_input_InputMethodProxy_create_1im_1context (JNIEnv *env, jobject self, jlong gtk_window_pointer) {
  g_printf("in create_1im_1contex\n");

/*   GtkWindow *window = (GtkWindow *)gtk_window_pointer; */
  window = (GtkWindow *)gtk_window_pointer;
  gtk_window_set_title(window, "I'm a window");
  g_printf("done cast\n");

  gdk_threads_enter();

  /** crate input context for this window*/
  GtkIMContext * gtk_im_context = gtk_im_multicontext_new();
  g_printf("got context\n");

  gtk_im_context_set_client_window(gtk_im_context, GTK_WIDGET(window)->window); 
  g_printf("have set client window\n");

  g_signal_connect (gtk_im_context, "commit", G_CALLBACK (commit_text), self);   
  g_printf("have done signal connect\n");

  gdk_threads_leave();

  return (jlong)gtk_im_context;
}


JNIEXPORT void JNICALL Java_org_jalimo_hildon_input_InputMethodProxy_raise_1keyboard (JNIEnv *env, jobject self, jlong im_context) {
  GtkIMContext *gtk_im_context =  (GtkIMContext *)im_context;

  gdk_threads_enter();

  gtk_im_context_reset (gtk_im_context);
  hildon_gtk_im_context_show(gtk_im_context);
  gtk_im_context_focus_in (gtk_im_context);
  gdk_threads_leave();
}


JNIEXPORT void JNICALL Java_org_jalimo_hildon_input_InputMethodProxy_lower_1keyboard (JNIEnv *env, jobject self, jlong im_context) {
  GtkIMContext *gtk_im_context =  (GtkIMContext *)im_context;

  gdk_threads_enter();
  hildon_gtk_im_context_hide(gtk_im_context);
  gdk_threads_leave();
}


