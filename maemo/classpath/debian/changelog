classpath (2:0.95+cvs20070516-1) experimental; urgency=low

  * Build with escher.jar and local sockets enabled.

 -- Robert Schuster <thebohemian@gmx.net>  Wed, 16 May 2007 08:19:38 +0200

classpath (2:0.95-1) experimental; urgency=low

  * New upstream release.
  * Changed the hacking.info and vmintegration.info for cp-hacking.info and cp-vmintegration.info

 -- Sebastian Mancke <asteban@gmx.de>  Thu, 16 Apr 2007 15:27:00 +0100

classpath (2:0.93-1) experimental; urgency=low

  * Upload to experimental.
  * New upstream release.
  * Updated config.guess and config.sub.

 -- Michael Koch <konqueror@gmx.de>  Sat, 17 Feb 2007 08:56:00 +0100

classpath (2:0.92-4) unstable; urgency=low

  * Include /usr/lib/classpath/libgconfpeer.so.0.0.0 (Closes: #387318).
  * Always build-depend on libxul-dev, all architectures have it now
    (Closes: #372851).
  * Corrected Conflicts and Replaces for classpath-qtpeer: epoch was
    missing.
  * Don't set links for libjawt.so. Not needed anymore.

 -- Michael Koch <konqueror@gmx.de>  Sun, 14 Jan 2007 21:35:45 +0100

classpath (2:0.92-3.1) unstable; urgency=low

  * Non-maintainer upload.
  * Corrected Conflicts and Replaces for classpath-gtkpeer: epoch was
    missing. (Closes: #387983)

 -- Franz Pletz <fpletz@franz-pletz.org>  Sun, 15 Oct 2006 15:59:02 +0200

classpath (2:0.92-3) unstable; urgency=low

  * Build tools correctly on binary-arch-only builds (Closes: #387033).
  * Added /usr/lib/classpath/libgconfpeer.so to classpath package
    (Closes: #387318).
  * Build-Depends on libmagic-dev.

 -- Michael Koch <konqueror@gmx.de>  Wed, 13 Sep 2006 16:35:23 +0000

classpath (2:0.92-2) unstable; urgency=low

  * debian/control: Added Build-Depends on libgconf2-dev (Closes: #386940).
  * debian/control: classpath-gtkpeer, classpath-qtpeer: Added Replaces on
    classpath (<< 0.92-1) and Conflicts (<< 0.92-1) (Closes: #386917, #386919).

 -- Michael Koch <konqueror@gmx.de>  Mon, 11 Sep 2006 17:28:17 +0000

classpath (2:0.92-1) unstable; urgency=low

  * New upstream release (Closes: #385369, #384354).
    - Adjusted debian/patches/10_appletviewer.dpatch to patch gappletviewer.
  * debian/control: Don't Build-Depends on cdbs.

 -- Michael Koch <konqueror@gmx.de>  Sat,  9 Sep 2006 07:39:01 +0000

classpath (2:0.91+cvs20060611-2) experimental; urgency=low

  * Honors disabled Java setting in Firefox (Closes: #266906).
  * debian/control: Moved gcjwebplugin to Section: net.
  * debian/control: Don't Build-Depends on sound dependencies on kfreebsd-i386,
    kfreebsd-amd64 and hurd-i386.
  * debian/rules: Enable sound support only on linux (Closes: #372851).
  * debian/README.Debian: Removed description for enabling Graphics2D support.
    It's used by default now.
  * debian/control: Build-Depends on libxul-dev (Closes: #373791).

 -- Michael Koch <konqueror@gmx.de>  Sun, 18 Jun 2006 11:04:50 +0000

classpath (2:0.91+cvs20060611-1) experimental; urgency=low

  * New upstream release
    - Shows warning before loading an applet (Closes: #267040, #301134).
    - New package Depends on jamvm or cacao (Closes: #369979).
    - Crashes with Firefox are not reproducable anymore
      (Closes: #290498, #336773).
    - Starts up with Firefox (Closes: #275245).
    - Fixes NullPointerException during applet loading (Closes: #351518).
    - Fixes deadlock in image drawing code (Closes: #357830).
  * debian/control: New package gcjwebplugin
  * debian/gcjwebplugin.links, debian/gcjwebplugin.install: New files.
  * debian/classpath-common-unzipped.install: Install classes in sun.*
    namespace.
  * debian/classpath.install: Don't install new appletviewer binary.
  * debian/patches/appletviewer.patch: Added runtime selection to
    appletviewer (Closes: #359654).

 -- Michael Koch <konqueror@gmx.de>  Sun, 11 Jun 2006 16:03:07 +0000

classpath (2:0.91-3) unstable; urgency=low

  * Install header files to /usr/include/classpath.
  * debian/control: classpath: Conflict with jamvm < 1.4.3 and
    cacao < 0.96 (Closes: #368172).

 -- Michael Koch <konqueror@gmx.de>  Sat, 27 May 2006 17:51:11 +0000

classpath (2:0.91-2) unstable; urgency=low

  * Set the correct version moc for architecture specific builds too.

 -- Michael Koch <konqueror@gmx.de>  Tue, 16 May 2006 05:35:36 +0000

classpath (2:0.91-1) unstable; urgency=low

  * New upstream release
    - crash when using cairo (Closes: #358356)
    - includes java.lang.Thread.UncaughtExceptionHandler (Closes: #356805)
  * debian/rules: Don't build tools.zip when only building architecture
    specific stuff. (Closes: #356353)
  * debian/control: Fixed typo. (Closes: #363202)
  * debian/classpath-common.install: Install tools.zip from correct place.
  * debian/rules: Make sure to build with the correct version of moc.
    (Closes: #356663, #356666)
  * Activated MIDI support. (Closes: #338287)
  * Updated Standards-Version to 3.7.2.

 -- Michael Koch <konqueror@gmx.de>  Mon, 15 May 2006 19:50:11 +0000

classpath (2:0.90-1) unstable; urgency=low

  * New upstream release
  * debian/README.Debian: Clarified usage of Graphics2D support.
  * Enabled DSSI support.

 -- Michael Koch <konqueror@gmx.de>  Mon,  6 Mar 2006 22:46:46 +0000

classpath (2:0.20-2) unstable; urgency=low

  * Moved package under maintenance of Debian Java Maintainers.
  * Added John to the Uploaders. Thanks John for the good job.
  * Enabled build of QT peer.

 -- Michael Koch <konqueror@gmx.de>  Fri, 27 Jan 2006 12:28:57 +0000

classpath (2:0.20-1) unstable; urgency=low

  * New upstream release
    - Fixes intial window size for JAP (Closes: #340964)
    - Fixes ant code completion in Eclipse (Closes: #338600)
  * debian/classpath.links: Added link for libjawt.so (Closes: #337601)

 -- Michael Koch <konqueror@gmx.de>  Sat, 14 Jan 2006 12:45:01 +0000

classpath (2:0.19-2) unstable; urgency=low

  * debian/classpath.install: Use wildcards instead of name each file
    explicitely. (Closes: #337875)
  * Build-Depends on libasound2-dev.
  * Upload sponsored by Petter Reinholdtsen.

 -- Michael Koch <konqueror@gmx.de>  Wed,  9 Nov 2005 08:55:05 +0000

classpath (2:0.19-1) unstable; urgency=low

  * New upstream release
    - Reworked debian/patches/disable-qt-peer.dpatch.
    - Removed debian/patches/serialization.dpatch. Applied upstream.
  * Upload sponsored by Petter Reinholdtsen

 -- Michael Koch <konqueror@gmx.de>  Wed,  9 Nov 2005 08:33:15 +0000

classpath (2:0.18-6) unstable; urgency=low

  * debian/watch: Reworked to ignore releases of generics branch.

 -- Michael Koch <konqueror@gmx.de>  Sun,  6 Nov 2005 12:16:43 +0000

classpath (2:0.18-5) unstable; urgency=low

  * debian/rules: Depend on patch target for configure-native-only

 -- Michael Koch <konqueror@gmx.de>  Sat, 24 Sep 2005 07:45:54 +0000

classpath (2:0.18-4) unstable; urgency=low

  * Moved gjdoc from Build-Depends to Build-Depends-Indep and updated
    it use >= 0.7.5-4

 -- Michael Koch <konqueror@gmx.de>  Fri, 23 Sep 2005 22:15:55 +0000

classpath (2:0.18-3) unstable; urgency=low

  * Build-Depends on dpatch and gjdoc (>= 0.7.5-2)
  * debian/rules: Use dpatch
  * Added debian/patches/serialization.dpatch and patched
    java/io/ObjectInputStream.java to fix serialization  (Closes: #310520)
  * Reworked debian/patches/disable-qt-peer.diff to be
    debian/patches/disable-qt-peer.dpatch
  * debian/rules: use --with-gjdoc and copy javadocs to the right dir
    (Closes: #222495)

 -- Michael Koch <konqueror@gmx.de>  Thu, 15 Sep 2005 19:49:10 +0000

classpath (2:0.18-2) unstable; urgency=low

  * Build-Depends on libcairo2-dev
  * Use --enable-gtk-cairo with configure (Closes: #327177)
  * debian/control: classpath-common has to replace classpath (<= 2:0.07-2)
    (Closes: #246575)
  * Upload sponsored by Petter Reinholdtsen

 -- Michael Koch <konqueror@gmx.de>  Fri,  9 Sep 2005 21:17:51 +0000

classpath (2:0.18-1) unstable; urgency=low

  * New upstream release

 -- Michael Koch <konqueror@gmx.de>  Mon,  5 Sep 2005 11:56:28 +0000

classpath (2:0.17-1) unstable; urgency=low

  * New upstream release (Closes: #325348)
  * Aknowledged NMUs. Thanks Steve (Closes: #322143)
  * Updated Standards-Version to 3.6.2
  * debian/copyright: Updated address of FSF

 -- Michael Koch <konqueror@gmx.de>  Tue, 30 Aug 2005 06:56:52 +0000

classpath (2:0.14-2.2) unstable; urgency=low

  * Non-maintainer upload.
  * In addition to build-depending on gcj, we also need to invoke gcj
    when building instead of looking for /usr/bin/gcj-3.3!
    (really-closes: #322143)

 -- Steve Langasek <vorlon@debian.org>  Sun, 28 Aug 2005 00:21:07 -0700

classpath (2:0.14-2.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Medium-urgency upload for RC bugfix.
  * Build-depend on gcj on arm, instead of the now-removed gcj-3.3
    (closes: #322143).

 -- Steve Langasek <vorlon@debian.org>  Thu, 25 Aug 2005 18:17:50 -0700

classpath (2:0.14-2) unstable; urgency=high

  * Removed build-dependency on gjdoc as gjdoc is not currently used during
    build.

 -- Michael Koch <konqueror@gmx.de>  Mon,  2 May 2005 19:03:17 +0000

classpath (2:0.14-1) unstable; urgency=low

  * New upstream release
  * debian/control: Depend on gjdoc
  * debian/rules: Generate javadocs

 -- Michael Koch <konqueror@gmx.de>  Fri, 25 Feb 2005 14:29:53 +0000

classpath (2:0.13-3) unstable; urgency=low

  * Include usr/share/classpath/gnu/java/awt/peer/gtk/font.properties
  in classpath-common-unzipped

 -- Michael Koch <konqueror@gmx.de>  Mon, 10 Jan 2005 11:55:29 +0000

classpath (2:0.13-2) unstable; urgency=low

  * debian/copyright clarified.
  * Removed font.properties from classpath-common-unzipped
  * Added libx11-dev and libice-dev to Build-Depends
  * Move examples only when building arch-indep packages

 -- Michael Koch <konqueror@gmx.de>  Sun,  9 Jan 2005 11:31:45 +0000

classpath (2:0.13-1) unstable; urgency=low

  * New upstream release
  * Removed Recommends on libgnujaxp-java
  * Added Build-Depends on libxt-dev and libxtst-dev
  * Removed Build-Conflict with gjdoc.
  * Updated Build-Depends for jikes (>= 1.19)
  * debian/rules.cdbs: Removed vm/current workaround
  * debian/control: Revised descriptions
  * Included examples in classpath-doc package

 -- Michael Koch <konqueror@gmx.de>  Sat,  8 Jan 2005 17:05:10 +0000

classpath (2:0.12-1) unstable; urgency=low

  * New upstream release
  * Use old build system again for now as it supports binary-arch builds more
    easily
  * Added patches to allow build of native code only

 -- Michael Koch <konqueror@gmx.de>  Mon, 15 Nov 2004 07:58:19 +0000

classpath (2:0.11-2) unstable; urgency=low

  * Fixed file conflict (Closes: #271750)
  * Downgraded Depends on libgnujaxp-java to a Recommends
  * Include usr/share/info/vmintegration.info in classpath

 -- Michael Koch <konqueror@gmx.de>  Tue, 12 Oct 2004 09:47:55 +0000

classpath (2:0.11-1) unstable; urgency=low

  * New upstream release
  * classpath: Depend on libgnujaxp-java
  * debian/rules: Removed jni.h cleanup hack

 -- Michael Koch <konqueror@gmx.de>  Tue, 14 Sep 2004 07:26:05 +0000

classpath (2:0.10.1-2) unstable; urgency=low

  * debian/rules: Fixed arm for real now
    (a little typo and using $(shell ...) instead of backticks)
  * debian/watch: Added

 -- Michael Koch <konqueror@gmx.de>  Sat, 28 Aug 2004 10:13:33 +0200

classpath (2:0.10.1-1) unstable; urgency=low

  * New upstream release (hack to make this a non-native package)

 -- John Leuner <jewel@debian.org>  Fri, 27 Aug 2004 14:10:17 +0200

classpath (2:0.10-3) unstable; urgency=low

  * Added Michael Koch and Arnaud Vandyck as uploaders 

 -- John Leuner <jewel@debian.org>  Thu, 26 Aug 2004 12:54:15 +0200

classpath (2:0.10-2) unstable; urgency=low

  * Use gcj-3.3 on arm
  * debian/rules: Added check-install target
  * classpath-doc: Moved to doc section
  * Build-Depends on zip instead of unzip
  * Added link from /usr/share/man/man1/jikes-classpath.1.gz to
    /usr/share/man/man1/jikes.1.gz
  * Ignore JNI libraries in usr/lib/classpath when calling dh_makeshlibs

 -- John Leuner <jewel@debian.org>  Mon,  9 Aug 2004 09:44:46 +0200

classpath (2:0.10-1) unstable; urgency=low

  * New upstream release
    - Adds AMD64 support (Closes: #248915, #255726)
  * Ported to CDBS as build system (Closes: #207190)
  * Updated Standards-Version
  * debian/copyright: Fixed lintian warning (Closes: #250370)
  * debian/control: Removed some unused cruft
  * Aknowledge old NMUs (Closes: #148361, #151103)

 -- John Leuner <jewel@debian.org>  Wed, 14 Jul 2004 10:02:39 +0200

classpath (2:0.09-2) unstable; urgency=low

  * added classpath-common-unzipped package with unzipped glibj.zip as
    requested by Michael Koch (Closes: #249643)

 -- John Leuner <jewel@debian.org>  Mon, 31 May 2004 10:35:59 +0200

classpath (2:0.09-1) unstable; urgency=low

  * New upstream release

 -- John Leuner <jewel@debian.org>  Thu,  6 May 2004 17:51:04 +0200

classpath (2:0.08-3) unstable; urgency=low

  * fixed dependings and Replaces 

 -- John Leuner <jewel@debian.org>  Fri, 30 Apr 2004 14:04:10 +0200

classpath (2:0.08-2) unstable; urgency=low

  * Fix copyright file

 -- John Leuner <jewel@debian.org>  Wed,  7 Apr 2004 10:51:56 +0200

classpath (2:0.08-1) unstable; urgency=low

  * New upstream release
  * Put glibj.zip into classpath-common package

 -- John Leuner <jewel@debian.org>  Thu, 18 Mar 2004 16:56:11 +0200

classpath (2:0.07-2) unstable; urgency=low

  * Re-uploading

 -- John Leuner <jewel@debian.org>  Sun, 18 Jan 2004 14:44:39 +0200

classpath (0.07-1) unstable; urgency=low

  * New upstream release

 -- John Leuner <jewel@debian.org>  Wed,  3 Dec 2003 14:03:48 +0200

classpath (0.06-2) unstable; urgency=low

  * Changed deps from gtk 1.2 to gtk 2.0
    (Closes: #207043)

 -- John Leuner <jewel@debian.org>  Mon, 25 Aug 2003 02:17:39 +0100

classpath (0.06-1) unstable; urgency=low

  * New upstream release

 -- John Leuner <jewel@debian.org>  Sat, 23 Aug 2003 16:48:54 +0100

classpath (0.05-3) unstable; urgency=low

  * Removed build-depends of automake,autoconf

 -- John Leuner <jewel@debian.org>  Wed, 11 Jun 2003 17:51:58 +0100

classpath (0.05-2) unstable; urgency=low

  * Moved documentation into correct place
    (Closes: #193037)

 -- John Leuner <jewel@debian.org>  Mon, 12 May 2003 23:53:37 +0100

classpath (0.05-1) unstable; urgency=low

  * New upstream release

 -- John Leuner <jewel@debian.org>  Thu, 20 Feb 2003 17:41:17 +0000

classpath (0.04-7) unstable; urgency=low

  * Renamed installed hacking.info.gz to classpath_hacking.info.gz

 -- John Leuner <jewel@debian.org>  Wed,  2 Oct 2002 17:47:08 +0100

classpath (0.04-6) unstable; urgency=low

  * Patched ieeefp.h to support S390 arch

  * Applied C. Scott Ananian's patch to create classpath-config
 
 -- John Leuner <jewel@debian.org>  Tue, 10 Sep 2002 19:44:31 +0100

classpath (0.04-5) unstable; urgency=low

  * Patched java.lang.String to version 1.49 of CVS (fixes trim())

 -- John Leuner <jewel@debian.org>  Tue, 13 Aug 2002 18:20:17 +0100

classpath (0.04-4.1) unstable; urgency=low

  * NMU.
  * Don't install common files with libgcj, depend on libgcj-common.
    Closes: #148361, #149187, #151103.

 -- Matthias Klose <doko@debian.org>  Fri,  2 Aug 2002 23:21:11 +0200

classpath (0.04-4) unstable; urgency=low

  * Changed the short description (was truncated) 

 -- John Leuner <jewel@debian.org>  Sat, 25 May 2002 18:22:25 +0100

classpath (0.04-3) unstable; urgency=low

  * Added build-depends on libart-dev 

 -- John Leuner <jewel@debian.org>  Sat, 25 May 2002 11:26:08 +0100

classpath (0.04-2) unstable; urgency=low

  * added build-depends on libgtk-dev 1.2 

 -- John Leuner <jewel@debian.org>  Fri, 24 May 2002 15:27:11 +0100

classpath (0.04-1) unstable; urgency=low

  * Initial Release.

 -- John Leuner <jewel@debian.org>  Tue,  7 May 2002 18:30:39 +0100

