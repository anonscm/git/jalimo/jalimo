
package org.jalimo.gstreamer.grab;

import java.util.List;
import java.util.ArrayList;

public class Grabber {

	int width; 
	int height;
	int bpp;
	
	long startTime;
	long imageCount = 0;
	
    private List<FrameListener> frameListener = new ArrayList<FrameListener>(1);

    static {
        System.loadLibrary("gstreamer-java");
    };
    
    public Grabber(int width, int height, int bpp) {
    	this.width = width;
    	this.height = height;
    	this.bpp = bpp;
    }

    public void addFrameListener(FrameListener listener) {
        frameListener.add(listener);
    }    
    
    public void start() {
    	startGrabbing();
        startTime = System.currentTimeMillis();
        imageCount = 0;
    }

    public void stop() {
        System.out.println("stop grabbing not implemented yet");	
    }

    protected native void startGrabbing();
    
    protected void fireNextFrame(byte[] frame) {
        for (FrameListener listener : frameListener) {
            listener.nextFrame(frame, width, height, bpp);
        }
    }

    protected void nextFrame(byte[] frame) {
    	try {
    		imageCount++;
    		fireNextFrame(frame);
    		if (imageCount % 20 == 0) {
    			System.out.println( imageCount * 1000 / (System.currentTimeMillis()-startTime) +" fps");
    			imageCount = 0;
    	        startTime = System.currentTimeMillis();
    		}
    	} catch (Exception e) {
    		e.printStackTrace(System.err);
    	}
    }

    public static void main(String args[]) throws Exception {
        System.out.println("creating grabber");
        Grabber g = new Grabber(176, 144, 24);
        g.addFrameListener(new FrameListener() {
                public void nextFrame(byte[] frameData, int width, int height, int bpp) {
                    System.out.println("got frame into java! array length: "+frameData.length);		    
                    System.out.println("frameData[0, 1, 3]="+(0x00 | ((int)frameData[0])) +", "+frameData[1] +", "+frameData[2]);
                }
            });
        System.out.println("starting grabber");
        g.start();
        System.out.println("grabber started");
        while (true) {
            Thread.sleep(100);
        }
    }
}