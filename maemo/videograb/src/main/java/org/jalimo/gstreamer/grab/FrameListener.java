package org.jalimo.gstreamer.grab;


/**
 * Interface for receiver of new frames
 */
public interface FrameListener {

    /**
     * Notifies the receiver about a new frame
     *
     * @param frameData the raw rgb data of the image
     * @param width the image width
     * @param height the image height
     * @param bpp the color deepth in bit per pixel
     */
    public void nextFrame(byte[] frameData, int width, int height, int bpp);

}