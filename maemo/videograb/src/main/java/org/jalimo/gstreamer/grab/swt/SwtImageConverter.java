package org.jalimo.gstreamer.grab.swt;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.graphics.PaletteData;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.jalimo.gstreamer.grab.FrameListener;
import org.jalimo.gstreamer.grab.Grabber;

public abstract class SwtImageConverter implements FrameListener {
	
	/**
	 * Listener method for the FrameListener interface.
	 * It converts the image to an SWT Image and calls nextFrame(Image) with this.	 
	 *  
	 */
	public void nextFrame(byte[] frameData, int width, int height, int bpp) {
		Image image = constructImage(frameData, width, height, bpp);
		nextFrame(image);
	}

	/**
	 * Abstract hook for receiving SWT Images, overwrite this
	 * to get all images. This hook will be called within the Camera thread, use Display.getDefault().syncExec() for gui updates.  
	 * 
	 */
	public abstract void nextFrame(Image image);

	/**
	 * Constructs a SWT image out of the frame Data
	 * 
	 * @param frameData Raw RGB
	 * @param width
	 * @param height
	 * @param bpp
	 * @return
	 */
	public static Image constructImage(byte[] frameData, int width, int height, int bpp) {
		PaletteData palette = new PaletteData(0xff0000, 0x00ff00, 0x0000ff); 
		ImageData data = new ImageData(width, height, bpp, palette, width*3, frameData);
		return new Image(Display.getDefault(), data);
	}

	/**
	 * Test method, which simply shows a frame with the images  
	 * 
	 * @param args
	 */
	public static void main(String[] args) {		
		// get the display
		Display display = Display.getDefault();
		
		// create the main window
		final Shell shell = new Shell(display);
		shell.setLayout(new RowLayout(SWT.VERTICAL));		
		
		//final ImageWidget imageWidget = new ImageWidget(shell, SWT.NONE);
		final Label l = new Label(shell, SWT.NONE);

        Grabber g = new Grabber(176, 144, 24);
        g.addFrameListener(new SwtImageConverter() {
               Image oldImage = null;

               public void nextFrame(final Image image) {
            	   Display.getDefault().syncExec(new Runnable () {
            		   public void run () {
            			   try {
            				   System.out.println("set Image now");
            				   l.setImage(image);
            				   if (oldImage == null)
            					   shell.layout();            		   
            				   if (oldImage != null)
            					   oldImage.dispose();
            				   oldImage = image;
            			   } catch (Exception e) {
            				   e.printStackTrace();            		   
            			   }
            		   }
            	   });
                }
            });

        System.out.println("starting grabber");
        g.start();

		// create and add the button
		Button button = new Button(shell, SWT.NONE);
		button.setText("close");
		
		// add a click listener to close the main window
		button.addListener(SWT.Selection, new Listener() {
			public void handleEvent(Event arg0) {
				System.exit(0);
			}
		});
		
		System.out.println("open now");
		// open the main window		
		shell.open();		
		
		// start the gui
		while (!shell.isDisposed())
			display.readAndDispatch();
	}
}
