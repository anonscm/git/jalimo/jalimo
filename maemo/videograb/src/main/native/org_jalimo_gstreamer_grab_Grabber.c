
#include <jni.h>
#include <stdlib.h>
#include <gst/gst.h>

static JavaVM*  cachedJavaVM;

static jclass cls;
static jmethodID mid;
static jbyteArray jbuffer_data;

static int width = 176;
static int height = 144;
static int bpp = 24;

#define VIDEO_SRC "v4l2src"
#define RUNTIME_EX "java/lang/RuntimeException"

void throwException(JNIEnv *env, char *exception, char *msg) {
  jclass clazz = (*env)->FindClass(env, exception);
  if (!clazz) {
    g_critical("Couldn't get class for exception %s", exception);
  } else {
    (*env)->ThrowNew(env, clazz, msg);
  }
}

/**
 * Here we cache the JavaVM pointer so that we can use it to retrieve the
 * JNIEnv interface pointer whenever necessary.
 */
/*
 * A symbol by this name is automatically called when the library containing it
 * is loaded by a System.loadLibrary().
 */
JNIEXPORT jint JNICALL JNI_OnLoad(JavaVM *jvm, void *reserved) {
  cachedJavaVM = jvm;
  return JNI_VERSION_1_4;
}

/**
 * Since the JNIEnv pointer is specific to each thread, it is necessary to
 * retrieve it from the VM directly when we are dealing with arbitrary events
 * as opposed to local JNI calls. Returns NULL on failure.
 */
JNIEnv* java_getEnv() {
  JNIEnv* env;
  jint result;

  result = (*cachedJavaVM)->GetEnv(cachedJavaVM, (void **) &env, JNI_VERSION_1_4);
  if (env == NULL) {
    switch (result) {
    case JNI_EDETACHED:
      (*cachedJavaVM)->AttachCurrentThread(cachedJavaVM, (void **) &env, NULL);
      if (env == NULL) {
        g_critical("Tried to get JNIEnv but this thread detached and AttachCurrentThread does not help");
      } else {
        return env;
      }
      break;
    case JNI_EVERSION:
      g_error("Trying to get JNIEnv resulted in version error.");
      break;
    }
    return NULL;
  }
  return env;
}

static gboolean buffer_probe_callback(
		GstElement *image_sink,
		GstBuffer *buffer, GstPad *pad, void *selfPointer)
{
  
  /* This is the raw RGB-data that image sink is about
   * to discard */
  unsigned char *buffer_data = (unsigned char *) GST_BUFFER_DATA(buffer);
  
  // get the jni environment
  JNIEnv* env = java_getEnv();

  // get the reference to the associated java class
  jobject self = (jobject)selfPointer;

  if (NULL == cls) {
    cls = (*env)->GetObjectClass(env, self);
    if (NULL == cls) {
      g_critical("Couldn't find class");
      throwException(env, RUNTIME_EX, "Couldn't find class");
      return TRUE;
    }

    mid = (*env)->GetMethodID(env, cls, "nextFrame", "([B)V");
    if (NULL == mid) {
      g_critical("Couldn't find method id");
      throwException(env, RUNTIME_EX, "Couldn't find method id");
      return TRUE;
    }
  }

  jsize size = height * width * bpp/8;
  
   if (jbuffer_data == NULL) 
     jbuffer_data = (*env)->NewByteArray(env, size); 

/*   jbyteArray jbuffer_data = (*env)->NewByteArray(env, size); */
  //g_printf("array has length: %d\n", (*env)->GetArrayLength(env, &jbuffer_data));
  (*env)->SetByteArrayRegion(env, jbuffer_data, 0, size, buffer_data);
  (*env)->CallVoidMethod(env, self, mid, jbuffer_data);
  
  return TRUE;
}

/* Callback that gets called whenever pipeline's message bus has
 * a message */
static void bus_callback(GstBus *bus, GstMessage *message, void *appdata)
{
	gchar *message_str;
	GError *error;
	
	/* Report errors to the console */
	if(GST_MESSAGE_TYPE(message) == GST_MESSAGE_ERROR)
	{	
		gst_message_parse_error(message, &error, &message_str);
		g_error("GST error: %s\n", message_str);
		g_free(error);
		g_free(message_str);
	}
	
	/* Report warnings to the console */
	if(GST_MESSAGE_TYPE(message) == GST_MESSAGE_WARNING)
	{	
		gst_message_parse_warning(message, &error, &message_str);
		g_warning("GST warning: %s\n", message_str);
		g_free(error);
		g_free(message_str);
	}			
}


JNIEXPORT void JNICALL Java_org_jalimo_gstreamer_grab_Grabber_startGrabbing (JNIEnv *env, jobject self) {
  GstElement *pipeline, *camera_src, *image_sink;
  GstElement *csp_filter, *image_filter;
  GstCaps *caps;
  GstBus *bus;

  /* Initialize Gstreamer */
  gst_init(0, NULL);
	
  /* Create pipeline and attach a callback to it's
   * message bus */
  pipeline = gst_pipeline_new("grab-camera");

  bus = gst_pipeline_get_bus(GST_PIPELINE(pipeline));
  gst_bus_add_watch(bus, (GstBusFunc)bus_callback, NULL);
  gst_object_unref(GST_OBJECT(bus));
	
  /* Create elements */
  /* Camera video stream comes from a Video4Linux driver */
  camera_src = gst_element_factory_make(VIDEO_SRC, "camera_src");
  /* Colorspace filter is needed to make sure that sinks understands
   * the stream coming from the camera */
  csp_filter = gst_element_factory_make("ffmpegcolorspace", "csp_filter");

  /* Filter to convert stream to useable format */
  image_filter = gst_element_factory_make("ffmpegcolorspace", "image_filter");
  /* A dummy sink for the image stream. Goes to bitheaven */
  image_sink = gst_element_factory_make("fakesink", "image_sink");
  
  /* Check that elements are correctly initialized */
  if(!(pipeline && camera_src  && csp_filter 
       && image_filter && image_sink))
    {
      g_critical("Couldn't create pipeline elements");
      throwException(env, RUNTIME_EX, "Couldn't create pipeline elements");
      return;
    }
  
  /* Set image sink to emit handoff-signal before throwing away
   * it's buffer */
  g_object_set(G_OBJECT(image_sink), "signal-handoffs", TRUE, NULL);
  
  /* Add elements to the pipeline. This has to be done prior to
   * linking them */

  gst_bin_add_many(GST_BIN(pipeline), camera_src, csp_filter,
		   image_filter, image_sink, NULL);
	
  /* Specify what kind of video is wanted from the camera */
  caps = gst_caps_new_simple("video/x-raw-rgb",
			     "width", G_TYPE_INT, width,
			     "height", G_TYPE_INT, height,
			     NULL);
			

  /* Link the camera source and colorspace filter using capabilities
   * specified */
  if(!gst_element_link_filtered(camera_src, csp_filter, caps))
    {
      throwException(env, RUNTIME_EX, "Couldn't link elements: camera_src, csp_filter, caps");
      return;
    }
  gst_caps_unref(caps);

  /* gdkpixbuf requires 8 bits per sample which is 24 bits per
   * pixel */
  caps = gst_caps_new_simple("video/x-raw-rgb",
			     "width", G_TYPE_INT, width,
			     "height", G_TYPE_INT, height,
			     "bpp", G_TYPE_INT, bpp,
			     "depth", G_TYPE_INT, bpp,
			     "framerate", GST_TYPE_FRACTION, 15, 1,
			     NULL);
			
  /* Link the image-branch of the pipeline. The pipeline is
   * ready after this */
  if(!gst_element_link_many(csp_filter, image_filter, NULL)) { 
    throwException(env, RUNTIME_EX, "Couldn't link elements: csp_filter, image_filter");
    return;
  };
  if(!gst_element_link_filtered(image_filter, image_sink, caps)) { 
    throwException(env, RUNTIME_EX, "Couldn't link elements: image_filter, image_sink, caps");
    return;
  };
  
  gst_caps_unref(caps);

  g_signal_connect(G_OBJECT(image_sink), "handoff", G_CALLBACK(buffer_probe_callback), self);
	
  gst_element_set_state(pipeline, GST_STATE_PLAYING);
  return;
}


/* Destroy the pipeline on exit
static void destroy_pipeline(GtkWidget *widget, AppData *appdata)
{
	gst_element_set_state(appdata->pipeline, GST_STATE_NULL);
	gst_object_unref(GST_OBJECT(appdata->pipeline));
}

 */
