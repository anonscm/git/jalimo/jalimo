package org.jalimo.examples.swt;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableItem;

public class SwtExample2
{
  Display display;
  Shell shell;
  Button fullscreenButton;
  Button fileButton;
	Table table;
  FileDialog fd;

  public static void main(String[] args)
  {
    new SwtExample2();
  }

  public SwtExample2()
  {
    Display.setAppName("SWT Example Two");
    display = new Display();

    shell = new Shell(display);

    display.addFilter(SWT.KeyDown, new Listener()
    {
      public void handleEvent(Event e)
      {
        if (e.keyCode == SWT.F6)
          toggleFullscreen();
      }
    });

    createGUI();

    shell.open();

    while (!shell.isDisposed())
		{
      if (!display.readAndDispatch())
        display.sleep();
		}
  }

  private void toggleFullscreen()
  {
	  if ("Windows CE".equals(System.getProperty("os.name"))) {
		  MessageBox mbox = new MessageBox(shell);
		  mbox.setMessage("Not supported on Windows CE");
		  mbox.setText("Not supported");
		  mbox.open();
	  } else {
		  boolean b = !shell.getFullScreen();
		  fullscreenButton.setSelection(b);
		  shell.setFullScreen(b);
	  }
  }
  
  private void toggleFullscreen(boolean b) {
		if ("Windows CE".equals(System.getProperty("os.name"))) {
			MessageBox mbox = new MessageBox(shell);
			mbox.setMessage("Not supported on Windows CE");
			mbox.setText("Not supported");
			mbox.open();
		} else {
			shell.setFullScreen(b);
		}
  }

  private void createGUI()
  {
    fd = new FileDialog(shell, SWT.OPEN);
    fd.setText("FooBar!");

    Menu bar = new Menu(shell, SWT.BAR);
    MenuItem mainHeader = new MenuItem(bar, SWT.CASCADE); mainHeader.setText("Main");
    Menu main = new Menu(shell, SWT.DROP_DOWN);
    MenuItem item = new MenuItem(main, SWT.PUSH); item.setText("toggle fullscreen");
    item.addListener(SWT.Selection, new Listener() {
		public void handleEvent(Event arg0) {
			toggleFullscreen();
		}
    });

    MenuItem item2 = new MenuItem(bar, SWT.PUSH); item2.setText("Exit");
    item2.addListener(SWT.Selection, new Listener() {
		public void handleEvent(Event arg0) {
			shell.close();
			System.exit(0);
		}
    });

    mainHeader.setMenu(main);
    shell.setMenuBar(bar);

    shell.setLayout(new FillLayout(SWT.VERTICAL));

    Composite upper = new Composite(shell, SWT.NONE);
    upper.setLayout(new FillLayout());

    fullscreenButton = new Button(upper, SWT.TOGGLE);
    fullscreenButton.setText("toggle fullscreen");
    fullscreenButton.addListener(SWT.Selection, new Listener() {
		public void handleEvent(Event arg0) {
			boolean b = fullscreenButton.getSelection();
			toggleFullscreen(b);
		}
    });

    fileButton = new Button(upper, SWT.PUSH);
    fileButton.setText("open file\ndialog");
    fileButton.addListener(SWT.Selection, new Listener() {
		public void handleEvent(Event arg0) {
			fd.open();
		}
    });

    table = new Table (shell, SWT.BORDER);
    for (int i=0;i<200; i++) {
        TableItem tableItem = new TableItem(table, SWT.NONE, i);
        tableItem.setText ("Item " + i);
    }
  }
}
