package org.jalimo.examples.console;

import java.util.Enumeration;


public class HelloWorld {

	public static void main(String[] args) {
		if (args.length > 0) {
			if (args[0].equals("-h") || args[0].equals("--help")) {
				System.out.println("usage: jalimo-console-example [--help] [--sysinfo]");			
			} else if (args[0].equals("--sysinfo")) {
				Enumeration keys = System.getProperties().keys();
				while (keys.hasMoreElements()) {
					String key = (String)keys.nextElement();
					System.out.println(key+"="+System.getProperty(key));
				}
				System.out.println();
			}
		}
		System.out.println("Hello World");
	}

}
