package de.tarent.opreco;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;

public class SocketClientTransport implements ClientTransport {

	int port;
	String host;
	String password;
	Socket socket;
	OutputStreamWriter writer;
	BufferedReader reader;
	
	/**
	 * Create a new Socket connection to the supplied port
	 * @param port
	 */
	public SocketClientTransport(String host, int port) {
		this.host = host;
		this.port = port;
	}
	
	public void close() throws IOException {
		if (isConnected())
			socket.close();
		socket = null;
		writer = null;
		reader = null;
	}

	public boolean connect() throws IOException {
		socket = new Socket(host, port);
		writer = new OutputStreamWriter(socket.getOutputStream(),"UTF-8");
		reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));

		System.out.println("Send: "+password);
		
		String random = reader.readLine();
		System.out.println(ChecksumTool.createChecksum(password+random));
		
		writer.write(ChecksumTool.createChecksum(password+random));
		writer.write("\n");
		writer.flush();

		
		if(reader.readLine().equals("Bad Password")){
			socket.close();
			return false;
		}
		else {
			return true;
		}
	}
	

	public void sendCommand(String cmd) throws IOException {
		if(cmd.contains(";") ) {
			for (String s : cmd.split(";"))
				sendCommand(s);
			return;
		}
		if (!isConnected())
			throw new IOException("not connected");
		writer.write(cmd);
 		writer.write("\n");
		writer.flush();
		String result = reader.readLine();
		if("kicked, new client".equals(result)){
			socket.close();
			throw new IOException("Got Kicked, new client connected");			
		}
		else if (result == null)
			throw new IOException("no response from server");
		/*else if (!RESULT_OK.equals(result))
			throw new IOException("command "+cmd+" not accepted: "+result); */
	}

	public boolean isConnected() {
		return socket != null && socket.isConnected();
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	public String toString() {
		return "socket://"+host+":"+port;
	}

}
