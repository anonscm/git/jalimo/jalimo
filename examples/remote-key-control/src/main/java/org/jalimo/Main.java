package de.tarent.opreco;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;


public class Main {

	/**
	 * Start the key control application.
	 * Parameters: --client/--server
	 * @throws SocketException 
	 */
	public static void main(String[] args) {
		if (args.length == 0 || args[0].equals("--client")) {
			String host = "127.0.0.1";
			int port = 2000;
			if (args.length == 3) {
				host = args[1];
				port = Integer.parseInt(args[2]);
			} else {
				// choose a usable default by showing the own ip address
				if (!"Windows CE".equals(System.getProperty("os.name"))) {

					try {
						NetworkInterface ni = NetworkInterface.getByName("eth0");
						if (ni == null)
							ni = NetworkInterface.getByName("eth1");
						if (ni != null) {
							Enumeration<InetAddress> addresses = ni.getInetAddresses();
							while (addresses.hasMoreElements()) {
								InetAddress address = addresses.nextElement();
								if (address instanceof Inet4Address)
									host = address.getHostAddress();
							}
						}
					} catch (SocketException e) {
						// 	do nothing
					}
				}
				
			}
			ClientTransport transport = new SocketClientTransport(host, port);
			ControlClient client = new ControlClient(transport);
			client.start();
		} else if (args[0].equals("--server")) {
			ServerTransport transport = new SocketServerTransport(2000);
			Server server = new Server(transport);
			/*	if(args[1].equals("-p")){
					SocketServerTransport.password = args[2];
					System.out.println(args[2]);
					 
				} */ 
			server.start();
		} else {
			System.out.println("usage remote-key-control --client/--server");
			System.exit(1);
		}
	}

}
