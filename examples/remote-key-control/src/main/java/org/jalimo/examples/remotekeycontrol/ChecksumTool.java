package org.jalimo.examples.remotekeycontrol;

import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.security.DigestInputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.xml.sax.InputSource;

/**
 * 
 * This class provides common checksum-methods based on the MD5-Algorithm
 * 
 * @author Fabian K&ouml;ster (f.koester@tarent.de), tarent GmbH Bonn
 *
 */
public class ChecksumTool
{
	
	/**
	 * Determines whether the given file and the given checksum are equal
	 * 
	 * @param pFileName The filename of the file to checksum
	 * @param pChecksum The MD5-checksum to compare with
	 * @return true if equal
	 */
	
	public static boolean validateFile(String pFileName, String pChecksum)
	{	
		return pChecksum.equals(createChecksum(pFileName));
	}
	
	/**
	 * Returns the MD5-checksum of the given file as a <code>String</code>
	 * 
	 * @param pFileName The file to checksum
	 * @return The MD5-checksum of the file as a <code>String</code> or null if file not found
	 */
	
	public static String createChecksum(String input)
	{
		String checksumString = null;
		
		MessageDigest md;
		try {
			md = MessageDigest.getInstance("MD5");
		} catch (NoSuchAlgorithmException e) {
			return null;
		}
		
		InputStream fis = new ByteArrayInputStream(input.getBytes());
	/*	try {
			fis = new InputStream();
			
		} catch (FileNotFoundException e) {
			return null;
		} */
		
		DigestInputStream dis = new DigestInputStream(fis, md);
		
		// The dis.read method reads one byte of fis and updates the
		// MessageDigest. The following code reads the whole file.
		try {
			while(dis.read() != -1);
			dis.close();
		} catch (IOException e) {
			return null;
		}

		// Now we can compute the checksum.
		byte [] digest = md.digest();
		StringBuffer buffer = new StringBuffer();
		for(int i = 0; i < digest.length; i++) {
			String s = Integer.toHexString(digest[i] & 0xff);
			s = (s.length() == 1) ?	"0" + s : s;
			buffer.append(s);
		}
		checksumString = buffer.toString();
		
		return checksumString;
	}
}