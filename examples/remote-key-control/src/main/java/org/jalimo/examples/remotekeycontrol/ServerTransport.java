package org.jalimo.examples.remotekeycontrol;

import java.io.IOException;

public interface ServerTransport extends Transport {
	
	/**
	 * Listen for incoming requests.
	 * @throws IOException
	 */
	public void listen(CommandHandler handler) throws IOException;
	
	/**
	 * Call back interface for command halder
	 */
	public interface CommandHandler {
		/**
		 * Handle a command
		 * @return the result
		 * @throws IOException 
		 */
		public String processCommand(String cmd) throws IOException;
	}
}
