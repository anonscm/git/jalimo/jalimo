package org.jalimo.examples.remotekeycontrol;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class Server implements ServerTransport.CommandHandler {

	Process xte;
	ServerTransport transport;
	OutputStreamWriter writer;
	BufferedReader reader;
	
	public Server(ServerTransport transport) {
		this.transport = transport;
	}

	public void start() {
		ProcessBuilder builder = new ProcessBuilder("xte");
		try {
			xte = builder.start();
			writer = new OutputStreamWriter(xte.getOutputStream());
			reader = new BufferedReader(new InputStreamReader(xte.getInputStream()));
		} catch (IOException e) {
			System.err.println("error starting 'xte', maybe you have to install the 'xautomation' package");
			System.err.println("e.g. sudo apt-get install xautomation");
			e.printStackTrace(System.err);
			System.exit(2);
		}		

		try {
			transport.listen(this);
		} catch (IOException e) {
			System.out.println("error on listening");
			e.printStackTrace(System.err);
			System.exit(3);			
		}
		
		xte.destroy();
	}

	public String processCommand(String cmd) throws IOException {
		if (Transport.SERVER_QUIT.equals(cmd)) {
			transport.close();
		}
		writer.write(cmd);
		writer.write("\n");
		writer.flush();
		try {
			Thread.sleep(100);
		} catch (InterruptedException e) {
			// ignore safely
		}
		
		if (xte.getInputStream().available() > 0)
			return reader.readLine();
		else
			return null;
	}
}
