package org.jalimo.examples.remotekeycontrol;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;
import org.eclipse.swt.widgets.Text;

public class ConnectionDialog {

	Shell dialog;
	ClientTransport selectedTransport;
	SocketClientTransport socketTransport;
	//MotionControl motionControl = new MotionControl(socketTransport);
	MotionControl motionControl;
	Text hostEntry;
	Text portEntry;
	Text passwordEntry;
	Text btPasswordEntry;
	List list;
	
	static String password;
	boolean motionStatus = false;
	boolean statusOk;
	
	public void motionControlStart(){
		if((!MotionControl.status) ){
			motionControl = new MotionControl(socketTransport);
			MotionControl.status = true;
		} 
	}
    
	Listener connectListener = new Listener() {
		public void handleEvent(Event arg0) {
			try {
				socketTransport = (SocketClientTransport)selectedTransport;
				socketTransport.setHost(hostEntry.getText());			
				socketTransport.setPort(Integer.parseInt(portEntry.getText()));
				socketTransport.setPassword(passwordEntry.getText());
				motionControlStart();
				motionControl.start();
				
				statusOk = true;
				dialog.dispose();
			} catch (NumberFormatException nfe) {
				MessageBox mb = new MessageBox(dialog, SWT.ICON_WARNING);
				mb.setText("wrong value");
				mb.setMessage("The port should be a number!");
				mb.open();
			}
		}
	};
	
	Listener bluetoothConnectButtonListener = new Listener(){
		public void handleEvent(Event arg0){
			String [] test = list.getSelection();
			String mac = "";
			for(int i = 1; i < 18; i++){
				mac = mac+test[0].charAt(i);
			}
			try{		
				Runtime.getRuntime().exec( "pand --killall" );
				Runtime.getRuntime().exec( "pand --connect "+mac+" --autozap" );
				System.out.println("pand connected!");
			}
			catch (IOException e) {
				MessageBox mb = new MessageBox(dialog, SWT.ICON_WARNING);
				mb.setText("Error");
				mb.setMessage("Error PAN setup");
				mb.open();	
				e.printStackTrace();
			}	
			Process ifconfig = null;		
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e1) {
				e1.printStackTrace();
			}
			
			try{		
				Runtime.getRuntime().exec("ifconfig bnep0 172.16.0.1");
			
					for(int i= 2; i < 254; i++){
						ifconfig = null;
						ifconfig = Runtime.getRuntime().exec("ping -c 1 172.16.0."+i);

						BufferedReader in = new BufferedReader(new InputStreamReader(ifconfig.getInputStream()));
						in.readLine();
						String temp = in.readLine();
						System.out.println(temp);
						if((temp.equals("From 172.16.0."+i+" icmp_seq=1 Destination Host Unreachable")) || temp.equals("") ){
							System.out.println("bnep0: 172.16.0."+i);
							Runtime.getRuntime().exec("ifconfig bnep0 172.16.0."+i);
							break;
						}
					}
			}
			catch (IOException e) {
					MessageBox mb = new MessageBox(dialog, SWT.ICON_WARNING);
					mb.setText("Error");
					mb.setMessage("Error during interface setup");
					mb.open();
					e.printStackTrace();		
			}		
			try {
				socketTransport = (SocketClientTransport)selectedTransport;
				socketTransport.setHost("172.16.0.1");			
				socketTransport.setPort(2000);
				socketTransport.setPassword(btPasswordEntry.getText());
				motionControlStart();
				MotionControl.start();
				
				statusOk = true;
				dialog.dispose();
			} catch (NumberFormatException nfe) {
				MessageBox mb = new MessageBox(dialog, SWT.ICON_WARNING);
				mb.setText("wrong value");
				mb.setMessage("The port should be a number!");
				mb.open();
			}
		}
	};

	Listener cancelListener = new Listener() {
		public void handleEvent(Event arg0) {
			statusOk = false;
			dialog.dispose();
		}
	};

	public ConnectionDialog(Shell parent, ClientTransport preSelectedTransport) {
		selectedTransport = preSelectedTransport;
		
		dialog = new Shell(parent, SWT.DIALOG_TRIM | SWT.APPLICATION_MODAL);
		dialog.setText ("Connection");
		dialog.setSize (500, 500);
		GridLayout dialogLayout = new GridLayout(2, false);
		dialogLayout.horizontalSpacing = 20;
		dialogLayout.verticalSpacing = 20;
		dialog.setLayout(dialogLayout);
		
		
		final TabFolder tabFolder = new TabFolder (dialog, SWT.NONE);
		TabItem bluetooth = new TabItem (tabFolder, SWT.NONE);
		bluetooth.setText("Bluetooth");
		TabItem lan = new TabItem (tabFolder, SWT.NONE);
		lan.setText("LAN/W-LAN");
		bluetooth.addListener(SWT.Selection, bluetoothListener);
		
		
		// socketConfiguration
		Composite socketConfiguration = new Composite(tabFolder, SWT.NONE);
		socketConfiguration.setLayout(new GridLayout(2, false));
		socketConfiguration.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, true, 2, 1));

		hostEntry = appendFormEntry(socketConfiguration, "Host");
		portEntry = appendFormEntry(socketConfiguration, "Port");
		passwordEntry = appendFormEntry(socketConfiguration, "Password");

		// button bar
		Button cancelButton = new Button(socketConfiguration, SWT.NONE);
		GridData cancelGridData = new GridData(SWT.RIGHT, SWT.BOTTOM, false, false);
		cancelGridData.widthHint = 100;
		cancelGridData.heightHint = 100;
		cancelButton.setLayoutData(cancelGridData);
		cancelButton.setText("Cancel");
		cancelButton.addListener(SWT.Selection, cancelListener);

		Button connectButton = new Button(socketConfiguration, SWT.NONE);
		GridData connectGridData = new GridData(SWT.RIGHT, SWT.BOTTOM, false, false);
		connectGridData.widthHint = 100;
		connectGridData.heightHint = 100;
		connectButton.setLayoutData(connectGridData);
		connectButton.setText("Connect");
		connectButton.addListener(SWT.Selection, connectListener);
		
		lan.setControl(socketConfiguration);
		
		
		//BluetoothConfiguration
		
		Composite bluetoothConfiguration = new Composite(tabFolder, SWT.NONE);
		bluetoothConfiguration.setLayout(new GridLayout(2, false));
		bluetoothConfiguration.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, true, 2, 1));
		
		Composite buttonComposite = new Composite(bluetoothConfiguration, SWT.NONE);
		buttonComposite.setLayout(new GridLayout(2, false));
		buttonComposite.setLayoutData(new GridData(SWT.FILL, SWT.RIGHT, true, true, 2, 1));
		
		Composite passwordComposite = new Composite(bluetoothConfiguration, SWT.NONE);
		passwordComposite.setLayout(new GridLayout(2, false));
		passwordComposite.setLayoutData(new GridData(SWT.FILL, SWT.RIGHT, true, true, 2, 1));
		
		list = new List (bluetoothConfiguration, SWT.BORDER | SWT.MULTI | SWT.V_SCROLL);
		GridData bluetoothConfigurationGridData = new GridData(SWT.RIGHT, SWT.BOTTOM, false, false);
		bluetoothConfigurationGridData.widthHint = 300;
		bluetoothConfigurationGridData.heightHint = 250;
		list.setLayoutData(bluetoothConfigurationGridData);
		
		Button bluetoothConnectButton = new Button(buttonComposite, SWT.PUSH);
		GridData bluetoothConnectButtonGridData = new GridData(SWT.RIGHT, SWT.BOTTOM, false, false);
		bluetoothConnectButtonGridData.widthHint = 150;
		bluetoothConnectButtonGridData.heightHint = 50;
		bluetoothConnectButton.setLayoutData(bluetoothConnectButtonGridData);
		bluetoothConnectButton.setText("Connect");
		bluetoothConnectButton.addListener(SWT.Selection, bluetoothConnectButtonListener);
		
		Button refreshButton = new Button(buttonComposite, SWT.PUSH);
		GridData refreshButtonGridData = new GridData(SWT.RIGHT, SWT.BOTTOM, false, false);
		refreshButtonGridData.widthHint = 150;
		refreshButtonGridData.heightHint = 50;
		refreshButton.setLayoutData(refreshButtonGridData);
		refreshButton.setText("Refresh");
		refreshButton.addListener(SWT.Selection, bluetoothListener);

		Label passwordLabel = new Label(passwordComposite, SWT.NONE);
		passwordLabel.setText("Password:");
		
		btPasswordEntry = new Text(passwordComposite, SWT.BORDER | SWT.MULTI);
		GridData btPasswordEntryGridData = new GridData(SWT.LEFT, SWT.TOP, false, false);
		btPasswordEntryGridData.widthHint = 240;
		btPasswordEntryGridData.heightHint = 20;
		btPasswordEntry.setLayoutData(btPasswordEntryGridData);
		btPasswordEntry.setText("1234");
		
		bluetooth.setControl(bluetoothConfiguration);
		
		
		dialog.pack();
		open();
	}
	
	/**
	 * Returns true, if the user clicked ok/connect and returns false,
	 * if the user has canceled the dialog
	 * @return
	 */
	
	Process proc = null;
	private void btsearch() {
		new Thread(new Runnable() {
			public void run() {
					String first = "";
					System.out.println("Searching for Bluetooth devices");
					try {	
						clearListAsync();
						setNewContentAsync("Searching...");
						proc = Runtime.getRuntime().exec( "hcitool scan --refresh" );
					}  
					catch (IOException e) {			
						e.printStackTrace();
					} 
					BufferedReader in = new BufferedReader(new InputStreamReader(proc.getInputStream()));
					String [] test = new String[100];
					int i = 0;
					try {
						first = in.readLine();					
					} 
					catch (IOException e1) {
						e1.printStackTrace();
					}
					System.out.println(first);		
					System.out.println("Search finished");
					while(true){
						try {
							test[i] = in.readLine();
						} 
						catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						if(test[i] == null) {
							if(i == 0){
								setNewContentAsync("Nothing found");
							}
							break;
						}
						i++;
					} 
					clearListAsync();
					for(int e=0; e < i; e++){ 
						if(test[e] != null){
							setNewContentAsync(test[e]);
							System.out.println(test[e]);
						} 					
					}
					if(first == null){
						setNewContentAsync("No powered bluetooth");
						setNewContentAsync("device found.");
					}
			}			
		}).start();
	}
	
	
	public void setNewContent(String string) {
		list.add(string);
	}

	public void clearListAsync() {
		Display.getDefault().asyncExec(new Runnable() {
			public void run() {
				list.removeAll();
			}			
		});
	}
	
	
	public void setNewContentAsync(final String string) {
		Display.getDefault().asyncExec(new Runnable() {
			public void run() {
				setNewContent(string);
			}			
		});
	}
	
	
	Listener bluetoothListener = new Listener(){
		public void handleEvent(Event arg0) {
			System.out.println("BLuetoothListener");
			btsearch();
		}	
	};
	
	
	public boolean isStatusOk() {
		return statusOk;
	}

	/**
	 * @param socketConfiguration
	 */
	private Text appendFormEntry(Composite socketConfiguration, String label) {
		Label labelComponent = new Label(socketConfiguration, SWT.NONE);
		labelComponent.setText(label);
		Text entry = new Text(socketConfiguration, SWT.SINGLE);
		GridData entryLayout = new GridData(SWT.FILL, SWT.TOP, true, true, 1, 1);
		entryLayout.widthHint = 300;
		entry.setLayoutData(entryLayout);
		return entry;
	}
	
	/**
	 * 
	 * @param currentTransport the transport currently used, may be null
	 */
	private void open() {
		socketTransport = (SocketClientTransport)selectedTransport;
		hostEntry.setText(saveNull(socketTransport.getHost()));
		portEntry.setText(""+socketTransport.getPort());
		passwordEntry.setText(saveNull(socketTransport.getPassword()));
		
		statusOk = false;
		dialog.open();
			Display display = Display.getDefault();
		while (!dialog.isDisposed())
			display.readAndDispatch();
	}

	private String saveNull(String string) {
		return string != null ? string : "";
	}

	public ClientTransport getSelectedTransport() {
		return selectedTransport;
	}

}


