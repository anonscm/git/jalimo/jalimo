package de.tarent.opreco;

import java.io.IOException;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;
import org.eclipse.swt.widgets.Text;

/*
 * Draft
 * Funktionalität:
 * - Button Views für verschiedene Applikationssets
 * - Connection Status, Connect, Disconnect, Shutdown Server
 * - Connection und Transport Konfiguration 
 *
 */

public class ControlClient {
   
	Display display;
	Shell shell;
	Text status;
	Button button;

	ClientTransport transport;
		
	Listener closeListener = new Listener() {
		public void handleEvent(Event arg0) {
			try {
				transport.close();
			} catch (IOException e) {
				System.out.println("error on closing the connection");
				e.printStackTrace();
			}	
			try {		
				Runtime.getRuntime().exec( "pand --killall" );
			}  catch (IOException e) {
				e.printStackTrace();
			}
			System.exit(1);
			shell.dispose();
		}
	};

	Listener connectionListener = new Listener() {
		public void handleEvent(Event arg0) {
			openConnectionDialog();
		}
	};

	Listener disConnectionListener = new Listener() {
		public void handleEvent(Event arg0) {
			try {
				transport.close();
			} catch (IOException e) {
				// TODO: how to handle?
			}
		}
	};
	
	public ControlClient(ClientTransport transport) {
		this.transport = transport;
		createGui();
		createMenu();
	}

	public void start() {		
		// open the main window
		shell.open();	
		openConnectionDialog();
		// do gui dispatching until the main window is closed
		while (!shell.isDisposed())
			display.readAndDispatch();
	}
	
	private void openConnectionDialog() {
		ConnectionDialog connectionDialog = new ConnectionDialog(shell, transport);
		System.out.println("connectionDialog.isStatusOk(): "+connectionDialog.isStatusOk());
		if (connectionDialog.isStatusOk()) {
			transport = connectionDialog.getSelectedTransport();
			connect();
		}	
	}
	
	public void openConnectionDialogAsync() {
		display.asyncExec(new Runnable() {
			public void run() {
				openConnectionDialog();		
			}			
		});
	}
	
	public void publishErrorAsync(final String title, final String text) {
		display.asyncExec(new Runnable() {
			public void run() {
				MessageBox mb = new MessageBox(shell, SWT.ICON_ERROR);
				mb.setText(title);
				mb.setMessage(text);
				mb.open();				
			}			
		});
	}

	private void connect() {
		new Thread(new Runnable() {
			public void run() {
				setStatusAsync("connecting to "+transport +" ...");
				try {
					if (transport.isConnected())
						transport.close();
					if(!transport.connect()){
						openConnectionDialogAsync();
						publishErrorAsync("Bad Password", "Entered wrong password, try again");
					}
				} catch (IOException e) {
					e.printStackTrace();
					setStatusAsync("connection error");
					publishErrorAsync("connection error", e.getMessage());
				}
				setStatusAsync("connected with "+transport +"!");
			}			
		}).start();
	}
	
	public void setStatus(String string) {
		status.setText(string);
	}

	public void setStatusAsync(final String string) {
		display.asyncExec(new Runnable() {
			public void run() {
				setStatus(string);
			}			
		});
	}

	private void createGui() {
		display = Display.getDefault();
		shell = new Shell(display);
		shell.setText("remote key");
		
		GridLayout gridLayout = new GridLayout();
		gridLayout.numColumns = 1;
		shell.setLayout(gridLayout);
		
		Control keyPanel = null;
		if (true == "Windows CE".equals(System.getProperty("os.name"))) {
			keyPanel = createKeyPanelWinCE(shell);			
		} else { 
			keyPanel = createKeyPanel(shell);
		}
		keyPanel.setLayoutData(new GridData(GridData.FILL_BOTH));
		
		status = new Text(shell, SWT.SINGLE);
		status.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		status.setEditable(false);
		status.setText("");
	}

	private Control createKeyPanelWinCE(Composite parent) {
		Composite buttonComposite = new Composite(parent, SWT.NONE);
		FillLayout fillLayout = new FillLayout(SWT.VERTICAL);
		fillLayout.spacing = 10;
		buttonComposite.setLayout(fillLayout);
		createCommandButton(buttonComposite, new Command("<-", "key Left"));
		createCommandButton(buttonComposite, new Command("->", "key Right"));
		return buttonComposite;
	}

	/**
	 * 
	 */
	private Control createKeyPanel(Composite parent) {
		TabFolder tabFolder = new TabFolder(parent, SWT.BORDER);

		
		Composite buttonComposite = new Composite(tabFolder, SWT.NONE);
		FillLayout fillLayout = new FillLayout(SWT.VERTICAL);
		fillLayout.spacing = 10;
		buttonComposite.setLayout(fillLayout);
		TabItem tabItem = new TabItem(tabFolder, SWT.NONE);
		tabItem.setText("Navigation");
		tabItem.setControl(buttonComposite);
		createCommandButton(buttonComposite, new Command("<-", "key Left"));
		createCommandButton(buttonComposite, new Command("->", "key Right"));

		Composite buttonComposite2 = new Composite(tabFolder, SWT.NONE);
		GridLayout layout2 = new GridLayout(2, true);
		layout2.horizontalSpacing = 10;
		layout2.verticalSpacing = 10;
		buttonComposite2.setLayout(layout2);
		TabItem tabItem2 = new TabItem(tabFolder, SWT.NONE);
		tabItem2.setText("Slides");
		tabItem2.setControl(buttonComposite2);
		GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
		createCommandButton(buttonComposite2, new Command("Esc", "key Escape")).setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 2, 1));
		createCommandButton(buttonComposite2, new Command("CTRL-L", "keydown Control_L;key l;keyup Control_L")).setLayoutData(gridData);
		createCommandButton(buttonComposite2, new Command("F5", "key F5")).setLayoutData(gridData);
		createCommandButton(buttonComposite2, new Command("Home", "key Home")).setLayoutData(gridData);
		createCommandButton(buttonComposite2, new Command("PgUp", "key Page_Up")).setLayoutData(gridData);
		createCommandButton(buttonComposite2, new Command("End", "key End")).setLayoutData(gridData);
		createCommandButton(buttonComposite2, new Command("PgDn", "key Page_Down")).setLayoutData(gridData);
		return tabFolder;
	}
	
	Button createCommandButton(Composite parent, Command cmd) {
		Button button = new Button(parent, SWT.NONE);
		button.setText(cmd.getLabel());
		button.addListener(SWT.Selection, cmd);
		return button;		
	}
	
	private void createMenu() {
        Menu bar = new Menu(shell, SWT.BAR);
        shell.setMenuBar(bar);

        MenuItem fileMenuItem = new MenuItem (bar, SWT.CASCADE);
        fileMenuItem.setText ("File");
        Menu fileMenu = new Menu (shell, SWT.DROP_DOWN);
        fileMenuItem.setMenu(fileMenu);

        MenuItem connect = new MenuItem (fileMenu, SWT.PUSH);
        connect.setText("Connect");
        connect.addListener(SWT.Selection, connectionListener);

        MenuItem disConnect = new MenuItem (fileMenu, SWT.PUSH);
        disConnect.setText("Disconnect");
        disConnect.addListener(SWT.Selection, disConnectionListener);

        MenuItem quit = new MenuItem (fileMenu, SWT.NONE);
        quit.setText("Quit");
        quit.addListener(SWT.Selection, closeListener);
	}

	public class Command implements Listener {
		String cmd;
		String label;
		
		public Command(String label, String cmd) {
			this.cmd = cmd;
			this.label = label;
		}
		
		public void handleEvent(Event arg0) {
			if (!transport.isConnected()) {
				openConnectionDialog();
				try {
					Thread.sleep(300);
				} catch (InterruptedException e) {
					// ignore
				}
				if (!transport.isConnected())
					return;
			}
			new Thread(new Runnable() {
				public void run() {
					setStatusAsync("sending "+cmd+"...");
					try {
						transport.sendCommand(cmd);
					} catch (IOException e) {
						e.printStackTrace();
						setStatusAsync("sending error");
						publishErrorAsync("sending error", e.getMessage());
					}
					setStatusAsync("sent "+cmd+"");
				}			
			}).start();
		}

		public String getCmd() {
			return cmd;
		}

		public void setCmd(String cmd) {
			this.cmd = cmd;
		}

		public String getLabel() {
			return label;
		}

		public void setLabel(String label) {
			this.label = label;
		}
	};
}
