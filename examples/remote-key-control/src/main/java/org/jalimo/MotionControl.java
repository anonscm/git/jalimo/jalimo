package de.tarent.opreco;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class MotionControl {
	
	public SocketClientTransport temp;
	public static boolean status = false;
	public MotionControl(SocketClientTransport socketClientTransport){	
		temp = socketClientTransport;
		new Thread(new Runnable() {
			public void run() {	
				try {
					System.out.println("Motiontracking started");
					FileInputStream inputStream = new FileInputStream("/dev/input/event2");
					byte[] input = new byte[48];
					
					while(true){
						if(status){
							try {
								Thread.sleep(2);
							} catch (InterruptedException e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
							
							try {
								inputStream.read(input);
							} catch (IOException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
			
							int x = 256 * input[13] + (input[12] & 0xff);
							int y = 256 * input[29] + (input[28] & 0xff);
							int z = 256 * input[45] + (input[44] & 0xff);
							
							if(x > 2000){
								System.out.println("Next slide");
								sendCommand();
								try {
									Thread.sleep(1000);
								} catch (InterruptedException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							}					
						}
					}
				}
			
				 catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}).start();
	}
	public void sendCommand(){
		new Thread(new Runnable() {
			public void run(){
				try {
					temp.sendCommand("key Right");
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}).start();
	}
	public static void start(){
		status = true;
	}
}


