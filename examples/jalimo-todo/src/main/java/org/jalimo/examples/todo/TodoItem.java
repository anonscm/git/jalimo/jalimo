package org.jalimo.examples.todo;


public class TodoItem {
	
	/**
	 * A short description
	 */
	String subject = "";
	
	/**
	 * Detailed Information about the Todo
	 */
	String detail = "";
	
	
	/**
	 * The priority of the todo 1-3
	 */
	int priotiry = -1;
		
	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getDetail() {
		return detail;
	}

	public void setDetail(String detail) {
		this.detail = detail;
	}

	public int getPriotiry() {
		return priotiry;
	}

	public void setPriotiry(int priotiry) {
		this.priotiry = priotiry;
	}

}
