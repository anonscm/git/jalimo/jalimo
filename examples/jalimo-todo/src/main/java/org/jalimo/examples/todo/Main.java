package org.jalimo.examples.todo;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableItem;

public class Main {

	Display display;
	Shell shell;
	Table table;
	
	/**
	 * listener for closing the main window 
	 */
	Listener closeListener = new Listener() {
		public void handleEvent(Event arg0) {
			shell.dispose();
		}
	};
	
	/**
	 * new item
	 */
	Listener newItemListener = new Listener() {
		public void handleEvent(Event arg0) { 	
			TodoItem newTodoItem = new TodoItem();
			new ItemDialog(shell, SWT.NONE).open(newTodoItem);		
			TableItem item = new TableItem(table, SWT.NONE);
			item.setText(newTodoItem.getSubject());
			item.setData(newTodoItem);
		}
	};

	/**
	 * Item selected by the user
	 */
	SelectionListener itemSelectionListener = new SelectionListener() {
		public void widgetSelected(SelectionEvent event) {
			TableItem item = (TableItem) event.item;
			TodoItem todoItem = (TodoItem)item.getData();
			new ItemDialog(shell, SWT.NONE).open(todoItem);		
			item.setText(todoItem.getSubject());				
		}

		public void widgetDefaultSelected(SelectionEvent arg0) {						
		}		
	};


	public static void main(String[] args) {
		new Main().start();
	}

	/**
	 * Create the main dialog wit the table
	 */
	public Main() {
		display = Display.getDefault();
		shell = new Shell(display);
		shell.setLayout(new FillLayout());
		createMenu();

		shell.setText("Jalimo TODO example");

		table = new Table (shell,  SWT.V_SCROLL);
		table.addSelectionListener(itemSelectionListener);
	}
	
	/**
	 * create the menu bar
	 */
	private void createMenu() {
        Menu bar = new Menu(shell, SWT.BAR);
        shell.setMenuBar(bar);

        MenuItem fileMenuItem = new MenuItem (bar, SWT.CASCADE);
        fileMenuItem.setText ("File");
        Menu fileMenu = new Menu (shell, SWT.DROP_DOWN);
        fileMenuItem.setMenu(fileMenu);

        MenuItem newItem = new MenuItem (fileMenu, SWT.PUSH);
        newItem.setText("New Item");
        newItem.addListener(SWT.Selection, newItemListener);

        MenuItem quit = new MenuItem (fileMenu, SWT.NONE);
        quit.setText("Quit");
        quit.addListener(SWT.Selection, closeListener);
	}

	/**
	 * Start the application
	 */	
	private void start() {
		// open the main window
		shell.open();		
		// do gui dispatching until the main window is closed
		while (!shell.isDisposed())
			display.readAndDispatch();
	}
}
