package org.jalimo.examples.todo;

import net.miginfocom.swt.MigLayout;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Dialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

public class ItemDialog extends Dialog {
	
	private TodoItem currentItem;
	private Shell shell;
	private Text subject;
	private Text detail;
	private Combo priority;
	
	public ItemDialog (Shell parent, int style) {
		super (parent, style);
	}
	
	public ItemDialog (Shell parent) {
		this (parent, 0);
	}

	/**
	 * open the dialog
	 */
	public TodoItem open (TodoItem viewItem) {
		currentItem = viewItem;
		Shell parent = getParent();
		shell = new Shell(parent,  SWT.DIALOG_TRIM | SWT.APPLICATION_MODAL);
		createLayout();
		fill();
		shell.open();
		Display display = parent.getDisplay();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) 
					display.sleep();
		}
		return currentItem;
	}

	/**
	 * create the main layout
	 */
	private void createLayout() {
		shell.setText("Edit todo item");
		shell.setLayout(new MigLayout("fill, wrap 2", "[][grow]"));
		shell.setSize(300, 200);
		
		subject = new Text(shell, SWT.SINGLE | SWT.BORDER);
		subject.setLayoutData("span 2, growx");
		
		Label priorityLabel = new Label(shell, SWT.NONE | SWT.READ_ONLY);
		priorityLabel.setLayoutData("");
		priorityLabel.setText("Priority:");
		priority = new Combo(shell, SWT.NONE);
		priority.add("low");
		priority.add("medium");
		priority.add("high");
		priority.setLayoutData("growx");

		Label detailLabel = new Label(shell, SWT.NONE);
		detailLabel.setLayoutData("");
		detailLabel.setText("Description:");

		detail = new Text(shell, SWT.MULTI | SWT.BORDER);
		detail.setLayoutData("grow");

		Composite buttonBar = new Composite(shell, SWT.NONE);
		buttonBar.setLayout(new MigLayout("debug", "[grow][][]"));
		buttonBar.setLayoutData("span 2, align right");
		
		Button ok = new Button(buttonBar, SWT.PUSH);		
		ok.setText("Ok");
		ok.setLayoutData("align right");
		ok.addListener(SWT.Selection, new Listener() {
			public void handleEvent(Event arg0) {
				save();
				close();
			}
		});

		Button cancel = new Button(buttonBar, SWT.CANCEL);
		cancel.setText("Cancel");
		cancel.setLayoutData("align right");
		cancel.addListener(SWT.Selection, new Listener() {
			public void handleEvent(Event arg0) {
				close();
			}			
		});
	}

	/**
	 * fill the widgets
	 */
	void fill() {
		subject.setText(currentItem.getSubject());
		detail.setText(currentItem.getDetail());
		priority.select(currentItem.getPriotiry()+1);
	}
	
	/**
	 * fill the todo item
	 */
	void save() {
		currentItem.setSubject(subject.getText());
		currentItem.setDetail(detail.getText());
		currentItem.setPriotiry(priority.getSelectionIndex() -1);
	}
	
	/**
	 * close the dialog
	 */
	void close() {
		shell.dispose();
	}
}
