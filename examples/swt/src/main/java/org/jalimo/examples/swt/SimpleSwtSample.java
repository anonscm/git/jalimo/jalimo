package org.jalimo.examples.swt;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;

public class SimpleSwtSample {

	public static void main(String[] args) {
		
		// get the display
		Display display = Display.getDefault();
		
		// create the main window
		final Shell shell = new Shell(display);
		shell.setLayout(new RowLayout(SWT.VERTICAL));
		
		// create and add the label
		Label label = new Label(shell, SWT.CENTER);
		label.setText("Hello maemo");
		
		// create and add the button
		Button button = new Button(shell, SWT.NONE);
		button.setText("close");
		
		// add a click listener to close the main window
		button.addListener(SWT.Selection, new Listener() {
			public void handleEvent(Event arg0) {
				shell.dispose();
			}
		});
		
		// open the main window		
		shell.open();		
		
		// start the gui
		while (!shell.isDisposed())
			if (!display.readAndDispatch())
				display.sleep();
	}
}
