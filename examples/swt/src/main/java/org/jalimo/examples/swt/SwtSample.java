package org.jalimo.examples.swt;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;

public class SwtSample {

	Display display;
	Shell shell;
	Label imageLabel;
	Button button;
	
	/**
	 * listener for closing the main window 
	 */
	Listener closeListener = new Listener() {
		public void handleEvent(Event arg0) {
			shell.dispose();
		}
	};

	/**
	 * listener the button click event
	 */
	Listener smileListener = new Listener() {
		public void handleEvent(Event arg0) {
			imageLabel.setVisible(true);
			button.setText("close");
			button.removeListener(SWT.Selection, this);
			button.addListener(SWT.Selection, closeListener);
			shell.layout(true);
		}
	};

	/**
	 * listener the button click event
	 */
	Listener infoListener = new Listener() {
		public void handleEvent(Event arg0) { 		
			MessageBox infoBox = new MessageBox(shell, SWT.ICON_INFORMATION | SWT.OK );
			infoBox.setText("Info");
			infoBox.setMessage("This is an Jalimo SWT example. \n\n       www.jalimo.org\n"
								+"\njava version: "+System.getProperty("java.version")
								+"\nvm: "+System.getProperty("java.vm.vendor")+", "+System.getProperty("java.vm.version")
								+"\njava: "+System.getProperty("java.vendor"));
			infoBox.open();
		}
	};

	public static void main(String[] args) {
		new SwtSample().start();
	}

	public SwtSample() {
		display = Display.getDefault();
		shell = new Shell(display);
		shell.setText("Jalimo SWT example");
		RowLayout rowLayout = new RowLayout(SWT.VERTICAL);
		rowLayout.marginTop = 30;		
		rowLayout.marginLeft = 30;
		rowLayout.spacing = 10;
		shell.setLayout(rowLayout);
		
		imageLabel = new Label(shell, SWT.CENTER);
		imageLabel.setVisible(false);
		imageLabel.setImage(new Image(display, SwtSample.class.getResourceAsStream("face-glasses.png")));

		Label label = new Label(shell, SWT.CENTER);
		label.setText("Hello Jalimo!");

		button = new Button(shell, SWT.NONE);
		button.setText("click me");
		button.addListener(SWT.Selection, smileListener);
		
		createMenu();
	}
	
	private void createMenu() {
        Menu bar = new Menu(shell, SWT.BAR);
        shell.setMenuBar(bar);

        MenuItem fileMenuItem = new MenuItem (bar, SWT.CASCADE);
        fileMenuItem.setText ("File");
        Menu fileMenu = new Menu (shell, SWT.DROP_DOWN);
        fileMenuItem.setMenu(fileMenu);

        MenuItem info = new MenuItem (fileMenu, SWT.PUSH);
        info.setText("Info");
        info.addListener(SWT.Selection, infoListener);

        MenuItem quit = new MenuItem (fileMenu, SWT.NONE);
        quit.setText("Quit");
        quit.addListener(SWT.Selection, closeListener);
	}

	/**
	 * Start the application
	 */	
	private void start() {
		// open the main window
		shell.open();		
		// do gui dispatching until the main window is closed
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch())
				display.sleep();
		}
	}
}
