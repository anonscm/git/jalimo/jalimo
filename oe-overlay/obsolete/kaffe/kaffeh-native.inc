DEPENDS = "virtual/javac-native zziplib-native"

SRC_URI = "ftp://ftp.kaffe.org/pub/kaffe/v1.1.x-development/kaffe-${PV}.tar.bz2"

S = "${WORKDIR}/kaffe"

inherit autotools native

EXTRA_OECONF = "\
	--with-javac \
	--disable-debug \
	--disable-gcj \
	--disable-plugin \
	--disable-gconf-peer \
	--with-classpath-libdir=${STAGING_LIBDIR}/classpath \
	--with-classpath-includedir=${STAGING_INCDIR}/classpath \
	--with-classpath-classes=${STAGING_DATADIR}/classpath/glibj.zip \
	"

do_compile() {
	#	Just compile kaffeh and save us some time. :)
	make -C kaffe/kaffeh kaffeh
}

do_stage() {
	install -m 0755 kaffe/kaffeh/.libs/kaffeh ${STAGING_BINDIR}/kaffeh-${PV}
}

