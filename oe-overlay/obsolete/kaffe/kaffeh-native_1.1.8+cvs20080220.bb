require kaffeh-native.inc

SRCDATE = "20080220"

SRC_URI = "\
	cvs://readonly:readonly@cvs.kaffe.org/cvs/kaffe;module=kaffe;date=${SRCDATE} \
	"

S = "${WORKDIR}/kaffe"
