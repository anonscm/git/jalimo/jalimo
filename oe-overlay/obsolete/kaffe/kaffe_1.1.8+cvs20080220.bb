require kaffe.inc

SRCDATE = "20080220"

SRC_URI = "\
	cvs://readonly:readonly@cvs.kaffe.org/cvs/kaffe;module=kaffe;date=${SRCDATE} \
	file://kaffe-mkdir-p-fix.patch;patch=1 \
	"

S = "${WORKDIR}/kaffe"
