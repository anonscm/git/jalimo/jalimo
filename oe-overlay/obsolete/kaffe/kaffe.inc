DESCRIPTION = "Kaffe is a clean room implementation of the Java Virtual Machine"
HOMEPAGE = "http://www.kaffe.org/"
LICENSE  = "GPL LGPL W3C BSD"

SRC_URI = "ftp://ftp.kaffe.org/pub/kaffe/v1.1.x-development/kaffe-${PV}.tar.bz2"

DEPENDS = "libtool classpath virtual/javac-native fastjar-native zip-native kaffeh-native zziplib"
RDEPENDS_${PN} = "${PN}-common (>= ${PV}) classpath"
RPROVIDES_kaffe = "java2-runtime"

inherit autotools gettext java

EXTRA_OECONF = " \
#	--with-libffi \
  --with-classpath-prefix=${prefix} \
	--with-classpath-includedir=${STAGING_INCDIR}/classpath \
	--with-classpath-classes=${STAGING_DATADIR}/classpath/glibj.zip \
	--with-jni-library-path=${libdir_jni} \
	--with-target-classpath-classes=${datadir}/classpath/glibj.zip \
	--with-classpath-libdir=${libdir} \
	--with-engine=intrp \
	"

do_configure_prepend() {
  # Fixes some coreutils 5.3 incompatibility.
  sed -i -e "s|head -1|head -n 1|" aclocal.m4
}

export KAFFEH="${STAGING_BINDIR_NATIVE}/kaffeh-${PV}"

export JAVAC="javac"

# Kaffe folds itself into the typical JDK layout. Apply a similar workaround to 
# the one used in Debian in order to make it behave unixy.
oe_runconf () {
	if [ -x ${S}/configure ] ; then
		cfgcmd="${S}/configure \
		    --build=${BUILD_SYS} \
		    --host=${HOST_SYS} \
		    --target=${TARGET_SYS} \
		    --prefix=${libdir}/${PN} \
		    --datadir=${datadir} \
		    --sysconfdir=${sysconfdir} \
		    --sharedstatedir=${sharedstatedir}/${PN} \
		    --includedir=${includedir} \
		    --oldincludedir=${oldincludedir} \
		    --infodir=${infodir} \
		    --mandir=${mandir} \
			${EXTRA_OECONF} \
		    $@"
		oenote "Running $cfgcmd..."
		$cfgcmd || oefatal "oe_runconf failed" 
	else
		oefatal "no configure script found"
	fi
}

PACKAGES =+ "${PN}-common"
FILES_${PN}-dev += "${libdir}/kaffe/jre/lib/*/libkaffevm.so ${libdir}/kaffe/jre/lib/*/libkaffe.so"
FILES_${PN}-dbg += "${libdir}/kaffe/bin/.debug ${libdir}/kaffe/jre/bin/.debug ${libdir}/kaffe/jre/lib/*/.debug"
FILES_${PN} += "${libdir}/${PN}"
FILES_${PN}-common = "${libdir}/${PN}/jre/lib/*.jar"

do_install () {
	autotools_do_install

	# move partially duplicate stuff to a single location
  mv -f ${D}${libdir}/${PN}/bin/* ${D}${libdir}/${PN}/jre/bin
  rmdir ${D}${libdir}/${PN}/bin
  cd ${D}${libdir}/${PN} && ln -s jre/bin
}

ALTERNATIVE_NAME = "java"
ALTERNATIVE_LINK = "${bindir}/${ALTERNATIVE_NAME}"
ALTERNATIVE_PATH = "${libdir}/kaffe/jre/bin/kaffe"
ALTERNATIVE_PRIORITY = "10"

