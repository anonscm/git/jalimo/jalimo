DESCRIPTION = "Meta-package for Jalimo Feed Items"
LICENSE = "GPL"

PR = "r8"

inherit meta

# The package-index at the end causes regeneration of the Packages.gz and
# other control files.
DEPENDS = "task-jalimo package-index"

# Workaround to prevent package-index being built since that depends on IPK-specific functions
# which we do not have.
DEPENDS_nokia800 = "task-jalimo"
DEPENDS_nokia770 = "task-jalimo"
