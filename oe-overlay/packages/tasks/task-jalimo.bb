DESCRIPTION = "Jalimo: Feed Items"
SECTION = "jalimo/base"
LICENSE = "GPL"
ALLOW_EMPTY = "1"

PR = "r10"

inherit task

PACKAGES = "jalimo-task-base"

RDEPENDS_jalimo-task-base = "\
  jamvm \
  cacao \
  classpath \
  libswt3.4-gtk-java \
  midpath \
  midpath-demos \
  midpath-backend-alsa \
  midpath-backend-esd \
  midpath-backend-pulseaudio \
  midpath-backend-fb \
  midpath-backend-gtk \
  midpath-backend-escher \
  midpath-backend-sdl \
  libdbus-java \
  librxtx-java \
  logic-analyzer \
  icommand-bluez-jni \
  openjdk-langtools-native \
	phoneme-advanced-foundation \
  liblog4j1.2-java \
"

# Troublesome recipes:
#  libhsqldb-java \
#  libjava-gnome-java \

RDEPENDS_jalimo-task-base_er0100 += "\
  er-refresh-tools \
  er-fake-libs \
  "

