require java-gnome.inc

PR = "r2"

REV = "311"

SRC_URI = "\
  bzr://research.operationaldynamics.com/bzr/java-gnome/mainline;protocol=http;rev=${REV} \
  file://configure_cross_compiling.patch;patch=1 \
	file://jalimo-gui-extensions-2007-08-21.patch;patch=1 \
  "

S = "${WORKDIR}/mainline"

