DESCRIPTION = "Java bindings for gtk and gnome"
HOMEPAGE = "http://java-gnome.sf.net/"
LICENSE = "GPL + Linking Exception"
SECTION = "libs"

DEPENDS = "perl-native python-native gtk+ libglade"

inherit autotools java-library

PROVIDES = "java-gnome"

# A number that is appended to the Jars and shared libraries.
APIVERSION = "4.0"

do_install() {
  oe_jarinstall tmp/gtk-${APIVERSION}.jar java-gnome.jar

  install -d ${D}${libdir_jni}
  oe_libinstall -so -C tmp libgtkjni-${PV} ${D}/${libdir_jni}
}

do_configure_prepend () {
	export JNI_H_HOME=${STAGING_INCDIR_NATIVE}/classpath
}

do_stage() {
  oe_jarinstall -s tmp/gtk-${APIVERSION}.jar java-gnome.jar
}

PACKAGES += "lib${PN}-jni"
 
FILES_lib${PN}-jni = "${libdir_jni}/libgtkjni-*.so*"

RPROVIDES_${JPN} = "libjava-gnome-java"
RDEPENDS_${JPN} = "lib${PN}-jni"
