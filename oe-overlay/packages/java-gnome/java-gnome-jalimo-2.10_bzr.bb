require java-gnome.inc 

PV = "4.0.3+bzr${SRCDATE}"

SRC_URI = "\
  bzr://research.operationaldynamics.com/bzr/java-gnome/mainline;protocol=http \
  file://configure_cross_compiling.patch;patch=1 \
	file://jalimo-gui-extensions-2007-08-21.patch;patch=1 \
  "

S = "${WORKDIR}/mainline"
