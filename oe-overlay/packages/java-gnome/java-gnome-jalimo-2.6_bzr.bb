require java-gnome.inc 

# Specify dependencies manually to be able to install on machines where
# we cannot modify the system libraries. (e.g. iRex iliad)
EXCLUDE_FROM_SHLIBS = "1"
RDEPENDS_${PN}-jni = "gtk+ (>= 2.6.7)"

PV = "4.0.3+bzr${SRCDATE}"

SRC_URI = "\
  bzr://research.operationaldynamics.com/bzr/java-gnome/mainline;protocol=http \
  file://configure_cross_compiling.patch;patch=1 \
	file://gtk+2.6_gslice_backport.patch;patch=1 \
	file://gtk+2.6_backport.patch;patch=1 \
	file://glib2.6_backport_HACK.patch;patch=1 \
	file://gtk+2.6_gnome-modules.patch;patch=1 \
	file://2.6_backport_StatusIcon.patch;patch=1 \
	file://pango_layoutline_hack.patch;patch=1 \
	file://jalimo-gui-extensions-2007-08-21.patch;patch=1 \
  "

S = "${WORKDIR}/mainline"


