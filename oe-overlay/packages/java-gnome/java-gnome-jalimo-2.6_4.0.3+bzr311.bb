require java-gnome.inc

# Specify dependencies manually to be able to install on machines where
# we cannot modify the system libraries. (e.g. iRex iliad)
EXCLUDE_FROM_SHLIBS = "1"
RDEPENDS_${PN}-jni = "gtk+ (>= 2.6.7)"

PR = "r3"

REV=311

SRC_URI = "\
  bzr://research.operationaldynamics.com/bzr/java-gnome/mainline;protocol=http;rev=${REV} \
  file://configure_cross_compiling.patch;patch=1 \
	file://gtk+2.6_gslice_backport.patch;patch=1 \
	file://gtk+2.6_backport.patch;patch=1 \
	file://glib2.6_backport_HACK.patch;patch=1 \
	file://gtk+2.6_gnome-modules.patch;patch=1 \
	file://2.6_backport_StatusIcon.patch;patch=1 \
	file://pango_layoutline_hack.patch;patch=1 \
	file://attachoptions.patch;patch=1;pnum=0 \
	file://iconsize.patch;patch=1;pnum=0 \
	file://table.patch;patch=1;pnum=0 \
	file://toolbarstyle.patch;patch=1;pnum=0 \
	file://tooltips.patch;patch=1;pnum=0 \
	file://widget.patch;patch=1;pnum=0 \
	file://adjustment.patch;patch=1;pnum=0 \
	file://dialog.patch;patch=1;pnum=0 \
	file://filechooserdialog.patch;patch=1;pnum=0 \
	file://scrolledwindow.patch;patch=1;pnum=0 \
	file://window-helpmethods.patch;patch=1;pnum=0 \
  file://eventkey.patch;patch=1;pnum=0 \
  "

S = "${WORKDIR}/mainline"
