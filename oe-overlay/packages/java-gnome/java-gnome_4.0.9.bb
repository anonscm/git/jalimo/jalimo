require java-gnome.inc 

SRC_URI = "http://ftp.gnome.org/pub/gnome/sources/java-gnome/4.0/java-gnome-${PV}.tar.gz\
	file://configure_cross_compiling_4.0.9.patch;patch=1 \
	file://no-junit.patch;patch=1 \
	file://older-gtk.patch;patch=1 \
        "
