require java-gnome.inc 

SRC_URI = "http://ftp.gnome.org/pub/gnome/sources/java-gnome/4.0/java-gnome-4.0.3.tar.gz\
	file://configure_cross_compiling.patch;patch=1 \
	file://gtk+2.6_gslice_backport.patch;patch=1 \
	file://gtk+2.6_backport.patch;patch=1 \
	file://glib2.6_backport_HACK.patch;patch=1 \
	file://gtk+2.6_gnome-modules.patch;patch=1 \
	file://pango_layoutline_hack.patch;patch=1 \
        "
