DESCRIPTION = "ER fake libraries"
HOMEPAGE = "http://not-yet.org"
SECTION = "libs"
PRIORITY = "optional"
LICENSE = "LGPL"

RDEPENDS_${PN} = ""

inherit autotools

SRC_URI = "http://www.inf.fu-berlin.de/~rschuste/${PN}-${PV}.tar.gz"

do_stage() {
        install -d ${STAGING_INCDIR}/liberdm
        install -m 0644 include/liberdm/*.h ${STAGING_INCDIR}/liberdm

        install -d ${STAGING_INCDIR}/liberipc
        install -m 0644 include/liberipc/*.h ${STAGING_INCDIR}/liberipc

        oe_libinstall -so -C src/liberdm liberdm ${STAGING_LIBDIR}
        oe_libinstall -so -C src/liberipc liberipc ${STAGING_LIBDIR}
}

do_install() {
 :
}

