DESCRIPTION = "ER refresh tools"
HOMEPAGE = "http://not-yet.org"
SECTION = "tools"
PRIORITY = "optional"
LICENSE = "GPLv2"

DEPENDS = "er-fake-libs"

inherit autotools

SRC_URI = "http://www.inf.fu-berlin.de/~rschuste/${PN}-${PV}.tar.gz"
