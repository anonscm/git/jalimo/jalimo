DESCRIPTION = "Java class interface and class generator from qtjambi"
HOMEPAGE = "http://www.trolltech.com"
PRIORITY = "optional"
LICENSE = "GPL"
PR = "r0"

SRC_URI = "ftp://ftp.trolltech.com/qtjambi/source/qtjambi-src-gpl-${PV}_01.tar.gz"

S = "${WORKDIR}/qtjambi-src-gpl-${PV}_01"

inherit qt4x11 native

#CFLAGS +="-I${STAGING_INCDIR}/qt4"

do_configure() {
	cd ${S}/generator
	qmake2

	cd ${S}/juic
	qmake2
}

do_compile() {
	oe_runmake -C ${S}/generator
	oe_runmake -C ${S}/juic
}

do_stage() {
	install -d ${STAGING_BINDIR}
	install -m 0755 bin/juic ${STAGING_BINDIR}
	install -m 0755 generator/generator ${STAGING_BINDIR}
}
