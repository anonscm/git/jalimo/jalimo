DESCRIPTION = "Qt bindings for Java"
DEPENDS = "juic-native virtual/javac-native fastjar-native"
HOMEPAGE = "http://www.trolltech.com"
SECTIONS = "libs"
PRIORITY = "optional"
LICENSE = "GPL"
PR = "r0"

SRC_URI = "ftp://ftp.trolltech.com/qtjambi/source/qtjambi-gpl-src-${PV}_01.tar.gz"

S = "${WORKDIR}/qtjambi-gpl-src-${PV}_01"

inherit qt4x11

export JAVAINCDIR=${STAGING_INCDIR}/classpath
export QTDIR=${STAGING_INCDIR}/qt4

do_compile() {
  (cd ${S}/generator && generator)

  qmake -r
  oe_runmake

  juic -cp .

  javac -J-mx1024m @java_files
}

do_stage() {
}
