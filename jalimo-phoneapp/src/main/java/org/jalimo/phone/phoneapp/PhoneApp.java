package org.jalimo.phone.phoneapp;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

public class PhoneApp {

    protected static final String KEY_BACKSPACE = "<";
    Text number;
	private int appHeight = 640;
	private int appWidth = 480;

	private Font mediumFont;
	private Font bigFont;

	/**
	 * Key Listener
	 */
	Listener keyListener = new Listener() {
		public void handleEvent(Event event) {
			String key = (String)event.widget.getData();
			if (key == null)
				return;
			System.out.println(key);
			if (KEY_BACKSPACE.equals(key)) {
				if (number.getText().length() > 0) {
					String oldText = number.getText();
					number.setText(oldText.substring(0, oldText.length()-1));
				}				
			} else {
				number.append(key);
			}
		}
	};

	Listener dialListener = new Listener() {
		public void handleEvent(Event event) {
			System.out.println("dialing number");			
		}
	};

	Listener cancelListener = new Listener() {
		public void handleEvent(Event event) {
			System.exit(0);			
		}
	};
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {	
		new PhoneApp().start();
	}

	private void start() {
	    Display.setAppName("PhoneApp");
	    Display display = new Display();

	    Shell shell = new Shell(display);


	    createGUI(shell);

	    shell.open();

	    while (!shell.isDisposed())
		{
	      if (!display.readAndDispatch())
	        display.sleep();
		}		
	}

	private void createGUI(Shell shell) {
		shell.setSize(appWidth, appHeight);
		GridLayout shellLayout = new GridLayout(1, false);
		shell.setLayout(shellLayout);
		
		number = new Text(shell, SWT.CENTER);
		number.setText("");
		number.setFont(getBigBoldFont(number));
		number.setEditable(false);
		number.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, true, 1, 1));
		
		Control keyPad = createKeyPad(shell);
		keyPad.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, true, true, 1, 1));
		
		Control actionBar = createActionBar(shell);
		actionBar.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
	}

	private Control createKeyPad(Shell shell) {
		int buttonWidth = appWidth / 3 - 30;
		int buttonHeight = (int)(buttonWidth * 0.6);
		
		Composite keyPad = new Composite(shell, SWT.NONE);
		GridLayout keyPadLayout = new GridLayout(3, true);
		keyPadLayout.horizontalSpacing = (appWidth-buttonWidth*3)/4;
		keyPadLayout.verticalSpacing = keyPadLayout.horizontalSpacing/2;
		keyPad.setLayout(keyPadLayout);
		
		for (int i = 1; i<=9; i++) {
			Control keyButton = createKeyButton(keyPad, i+"", i+"");
			keyButton.setLayoutData(new GridData(buttonWidth, buttonHeight));
		}
		
		Control keyButton = createKeyButton(keyPad, "", "");
		keyButton.setLayoutData(new GridData(buttonWidth, buttonHeight));
		
		keyButton = createKeyButton(keyPad, "0", "0");
		keyButton.setLayoutData(new GridData(buttonWidth, buttonHeight));
		
		keyButton = createKeyButton(keyPad, KEY_BACKSPACE, KEY_BACKSPACE);
		keyButton.setLayoutData(new GridData(buttonWidth, buttonHeight));
		
		return keyPad;
	}

	/**
	 * @param keyPad
	 * @param i
	 */
	private Control createKeyButton(Composite keyPad, String caption, Object data) {
		Button keyButton = new Button(keyPad, SWT.NONE);
		keyButton.setText(caption);
		keyButton.setData(data);
		keyButton.addListener(SWT.Selection, keyListener);
		keyButton.setFont(getMediumFont(keyButton));
		return keyButton;
	}


	private Control createActionBar(Shell shell) {
		Composite actionBar = new Composite(shell, SWT.NONE);
		GridLayout actionBarLayout = new GridLayout(2, true);
		actionBar.setLayout(actionBarLayout);

		Button cancelButton = new Button(actionBar, SWT.NONE);
		cancelButton.setText("X");
		cancelButton.addListener(SWT.Selection, cancelListener);
		cancelButton.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		cancelButton.setFont(getMediumFont(cancelButton));

		Button dialButton= new Button(actionBar, SWT.NONE);
		dialButton.setText("dial");
		dialButton.addListener(SWT.Selection, dialListener);
		dialButton.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		dialButton.setFont(getMediumFont(dialButton));

		return actionBar;
	}
	

	private Font getMediumFont(Control control) {
		if (mediumFont == null) {		
			Font initialFont = number.getFont();
			FontData[] fontData = initialFont.getFontData();
			for (int i = 0; i < fontData.length; i++) {
				fontData[i].setHeight(14);
			}
			mediumFont = new Font(control.getDisplay(), fontData);
		}
		return mediumFont;
	}

	private Font getBigBoldFont(Control control) {
		if (bigFont == null) {		
			Font initialFont = number.getFont();
			FontData[] fontData = initialFont.getFontData();
			for (int i = 0; i < fontData.length; i++) {
				fontData[i].setHeight(18);
				fontData[i].setStyle(SWT.BOLD);
			}
			bigFont = new Font(control.getDisplay(), fontData);
		}
		return bigFont;
	}
}
