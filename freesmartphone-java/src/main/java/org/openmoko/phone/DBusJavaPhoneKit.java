/*
 * freesmartphone-java - Copyright (C) 2008 Guillaume Legris
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License version
 * 2 only, as published by the Free Software Foundation. 
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License version 2 for more details (a copy is
 * included at /legal/license.txt). 
 * 
 * You should have received a copy of the GNU General Public License
 * version 2 along with this work; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA 
 * 
 * Linking this library statically or dynamically with other modules is
 * making a combined work based on this library.  Thus, the terms and
 * conditions of the GNU General Public License cover the whole
 * combination.
 *
 * As a special exception, the copyright holders of this library give you
 * permission to link this library with independent modules to produce an
 * executable, regardless of the license terms of these independent
 * modules, and to copy and distribute the resulting executable under
 * terms of your choice, provided that you also meet, for each linked
 * independent module, the terms and conditions of the license of that
 * module.  An independent module is a module which is not derived from
 * or based on this library.  If you modify this library, you may extend
 * this exception to your version of the library, but you are not
 * obligated to do so.  If you do not wish to do so, delete this
 * exception statement from your version.
 */
package org.openmoko.phone;

import org.freedesktop.dbus.DBusConnection;
import org.freedesktop.dbus.DBusSigHandler;
import org.freedesktop.dbus.DBusSignal;
import org.freedesktop.dbus.exceptions.DBusException;
import org.openmoko.PhoneKit.Pair;
import org.openmoko.PhoneKit.Sms;
import org.openmoko.PhoneKit.Dialer.HungUp;
import org.openmoko.PhoneKit.Dialer.IncomingCall;
import org.openmoko.PhoneKit.Dialer.OutgoingCall;
import org.openmoko.PhoneKit.Dialer.Rejected;
import org.openmoko.PhoneKit.Dialer.Talking;
import org.openmoko.PhoneKit.Network.ProviderChanged;
import org.openmoko.PhoneKit.Network.StatusChanged;
import org.openmoko.PhoneKit.Network.SubscriberNumberChanged;

public class DBusJavaPhoneKit extends PhoneKit {
	
	private DBusConnection connection;
	
	private SMS smsInstance;
	private Network networkInstance;
	private Dialer dialerInstance;
	
	DBusJavaPhoneKit() throws DBusException {
		connection = DBusConnection.getConnection(DBusConnection.SESSION);
	}
	
	public SMS getSMSInstance() throws Exception {
		if (smsInstance == null) {
			smsInstance = new SMSImpl(connection);
		}
		return smsInstance;
	}
	
	public Network getNetworkInstance() throws Exception {
		if (networkInstance == null) {
			networkInstance = new NetworkImpl(connection);
		}
		return networkInstance;
	}
	
	public Dialer getDialerInstance() throws Exception {
		if (dialerInstance == null) {
			dialerInstance = new DialerImpl(connection);
		}
		return dialerInstance;
	}

	public void close() {
		connection.disconnect();
	}
	
	/**
	 * SMSImpl inner class
	 */
	private class SMSImpl implements DBusSigHandler, SMS {

		private Sms sms;
		private SMSEventListener smsListener;

		SMSImpl(DBusConnection conn) throws DBusException {
			sms = conn.getRemoteObject("org.openmoko.PhoneKit", "/org/openmoko/PhoneKit/Sms", Sms.class);
			
			/* Register signal Handlers */
			conn.addSigHandler(Sms.PhoneMemoryState.class, this);
			conn.addSigHandler(Sms.SimMemoryState.class, this);
			conn.addSigHandler(Sms.StatusChanged.class, this);
		}

		/* (non-Javadoc)
		 * @see org.openmoko.phone.SMS#send(java.lang.String, java.lang.String, boolean)
		 */
		public void send(String number, String message, boolean report) throws Exception {
			sms.Send(number, message, report);
		}


		/* (non-Javadoc)
		 * @see org.openmoko.phone.SMS#getMemoryStatus()
		 */
		public MemoryStatus getMemoryStatus() throws Exception {
			Pair<Boolean, Boolean> pair = sms.GetMemoryStatus();
			MemoryStatus status = new MemoryStatus();
			status.simFull = pair.a;
			status.phoneFull = pair.b;
			return status;
		}

		/* (non-Javadoc)
		 * @see org.openmoko.phone.SMS#addSMSListener(org.openmoko.phone.SMSEventListener)
		 */
		public void addSMSListener(SMSEventListener listener) {
			this.smsListener = listener;
		}

		public void handle(DBusSignal s) {
			if (s instanceof Sms.StatusChanged) {
				fireStatusChanged(((Sms.StatusChanged) s).status);
			} else if (s instanceof Sms.PhoneMemoryState) {
				firePhoneMemoryStateChanged(((Sms.PhoneMemoryState) s).full);
			} else if (s instanceof Sms.SimMemoryState) {
				fireSimMemoryStateChanged(((Sms.SimMemoryState) s).full);
			}
		}

		private void fireStatusChanged(int status) {
			if (smsListener != null)
				smsListener.statusChanged(status);
		}

		private void fireSimMemoryStateChanged(boolean full) {
			if (smsListener != null)
				smsListener.simMemoryStateChanged(full);
		}

		private void firePhoneMemoryStateChanged(boolean full) {
			if (smsListener != null)
				smsListener.phoneMemoryStateChanged(full);
		}

	}
	
	/**
	 * NetworkImpl inner class
	 */
	private class NetworkImpl implements DBusSigHandler, Network {

		private org.openmoko.PhoneKit.Network network;
		private NetworkEventListener networkListener;

		NetworkImpl(DBusConnection conn) throws DBusException {
			network = conn.getRemoteObject("org.openmoko.PhoneKit", "/org/openmoko/PhoneKit/Network",
					org.openmoko.PhoneKit.Network.class);
			
			/* Register signal Handlers */
			conn.addSigHandler(org.openmoko.PhoneKit.Network.StatusChanged.class, this);
			conn.addSigHandler(org.openmoko.PhoneKit.Network.SubscriberNumberChanged.class, this);
			conn.addSigHandler(org.openmoko.PhoneKit.Network.ProviderChanged.class, this);
		}

		/* (non-Javadoc)
		 * @see org.openmoko.phone.Network#getCountryCode()
		 */
		public String getCountryCode() throws Exception {
			return network.GetCountryCode();
		}

		/* (non-Javadoc)
		 * @see org.openmoko.phone.Network#getHomeCountryCode()
		 */
		public String getHomeCountryCode() throws Exception {
			return network.GetHomeCountryCode();
		}

		/* (non-Javadoc)
		 * @see org.openmoko.phone.Network#getImsi()
		 */
		public String getImsi() throws Exception {
			return network.GetImsi();
		}

		/* (non-Javadoc)
		 * @see org.openmoko.phone.Network#getProviderName()
		 */
		public String getProviderName() throws Exception {
			return network.GetProviderName();
		}

		/* (non-Javadoc)
		 * @see org.openmoko.phone.Network#getSubscriberNumber()
		 */
		public String getSubscriberNumber() throws Exception {
			return network.GetSubscriberNumber();
		}

		/* (non-Javadoc)
		 * @see org.openmoko.phone.Network#addNetworkListener(org.openmoko.phone.NetworkEventListener)
		 */
		public void addNetworkListener(NetworkEventListener listener) {
			this.networkListener = listener;
		}

		public void handle(DBusSignal s) {
			if (s instanceof SubscriberNumberChanged) {
				fireSubscriberNumberChanged(((SubscriberNumberChanged) s).number);
			} else if (s instanceof ProviderChanged) {
				fireProviderChanged(((ProviderChanged) s).name);
			} else if (s instanceof Sms.SimMemoryState) {
				fireStatusChanged(((StatusChanged) s).status);
			}
		}

		private void fireSubscriberNumberChanged(String number) {
			if (networkListener != null)
				networkListener.subscriberNumberChanged(number);
		}

		private void fireProviderChanged(String name) {
			if (networkListener != null)
				networkListener.providerChanged(name);
		}

		private void fireStatusChanged(int status) {
			if (networkListener != null)
				networkListener.statusChanged(status);
		}

	}
	
	
	/**
	 * DialerImpl inner class
	 */
	private class DialerImpl implements DBusSigHandler, Dialer {

		private org.openmoko.PhoneKit.Dialer dialer;
		private DialerEventListener dialerListener;

		DialerImpl(DBusConnection conn) throws DBusException {
			dialer = conn.getRemoteObject("org.openmoko.PhoneKit", "/org/openmoko/PhoneKit/Dialer",
					org.openmoko.PhoneKit.Dialer.class);
			
			/* Register signal Handlers */
			conn.addSigHandler(org.openmoko.PhoneKit.Dialer.HungUp.class, this);
			conn.addSigHandler(org.openmoko.PhoneKit.Dialer.IncomingCall.class, this);
			conn.addSigHandler(org.openmoko.PhoneKit.Dialer.OutgoingCall.class, this);
			conn.addSigHandler(org.openmoko.PhoneKit.Dialer.Rejected.class, this);
			conn.addSigHandler(org.openmoko.PhoneKit.Dialer.StatusChanged.class, this);
			conn.addSigHandler(org.openmoko.PhoneKit.Dialer.Talking.class, this);
		}
		
		public void dial(String number) {
			dialer.Dial(number);
		}

		public void hangUp(String message) {
			dialer.HangUp(message);
		}

		public void addDialerEventListener(DialerEventListener listener) {
			this.dialerListener = listener;
		}

		public void handle(DBusSignal s) {
			if (s instanceof HungUp) {
				fireHungUp();
			} else if (s instanceof IncomingCall) {
				fireIncomingCall(((IncomingCall) s).number);
			} else if (s instanceof OutgoingCall) {
				fireOutgoingCall(((OutgoingCall) s).number);
			} else if (s instanceof StatusChanged) {
				fireStatusChanged(((StatusChanged) s).status);
			} else if (s instanceof Rejected) {
				fireRejected();
			} else if (s instanceof Talking) {
				fireTalking();
			}
		}

		private void fireHungUp() {
			if (dialerListener != null)
				dialerListener.hungUp();
		}

		private void fireIncomingCall(String number) {
			if (dialerListener != null)
				dialerListener.incomingCall(number);
		}
		
		private void fireOutgoingCall(String number) {
			if (dialerListener != null)
				dialerListener.outgoingCall(number);
		}
		
		private void fireRejected() {
			if (dialerListener != null)
				dialerListener.rejected();
		}
		
		private void fireStatusChanged(int status) {
			if (dialerListener != null)
				dialerListener.statusChanged(status);
		}
		
		private void fireTalking() {
			if (dialerListener != null)
				dialerListener.talking();
		}

	}

}
