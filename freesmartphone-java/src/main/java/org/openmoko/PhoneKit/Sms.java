/*
 * freesmartphone-java - Copyright (C) 2008 Guillaume Legris
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License version
 * 2 only, as published by the Free Software Foundation. 
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License version 2 for more details (a copy is
 * included at /legal/license.txt). 
 * 
 * You should have received a copy of the GNU General Public License
 * version 2 along with this work; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA 
 * 
 * Linking this library statically or dynamically with other modules is
 * making a combined work based on this library.  Thus, the terms and
 * conditions of the GNU General Public License cover the whole
 * combination.
 *
 * As a special exception, the copyright holders of this library give you
 * permission to link this library with independent modules to produce an
 * executable, regardless of the license terms of these independent
 * modules, and to copy and distribute the resulting executable under
 * terms of your choice, provided that you also meet, for each linked
 * independent module, the terms and conditions of the license of that
 * module.  An independent module is a module which is not derived from
 * or based on this library.  If you modify this library, you may extend
 * this exception to your version of the library, but you are not
 * obligated to do so.  If you do not wish to do so, delete this
 * exception statement from your version.
 */
package org.openmoko.PhoneKit;

import org.freedesktop.dbus.DBusInterface;
import org.freedesktop.dbus.DBusSignal;
import org.freedesktop.dbus.exceptions.DBusException;

// @CSymbol("moko_sms")
public interface Sms extends DBusInterface {
	public static class StatusChanged extends DBusSignal {
		public final int status;

		public StatusChanged(String path, int status) throws DBusException {
			super(path, status);
			this.status = status;
		}
	}

	public static class SimMemoryState extends DBusSignal {
		public final boolean full;

		public SimMemoryState(String path, boolean full) throws DBusException {
			super(path, full);
			this.full = full;
		}
	}

	public static class PhoneMemoryState extends DBusSignal {
		public final boolean full;

		public PhoneMemoryState(String path, boolean full) throws DBusException {
			super(path, full);
			this.full = full;
		}
	}

	public String Send(String number, String message, boolean report);

	public Pair<Boolean, Boolean> GetMemoryStatus();

}
