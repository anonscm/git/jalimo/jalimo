/*
 * freesmartphone-java - Copyright (C) 2008 Guillaume Legris
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License version
 * 2 only, as published by the Free Software Foundation. 
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License version 2 for more details (a copy is
 * included at /legal/license.txt). 
 * 
 * You should have received a copy of the GNU General Public License
 * version 2 along with this work; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA 
 * 
 * Linking this library statically or dynamically with other modules is
 * making a combined work based on this library.  Thus, the terms and
 * conditions of the GNU General Public License cover the whole
 * combination.
 *
 * As a special exception, the copyright holders of this library give you
 * permission to link this library with independent modules to produce an
 * executable, regardless of the license terms of these independent
 * modules, and to copy and distribute the resulting executable under
 * terms of your choice, provided that you also meet, for each linked
 * independent module, the terms and conditions of the license of that
 * module.  An independent module is a module which is not derived from
 * or based on this library.  If you modify this library, you may extend
 * this exception to your version of the library, but you are not
 * obligated to do so.  If you do not wish to do so, delete this
 * exception statement from your version.
 */
package org.openmoko.PhoneKit;

import org.freedesktop.dbus.DBusInterface;
import org.freedesktop.dbus.DBusSignal;
import org.freedesktop.dbus.exceptions.DBusException;

//@CSymbol("moko_dialer")
public interface Dialer extends DBusInterface {
	public static class IncomingCall extends DBusSignal {
		public final String number;

		public IncomingCall(String path, String number) throws DBusException {
			super(path, number);
			this.number = number;
		}
	}

	public static class OutgoingCall extends DBusSignal {
		public final String number;

		public OutgoingCall(String path, String number) throws DBusException {
			super(path, number);
			this.number = number;
		}
	}

	public static class Talking extends DBusSignal {
		public Talking(String path) throws DBusException {
			super(path);
		}
	}

	public static class HungUp extends DBusSignal {
		public HungUp(String path) throws DBusException {
			super(path);
		}
	}

	public static class Rejected extends DBusSignal {
		public Rejected(String path) throws DBusException {
			super(path);
		}
	}

	public static class StatusChanged extends DBusSignal {
		public final int status;

		public StatusChanged(String path, int status) throws DBusException {
			super(path, status);
			this.status = status;
		}
	}

	public void Dial(String number);

	public void HangUp(String message);

}
