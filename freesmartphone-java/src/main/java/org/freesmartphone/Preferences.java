package org.freesmartphone;
import java.util.List;
import org.freedesktop.dbus.DBusInterface;
public interface Preferences extends DBusInterface
{

  public List<String> GetServices();
  public DBusInterface GetService(String name);
  public String GetProfile();
  public void SetProfile(String profile);
  public List<String> GetProfiles();

}
