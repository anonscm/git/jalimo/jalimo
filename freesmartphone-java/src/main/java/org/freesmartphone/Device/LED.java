package org.freesmartphone.Device;

import org.freedesktop.dbus.DBusInterface;

public interface LED extends DBusInterface {

    // @Async("fso_device_led")
    public void SetBrightness(int brightness);

    // @Async("fso_device_led")
    public void SetBlinking(int on_duration, int off_duration);

}
