package org.freesmartphone.Device;

import org.freedesktop.dbus.DBusInterface;
import org.freedesktop.dbus.DBusSignal;
import org.freedesktop.dbus.exceptions.DBusException;

public interface IdleNotifier extends DBusInterface {
    public static class State extends DBusSignal {
        public final String status;

        public State(String path, String status) throws DBusException {
            super(path, status);
            this.status = status;
        }
    }

    // @Async("fso_device_idlenotifier")
    public void GetState(String status);

    // @Async("fso_device_idlenotifier")
    public String SetState();

}
