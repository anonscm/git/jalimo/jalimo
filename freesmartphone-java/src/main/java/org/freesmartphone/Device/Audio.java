package org.freesmartphone.Device;

import java.util.List;
import java.util.Map;

import org.freedesktop.dbus.DBusInterface;
import org.freedesktop.dbus.DBusSignal;
import org.freedesktop.dbus.Variant;
import org.freedesktop.dbus.exceptions.DBusException;

public interface Audio extends DBusInterface {
    public static class SoundStatus extends DBusSignal {
        public final String id;
        public final String status;
        public final Map<String, Variant> properties;

        public SoundStatus(String path, String id, String status, Map<String, Variant> properties) throws DBusException {
            super(path, id, status, properties);
            this.id = id;
            this.status = status;
            this.properties = properties;
        }
    }

    public Map<String, Variant> GetInfo();

    public List<String> GetSupportedFormats();

    public List<String> GetAvailableScenarios();

    public void PlaySound(String id);

    public void StopSound(String id);

    public void StopAllSounds();

}
