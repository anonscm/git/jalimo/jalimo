package org.freesmartphone.Device;

import java.util.Map;

import org.freedesktop.dbus.DBusInterface;
import org.freedesktop.dbus.DBusSignal;
import org.freedesktop.dbus.Variant;
import org.freedesktop.dbus.exceptions.DBusException;

public interface PowerSupply extends DBusInterface {
    public static class PowerStatus extends DBusSignal {
        public final String status;

        public PowerStatus(String path, String status) throws DBusException {
            super(path, status);
            this.status = status;
        }
    }

    public Map<String, Variant> GetInfo();

    public int GetEnergyPercentage();

    public String GetPowerStatus();

}
