package org.freesmartphone.Device;

import org.freedesktop.dbus.DBusInterface;

public interface RealtimeClock extends DBusInterface {

    // @Async("fso_device_realtimeclock")
    public int GetCurrentTime();

    // @Async("fso_device_realtimeclock")
    public void SetCurrentTime(int time);

    // @Async("fso_device_realtimeclock")
    public int GetWakeupTime();

    // @Async("fso_device_realtimeclock")
    public void SetWakeupTime(boolean time);

}
