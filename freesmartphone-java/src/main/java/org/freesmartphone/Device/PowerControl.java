package org.freesmartphone.Device;

import org.freedesktop.dbus.DBusInterface;
import org.freedesktop.dbus.DBusSignal;
import org.freedesktop.dbus.exceptions.DBusException;

public interface PowerControl extends DBusInterface {
    public static class Power extends DBusSignal {
        public final boolean on;

        public Power(String path, boolean on) throws DBusException {
            super(path, on);
            this.on = on;
        }
    }

    // @Async("fso_device_powercontrol")
    public boolean GetPower();

    // @Async("fso_device_powercontrol")
    public void SetPower(boolean on);

}
