package org.freesmartphone.Device;

import java.util.Map;

import org.freedesktop.dbus.DBusInterface;
import org.freedesktop.dbus.Variant;

public interface Display extends DBusInterface {

    // @Async("fso_device_display")
    public Map<String, Variant> GetInfo();

    // @Async("fso_device_display")
    public int GetBrightness();

    // @Async("fso_device_display")
    public void SetBrightness(int brightness);

    // @Async("fso_device_display")
    public boolean GetBacklightPower();

    // @Async("fso_device_display")
    public void SetBacklightPower(boolean power);

}
