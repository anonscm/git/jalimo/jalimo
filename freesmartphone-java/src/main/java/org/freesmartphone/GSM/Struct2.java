package org.freesmartphone.GSM;

import org.freedesktop.dbus.Position;
import org.freedesktop.dbus.Struct;

public final class Struct2 extends Struct {
    @Position(0)
    public final int a;
    @Position(1)
    public final String b;
    @Position(2)
    public final String c;

    public Struct2(int a, String b, String c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }
}
