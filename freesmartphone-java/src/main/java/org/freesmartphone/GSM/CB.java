package org.freesmartphone.GSM;

import org.freedesktop.dbus.DBusInterface;
import org.freedesktop.dbus.DBusSignal;
import org.freedesktop.dbus.exceptions.DBusException;

public interface CB extends DBusInterface {
    public static class IncomingCellBroadcast extends DBusSignal {
        public final int serial;
        public final int channel;
        public final int encoding;
        public final int page;
        public final String data;

        public IncomingCellBroadcast(String path, int serial, int channel, int encoding, int page, String data) throws DBusException {
            super(path, serial, channel, encoding, page, data);
            this.serial = serial;
            this.channel = channel;
            this.encoding = encoding;
            this.page = page;
            this.data = data;
        }
    }

    // @Async("fso_gsm_cb")
    public String GetCellBroadcastSubscriptions();

    // @Async("fso_gsm_cb")
    public void SetCellBroadcastSubscriptions(String channels);

}
