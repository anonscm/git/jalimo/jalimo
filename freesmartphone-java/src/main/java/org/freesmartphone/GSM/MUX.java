package org.freesmartphone.GSM;

import org.freedesktop.DBus.GLib.CSymbol;
import org.freedesktop.dbus.DBusInterface;

public interface MUX extends DBusInterface {

    @CSymbol("muxer_control_power")
    public void SetPower(String origin, boolean on);

    @CSymbol("muxer_control_power")
    public boolean GetPower(String origin);

    @CSymbol("muxer_control_reset_modem")
    public void Reset(String origin);

    @CSymbol("muxer_control_alloc_channel")
    public String AllocChannel(String origin);

}
