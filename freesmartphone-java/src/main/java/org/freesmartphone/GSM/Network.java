package org.freesmartphone.GSM;

import java.util.List;
import java.util.Map;

import org.freedesktop.dbus.DBusInterface;
import org.freedesktop.dbus.DBusSignal;
import org.freedesktop.dbus.Variant;
import org.freedesktop.dbus.exceptions.DBusException;

public interface Network extends DBusInterface {
    public static class Status extends DBusSignal {
        public final Map<String, Variant> status;

        public Status(String path, Map<String, Variant> status) throws DBusException {
            super(path, status);
            this.status = status;
        }
    }

    public static class SignalStrength extends DBusSignal {
        public final int signal_strength;

        public SignalStrength(String path, int signal_strength) throws DBusException {
            super(path, signal_strength);
            this.signal_strength = signal_strength;
        }
    }

    public static class IncomingUssd extends DBusSignal {
        public final String mode;
        public final String message;

        public IncomingUssd(String path, String mode, String message) throws DBusException {
            super(path, mode, message);
            this.mode = mode;
            this.message = message;
        }
    }

    // @Async("fso_gsm_network")
    public void Register();

    // @Async("fso_gsm_network")
    public void Unregister();

    // @Async("fso_gsm_network")
    public Map<String, Variant> GetStatus();

    // @Async("fso_gsm_network")
    public int GetSignalStrength();

    // @Async("fso_gsm_network")
    public List<Struct1> ListProviders();

    // @Async("fso_gsm_network")
    public void RegisterWithProvider(int operator_code);

    // @Async("fso_gsm_network")
    public String GetCountryCode();

    // @Async("fso_gsm_network")
    public Map<String, Variant> GetCallForwarding();

    // @Async("fso_gsm_network")
    public void EnableCallForwarding(String reason, String _class, String number, int timeout);

    // @Async("fso_gsm_network")
    public void DisableCallForwarding(String reason, String _class);

    // @Async("fso_gsm_network")
    public void SetCallingIdentification(String visible);

    // @Async("fso_gsm_network")
    public String GetCallingIdentification();

    // @Async("fso_gsm_network")
    public void SendUssdRequest(String request);

}
