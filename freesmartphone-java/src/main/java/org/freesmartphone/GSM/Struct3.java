package org.freesmartphone.GSM;

import org.freedesktop.dbus.Position;
import org.freedesktop.dbus.Struct;

public final class Struct3 extends Struct {
    @Position(0)
    public final int a;
    @Position(1)
    public final String b;
    @Position(2)
    public final String c;
    @Position(3)
    public final String d;

    public Struct3(int a, String b, String c, String d) {
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
    }
}
