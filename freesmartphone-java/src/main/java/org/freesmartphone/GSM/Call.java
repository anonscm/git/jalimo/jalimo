package org.freesmartphone.GSM;

import java.util.List;
import java.util.Map;

import org.freedesktop.dbus.DBusInterface;
import org.freedesktop.dbus.DBusSignal;
import org.freedesktop.dbus.Variant;
import org.freedesktop.dbus.exceptions.DBusException;

public interface Call extends DBusInterface {
    public static class CallStatus extends DBusSignal {
        public final int id;
        public final String status;
        public final Map<String, Variant> properties;

        public CallStatus(String path, int id, String status, Map<String, Variant> properties) throws DBusException {
            super(path, id, status, properties);
            this.id = id;
            this.status = status;
            this.properties = properties;
        }
    }

    // @Async("fso_gsm_call")
    public String Emergency();

    // @Async("fso_gsm_call")
    public void Activate(int id);

    // @Async("fso_gsm_call")
    public void ActivateConference(int id);

    // @Async("fso_gsm_call")
    public void Release(String message, int id);

    // @Async("fso_gsm_call")
    public void HoldActive();

    // @Async("fso_gsm_call")
    public void Join();

    // @Async("fso_gsm_call")
    public void Transfer(String number);

    // @Async("fso_gsm_call")
    public void ReleaseHeld(String message);

    // @Async("fso_gsm_call")
    public void ReleaseAll(String message);

    // @Async("fso_gsm_call")
    public int Initiate(String number, String type);

    // @Async("fso_gsm_call")
    public List<Struct1> ListCalls();

    // @Async("fso_gsm_call")
    public void SendDtmf(String tones);

}
