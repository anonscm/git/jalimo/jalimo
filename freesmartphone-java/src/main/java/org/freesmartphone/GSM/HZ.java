package org.freesmartphone.GSM;

import java.util.List;

import org.freedesktop.dbus.DBusInterface;
import org.freedesktop.dbus.DBusSignal;
import org.freedesktop.dbus.exceptions.DBusException;

public interface HZ extends DBusInterface {
    public static class HomeZoneStatus extends DBusSignal {
        public final String name;

        public HomeZoneStatus(String path, String name) throws DBusException {
            super(path, name);
            this.name = name;
        }
    }

    // @Async("fso_gsm_hz")
    public List<String> GetKnownHomeZones();

    // @Async("fso_gsm_hz")
    public String GetHomeZoneStatus();

}
