package org.freesmartphone.GSM;

import java.util.List;
import java.util.Map;

import org.freedesktop.dbus.DBusInterface;
import org.freedesktop.dbus.DBusSignal;
import org.freedesktop.dbus.Variant;
import org.freedesktop.dbus.exceptions.DBusException;

public interface PDP extends DBusInterface {
    public static class ContextStatus extends DBusSignal {
        public final int id;
        public final String status;
        public final Map<String, Variant> properties;

        public ContextStatus(String path, int id, String status, Map<String, Variant> properties) throws DBusException {
            super(path, id, status, properties);
            this.id = id;
            this.status = status;
            this.properties = properties;
        }
    }

    // @Async("fso_gsm_pdp")
    public List<String> ListGprsClasses();

    // @Async("fso_gsm_pdp")
    public String GetCurrentGprsClass();

    // @Async("fso_gsm_pdp")
    public void SetCurrentGprsClass(String device_class);

    // @Async("fso_gsm_pdp")
    public int ActivateContext(String apn, String username, String password);

    // @Async("fso_gsm_pdp")
    public void DeactivateContext(int index);

    // @Async("fso_gsm_pdp")
    public String GetContextStatus(int index);

}
