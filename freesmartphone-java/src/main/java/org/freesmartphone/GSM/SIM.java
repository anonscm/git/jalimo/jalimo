package org.freesmartphone.GSM;

import java.util.List;
import java.util.Map;

import org.freedesktop.dbus.DBusInterface;
import org.freedesktop.dbus.DBusSignal;
import org.freedesktop.dbus.Variant;
import org.freedesktop.dbus.exceptions.DBusException;

public interface SIM extends DBusInterface {
    public static class AuthStatus extends DBusSignal {
        public final String status;

        public AuthStatus(String path, String status) throws DBusException {
            super(path, status);
            this.status = status;
        }
    }

    public static class IncomingMessage extends DBusSignal {
        public final int index;

        public IncomingMessage(String path, int index) throws DBusException {
            super(path, index);
            this.index = index;
        }
    }

    // @Async("fso_gsm_sim")
    public String GetAuthStatus();

    // @Async("fso_gsm_sim")
    public void SendAuthCode(String pin);

    // @Async("fso_gsm_sim")
    public void Unlock(String puk, String new_pin);

    // @Async("fso_gsm_sim")
    public void ChangeAuthCode(String old_pin, String new_pin);

    // @Async("fso_gsm_sim")
    public void SetAuthCodeRequired(boolean check, String pin);

    // @Async("fso_gsm_sim")
    public boolean GetAuthCodeRequired();

    // @Async("fso_gsm_sim")
    public Map<String, Variant> GetSimInfo();

    // @Async("fso_gsm_sim")
    public String SendGenericSimCommand(String command);

    // @Async("fso_gsm_sim")
    public String SendRestrictedSimCommand(int command, int fileid, int p1, int p2, int p3, String data);

    // @Async("fso_gsm_sim")
    public List<Struct1> GetHomeZones();

    // @Async("fso_gsm_sim")
    public Map<String, Variant> GetPhonebookInfo();

    // @Async("fso_gsm_sim")
    public List<Struct2> RetrievePhonebook();

    // @Async("fso_gsm_sim")
    public void DeleteEntry(int index);

    // @Async("fso_gsm_sim")
    public void StoreEntry(int index, String name, String number);

    // @Async("fso_gsm_sim")
    public Pair<String, String> RetrieveEntry(int index);

    // @Async("fso_gsm_sim")
    public Map<String, Variant> GetMessagebookInfo();

    // @Async("fso_gsm_sim")
    public List<Struct3> RetrieveMessagebook(String category);

    // @Async("fso_gsm_sim")
    public String GetServiceCenterNumber();

    // @Async("fso_gsm_sim")
    public void SetServiceCenterNumber(String number);

    // @Async("fso_gsm_sim")
    public void DeleteMessage(int index);

    // @Async("fso_gsm_sim")
    public int StoreMessage(String recipient_number, String contents);

    // @Async("fso_gsm_sim")
    public int SendStoredMessage(int index);

    // @Async("fso_gsm_sim")
    public Pair<String, String> RetrieveMessage(int index);

}
