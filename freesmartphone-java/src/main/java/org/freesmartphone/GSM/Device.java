package org.freesmartphone.GSM;

import java.util.Map;

import org.freedesktop.dbus.DBusInterface;
import org.freedesktop.dbus.Variant;

public interface Device extends DBusInterface {

    // @Async("fso_gsm_device")
    public Map<String, Variant> GetInfo();

    // @Async("fso_gsm_device")
    public boolean GetAntennaPower();

    // @Async("fso_gsm_device")
    public void SetAntennaPower(boolean antenna_power);

    // @Async("fso_gsm_device")
    public Map<String, Variant> GetFeatures();

    // @Async("fso_gsm_device")
    public void PrepareToSuspend();

    // @Async("fso_gsm_device")
    public void RecoverFromSuspend();

}
