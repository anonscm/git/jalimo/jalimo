package org.freesmartphone.device;


import org.freesmartphone.device.Input;
public interface Input {

    public void addInputListener(org.freesmartphone.device.InputListener newListener);
    public void removeInputListener(org.freesmartphone.device.InputListener removeListener);
}
