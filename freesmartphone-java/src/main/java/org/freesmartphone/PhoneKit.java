package org.freesmartphone;


import org.freedesktop.dbus.DBusConnection;
import org.freedesktop.dbus.DBusSignal;
import org.freedesktop.dbus.DBusInterface;
import org.freedesktop.dbus.exceptions.DBusException;


public class PhoneKit {

    private String serviceToConnect = "org.freedesktop.Gypsy";

    private DBusConnection connection;

    private org.freesmartphone.Phone phoneInstance;
    private org.freesmartphone.phone.Call callInstance;
    private org.freesmartphone.device.Input inputInstance;

    public PhoneKit() throws DBusException {
        connection = DBusConnection.getConnection(DBusConnection.SYSTEM);
    }

    public PhoneKit(String serviceToConnect) throws DBusException {
        connection = DBusConnection.getConnection(DBusConnection.SYSTEM);
        this.serviceToConnect = serviceToConnect;
    }

    public void close() {
        connection.disconnect();
    }

    public org.freesmartphone.Phone getPhoneInstance() throws Exception {
        if (phoneInstance == null)
            phoneInstance = new org.freesmartphone.impl.PhoneImpl(connection, serviceToConnect, "/org/freesmartphone/Phone");
        return phoneInstance;
    }

    public org.freesmartphone.phone.Call getCallInstance() throws Exception {
        if (callInstance == null)
            callInstance = new org.freesmartphone.impl.phone.CallImpl(connection, serviceToConnect, "/org/freesmartphone/Phone/Call");
        return callInstance;
    }

    public org.freesmartphone.device.Input getInputInstance() throws Exception {
        if (inputInstance == null)
            inputInstance = new org.freesmartphone.impl.device.InputImpl(connection, serviceToConnect, "/org/freesmartphone/Device/Input");
        return inputInstance;
    }

}
