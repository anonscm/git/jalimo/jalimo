package org.freesmartphone.phone;


import org.freesmartphone.phone.Call;
public interface Call {

    public String GetPeer() throws Exception;

    public String Initiate() throws Exception;

    public String Activate() throws Exception;

    public String Release() throws Exception;

    public String GetStatus() throws Exception;

    public void Remove() throws Exception;

    public void addCallListener(org.freesmartphone.phone.CallListener newListener);
    public void removeCallListener(org.freesmartphone.phone.CallListener removeListener);
}
