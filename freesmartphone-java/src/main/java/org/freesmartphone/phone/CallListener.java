package org.freesmartphone.phone;


public interface CallListener {

    public void CallOutgoing();

    public void CallReleased();

    public void CallActivated();

}
