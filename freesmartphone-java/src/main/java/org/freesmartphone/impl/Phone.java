package org.freesmartphone.impl;


import org.freedesktop.dbus.DBusInterface;
import org.freedesktop.dbus.DBusInterfaceName;
import org.freedesktop.dbus.DBusSignal;
import org.freedesktop.dbus.exceptions.DBusException;


@DBusInterfaceName("org.freesmartphone.Phone")
public interface Phone extends DBusInterface {

    public String[] InitProtocols();

    public org.freedesktop.dbus.Path CreateCall(String number, String protocol, boolean force);

    @DBusInterfaceName("org.freesmartphone.Phone.Incoming")
    public class Incoming extends DBusSignal {

        public final org.freesmartphone.phone.Call call;

        public Incoming(String path, org.freesmartphone.phone.Call call) throws DBusException {
            super(path, call);
            this.call = call;
        }
    }

}
