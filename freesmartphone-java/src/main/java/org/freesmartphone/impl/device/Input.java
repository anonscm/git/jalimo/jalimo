package org.freesmartphone.impl.device;


import org.freedesktop.dbus.DBusInterface;
import org.freedesktop.dbus.DBusInterfaceName;
import org.freedesktop.dbus.DBusSignal;
import org.freedesktop.dbus.exceptions.DBusException;


@DBusInterfaceName("org.freesmartphone.Device.Input")
public interface Input extends DBusInterface {

    @DBusInterfaceName("org.freesmartphone.Device.Input.Event")
    public class Event extends DBusSignal {

        public final String name;
        public final String action;
        public final int seconds;

        public Event(String path, String name, String action, int seconds) throws DBusException {
            super(path, name, action, seconds);
            this.name = name;
            this.action = action;
            this.seconds = seconds;
        }
    }

}
