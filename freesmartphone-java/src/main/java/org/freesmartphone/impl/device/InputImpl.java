package org.freesmartphone.impl.device;


import java.util.LinkedList;
import org.freedesktop.dbus.DBusInterface;
import org.freedesktop.dbus.DBusSignal;
import org.freedesktop.dbus.DBusSignalHelper;
import org.freedesktop.dbus.DBusConnection;
import org.freedesktop.dbus.exceptions.DBusException;
import org.freedesktop.dbus.DBusSigHandler;


public class InputImpl implements org.freesmartphone.device.Input, DBusSigHandler {

    private Input input;
    private DBusConnection conn;
    private String serviceToConnect;
    private LinkedList<org.freesmartphone.device.InputListener> listener = new LinkedList<org.freesmartphone.device.InputListener>();

    public InputImpl(DBusConnection conn, String serviceToConnect, String objectPath) throws DBusException {
        this.conn = conn;
        this.serviceToConnect = serviceToConnect;
        input = conn.getRemoteObject(serviceToConnect, objectPath, Input.class);

        DBusSignalHelper.addInterfaceMap("org.freesmartphone.impl.device.Input", "org.freesmartphone.Device.Input");
        conn.addSigHandler(org.freesmartphone.impl.device.Input.Event.class, this);
    }

    public void addInputListener(org.freesmartphone.device.InputListener newListener) {
        listener.add(newListener);
    }

    public void removeInputListener(org.freesmartphone.device.InputListener removeListener) {
        listener.remove(removeListener);
    }

    public void handle(DBusSignal s) {
        if (s instanceof Input.Event) {
            fireInputEvent((Input.Event) s);
        }
    }

    private void fireInputEvent(Input.Event state) {
        for (org.freesmartphone.device.InputListener l : listener)
            l.InputEvent(state.name, state.action, state.seconds);

    }

    public boolean isRemote() {
        return false;    }}
