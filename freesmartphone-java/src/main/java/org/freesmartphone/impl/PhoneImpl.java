package org.freesmartphone.impl;


import java.util.LinkedList;
import org.freedesktop.dbus.DBusInterface;
import org.freedesktop.dbus.DBusSignal;
import org.freedesktop.dbus.DBusSignalHelper;
import org.freedesktop.dbus.DBusConnection;
import org.freedesktop.dbus.exceptions.DBusException;
import org.freedesktop.dbus.DBusSigHandler;


public class PhoneImpl implements org.freesmartphone.Phone, DBusSigHandler {

    private Phone phone;
    private DBusConnection conn;
    private String serviceToConnect;
    private LinkedList<org.freesmartphone.PhoneListener> listener = new LinkedList<org.freesmartphone.PhoneListener>();

    public PhoneImpl(DBusConnection conn, String serviceToConnect, String objectPath) throws DBusException {
        this.conn = conn;
        this.serviceToConnect = serviceToConnect;
        phone = conn.getRemoteObject(serviceToConnect, objectPath, Phone.class);

        DBusSignalHelper.addInterfaceMap("org.freesmartphone.impl.Phone", "org.freesmartphone.Phone");
        conn.addSigHandler(org.freesmartphone.impl.Phone.Incoming.class, this);
    }

    public String[] InitProtocols() throws Exception {
        return phone.InitProtocols();
    }

    public org.freesmartphone.phone.Call CreateCall(String number, String protocol, boolean force) throws Exception {
        org.freedesktop.dbus.Path path = phone.CreateCall(number, protocol, force);
        return new org.freesmartphone.impl.phone.CallImpl(conn, serviceToConnect, path.getPath());
    }

    public void addPhoneListener(org.freesmartphone.PhoneListener newListener) {
        listener.add(newListener);
    }

    public void removePhoneListener(org.freesmartphone.PhoneListener removeListener) {
        listener.remove(removeListener);
    }

    public void handle(DBusSignal s) {
        if (s instanceof Phone.Incoming) {
            firePhoneIncoming((Phone.Incoming) s);
        }
    }

    private void firePhoneIncoming(Phone.Incoming state) {
        for (org.freesmartphone.PhoneListener l : listener)
            l.PhoneIncoming(state.call);

    }

    public boolean isRemote() {
        return false;    }}
