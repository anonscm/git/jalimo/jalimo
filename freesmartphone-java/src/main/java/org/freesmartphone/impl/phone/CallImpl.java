package org.freesmartphone.impl.phone;


import java.util.LinkedList;
import org.freedesktop.dbus.DBusInterface;
import org.freedesktop.dbus.DBusSignal;
import org.freedesktop.dbus.DBusSignalHelper;
import org.freedesktop.dbus.DBusConnection;
import org.freedesktop.dbus.exceptions.DBusException;
import org.freedesktop.dbus.DBusSigHandler;


public class CallImpl implements org.freesmartphone.phone.Call, DBusSigHandler {

    private Call call;
    private DBusConnection conn;
    private String serviceToConnect;
    private LinkedList<org.freesmartphone.phone.CallListener> listener = new LinkedList<org.freesmartphone.phone.CallListener>();

    public CallImpl(DBusConnection conn, String serviceToConnect, String objectPath) throws DBusException {
        this.conn = conn;
        this.serviceToConnect = serviceToConnect;
        call = conn.getRemoteObject(serviceToConnect, objectPath, Call.class);

        DBusSignalHelper.addInterfaceMap("org.freesmartphone.impl.phone.Call", "org.freesmartphone.Phone.Call");
        conn.addSigHandler(org.freesmartphone.impl.phone.Call.Outgoing.class, this);
        DBusSignalHelper.addInterfaceMap("org.freesmartphone.impl.phone.Call", "org.freesmartphone.Phone.Call");
        conn.addSigHandler(org.freesmartphone.impl.phone.Call.Released.class, this);
        DBusSignalHelper.addInterfaceMap("org.freesmartphone.impl.phone.Call", "org.freesmartphone.Phone.Call");
        conn.addSigHandler(org.freesmartphone.impl.phone.Call.Activated.class, this);
    }

    public String GetPeer() throws Exception {
        return call.GetPeer();
    }

    public String Initiate() throws Exception {
        return call.Initiate();
    }

    public String Activate() throws Exception {
        return call.Activate();
    }

    public String Release() throws Exception {
        return call.Release();
    }

    public String GetStatus() throws Exception {
        return call.GetStatus();
    }

    public void Remove() throws Exception {
        call.Remove();
    }

    public void addCallListener(org.freesmartphone.phone.CallListener newListener) {
        listener.add(newListener);
    }

    public void removeCallListener(org.freesmartphone.phone.CallListener removeListener) {
        listener.remove(removeListener);
    }

    public void handle(DBusSignal s) {
        if (s instanceof Call.Outgoing) {
            fireCallOutgoing((Call.Outgoing) s);
        }
        if (s instanceof Call.Released) {
            fireCallReleased((Call.Released) s);
        }
        if (s instanceof Call.Activated) {
            fireCallActivated((Call.Activated) s);
        }
    }

    private void fireCallOutgoing(Call.Outgoing state) {
        for (org.freesmartphone.phone.CallListener l : listener)
            l.CallOutgoing();

    }

    private void fireCallReleased(Call.Released state) {
        for (org.freesmartphone.phone.CallListener l : listener)
            l.CallReleased();

    }

    private void fireCallActivated(Call.Activated state) {
        for (org.freesmartphone.phone.CallListener l : listener)
            l.CallActivated();

    }

    public boolean isRemote() {
        return false;    }}
