package org.freesmartphone.impl.phone;


import org.freedesktop.dbus.DBusInterface;
import org.freedesktop.dbus.DBusInterfaceName;
import org.freedesktop.dbus.DBusSignal;
import org.freedesktop.dbus.exceptions.DBusException;


@DBusInterfaceName("org.freesmartphone.Phone.Call")
public interface Call extends DBusInterface {

    public String GetPeer();

    public String Initiate();

    public String Activate();

    public String Release();

    public String GetStatus();

    public void Remove();

    @DBusInterfaceName("org.freesmartphone.Phone.Call.Outgoing")
    public class Outgoing extends DBusSignal {


        public Outgoing(String path) throws DBusException {
            super(path);
        }
    }

    @DBusInterfaceName("org.freesmartphone.Phone.Call.Released")
    public class Released extends DBusSignal {


        public Released(String path) throws DBusException {
            super(path);
        }
    }

    @DBusInterfaceName("org.freesmartphone.Phone.Call.Activated")
    public class Activated extends DBusSignal {


        public Activated(String path) throws DBusException {
            super(path);
        }
    }

}
