package org.freesmartphone;

import java.util.List;

import org.freedesktop.dbus.DBusInterface;
import org.freedesktop.dbus.DBusSignal;
import org.freedesktop.dbus.exceptions.DBusException;

public interface Usage extends DBusInterface {
    public static class ResourceChanged extends DBusSignal {
        public final String name;

        public ResourceChanged(String path, String name) throws DBusException {
            super(path, name);
            this.name = name;
        }
    }

    // @Async("fso_usage")
    public List<String> ListResources();

    // @Async("fso_usage")
    public String GetResourcePolicy(String name);

    // @Async("fso_usage")
    public void SetResourcePolicy(String name, String policy);

    // @Async("fso_usage")
    public boolean GetResourceState(String name);

    // @Async("fso_usage")
    public List<String> GetResourceUsers(String name);

    // @Async("fso_usage")
    public boolean RequestResource(String name);

    // @Async("fso_usage")
    public void ReleaseResource(String name);

}
