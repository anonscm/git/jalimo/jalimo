package org.freesmartphone;


import org.freesmartphone.Phone;
public interface Phone {

    public String[] InitProtocols() throws Exception;

    public org.freesmartphone.phone.Call CreateCall(String number, String protocol, boolean force) throws Exception;

    public void addPhoneListener(org.freesmartphone.PhoneListener newListener);
    public void removePhoneListener(org.freesmartphone.PhoneListener removeListener);
}
