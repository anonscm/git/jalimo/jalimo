import org.freesmartphone.PhoneKit;
import org.freesmartphone.device.Input;
import org.freesmartphone.device.InputListener;



public class InputTest implements InputListener {

	/**
	 * @param args
	 * @throws Exception 
	 */
	public static void main(String[] args) throws Exception {	
		new InputTest();
		while (true) {
			Thread.sleep(50);
		}
	}
	
	public InputTest() throws Exception {
		PhoneKit kit = new PhoneKit();
		Input input = kit.getInputInstance();
		input.addInputListener(this);
		System.out.println("listening");
	}

	public void InputEvent(String name, String action, int seconds) {
		System.out.println("input event: "+name+", "+action+", "+seconds);
		
	}

}