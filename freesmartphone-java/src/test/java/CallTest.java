import org.freesmartphone.Phone;
import org.freesmartphone.PhoneKit;
import org.freesmartphone.phone.Call;


public class CallTest {

	/**
	 * @param args
	 * @throws Exception 
	 */
	public static void main(String[] args) throws Exception {
		PhoneKit kit = new PhoneKit();
		Phone phone = kit.getPhoneInstance();
		System.out.println("protocols: "+ phone.InitProtocols());
		Call call = phone.CreateCall("0171XXXX", "GSM", false);
		System.out.println("call state: "+call.GetStatus());
		System.out.println("call state: "+call.Initiate());
	}

}
