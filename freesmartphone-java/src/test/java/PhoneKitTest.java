/*
 * freesmartphone-java - Copyright (C) 2008 Guillaume Legris
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License version
 * 2 only, as published by the Free Software Foundation. 
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License version 2 for more details (a copy is
 * included at /legal/license.txt). 
 * 
 * You should have received a copy of the GNU General Public License
 * version 2 along with this work; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA 
 * 
 * Linking this library statically or dynamically with other modules is
 * making a combined work based on this library.  Thus, the terms and
 * conditions of the GNU General Public License cover the whole
 * combination.
 *
 * As a special exception, the copyright holders of this library give you
 * permission to link this library with independent modules to produce an
 * executable, regardless of the license terms of these independent
 * modules, and to copy and distribute the resulting executable under
 * terms of your choice, provided that you also meet, for each linked
 * independent module, the terms and conditions of the license of that
 * module.  An independent module is a module which is not derived from
 * or based on this library.  If you modify this library, you may extend
 * this exception to your version of the library, but you are not
 * obligated to do so.  If you do not wish to do so, delete this
 * exception statement from your version.
 */
import org.openmoko.phone.Dialer;
import org.openmoko.phone.DialerEventListener;
import org.openmoko.phone.MemoryStatus;
import org.openmoko.phone.Network;
import org.openmoko.phone.NetworkEventListener;
import org.openmoko.phone.PhoneKit;
import org.openmoko.phone.SMS;
import org.openmoko.phone.SMSEventListener;

public class PhoneKitTest {

	public static void main(String[] args) {

		String phoneNumber = args[0];

		try {

			/* Initialize the PhoneKit */
			PhoneKit phonekit = PhoneKit.createPhoneKit();

			/*
			 * Test SMS API
			 */

			SMS sms = phonekit.getSMSInstance();
			sms.addSMSListener(new SMSEventListener() {
				public void phoneMemoryStateChanged(boolean full) {
					System.out.println("[SMS] Event phoneMemoryStateChanged: " + full);
				}
				public void simMemoryStateChanged(boolean full) {
					System.out.println("[SMS] Event simMemoryStateChanged: " + full);
				}
				public void statusChanged(int status) {
					System.out.println("[SMS] Event statusChanged: " + status);
				}
			});

			MemoryStatus status = sms.getMemoryStatus();
			System.out.println("[SMS] MemoryStatus.isSimFull(): " + status.isSimFull());
			System.out.println("[SMS] MemoryStatus.isPhoneFull(): " + status.isPhoneFull());

			if (phoneNumber != null) {
				System.out.println("[SMS] Sending Hello World SMS... ");
				sms.send(phoneNumber, "Hello World", true);
			}

			/*
			 * Test Network API
			 */

			Network network = phonekit.getNetworkInstance();
			network.addNetworkListener(new NetworkEventListener() {
				public void providerChanged(String name) {
					System.out.println("[Network] Event providerChanged: " + name);
				}

				public void statusChanged(int status) {
					System.out.println("[Network] Event statusChanged: " + status);
				}

				public void subscriberNumberChanged(String number) {
					System.out.println("[Network] Event subscriberNumberChanged: " + number);
				}
			});

			System.out.println("[Network] getImsi(): " + network.getImsi());
			System.out.println("[Network] getProviderName(): " + network.getProviderName());

			// Doesn't work ...
			// System.out.println("[Network] getCountryCode(): " +
			// network.getCountryCode());
			// System.out.println("[Network] getHomeCountryCode(): " +
			// network.getHomeCountryCode());
			// System.out.println("[Network] getSubscriberNumber(): " +
			// network.getSubscriberNumber());

			/*
			 * Test Dialer API
			 */

			final Dialer dialer = phonekit.getDialerInstance();
			dialer.addDialerEventListener(new DialerEventListener() {
				public void hungUp() {
					System.out.println("[Dialer] Event hungUp ");
				}
				public void incomingCall(String number) {
					System.out.println("[Dialer] Event incomingCall: " + number);
				}
				public void outgoingCall(String number) {
					System.out.println("[Dialer] Event outgoingCall: " + number);
				}
				public void rejected() {
					System.out.println("[Dialer] Event rejected ");
				}
				public void talking() {
					System.out.println("[Dialer] Event talking ");
				}
				public void statusChanged(int status) {
					System.out.println("[Dialer] Event statusChanged: " + status);
				}
			});

			// if (phoneNumber != null) {
			// System.out.println("[Network] Dialing phone number ...");
			// dialer.dial(phoneNumber);
			// }
			// System.out.println("[Network] Hang up ...");
			// dialer.hangUp("HangUp");

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
