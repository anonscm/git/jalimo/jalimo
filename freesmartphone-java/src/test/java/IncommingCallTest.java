import org.freesmartphone.Phone;
import org.freesmartphone.PhoneKit;
import org.freesmartphone.PhoneListener;
import org.freesmartphone.phone.Call;


public class IncommingCallTest implements PhoneListener {

	/**
	 * @param args
	 * @throws Exception 
	 */
	public static void main(String[] args) throws Exception {	
		new IncommingCallTest();
		while (true) {
			Thread.sleep(50);
		}
	}
	
	public IncommingCallTest() throws Exception {
		PhoneKit kit = new PhoneKit();
		Phone phone = kit.getPhoneInstance();
		phone.addPhoneListener(this);
		System.out.println("listening");
	}

	public void PhoneIncoming(Call call) {
		System.out.println("incomming call");
		try {
			System.out.println("peer: "+call.GetPeer());
			System.out.println("activate now: "+call.Activate());			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
