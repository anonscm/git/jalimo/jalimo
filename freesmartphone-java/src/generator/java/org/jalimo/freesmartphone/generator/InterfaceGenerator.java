package org.jalimo.freesmartphone.generator;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Iterator;

public class InterfaceGenerator {
	String outputFolder;

	public InterfaceGenerator(String outputFolder) {
		this.outputFolder = outputFolder;
	}

	
	public void writeListenerInterfaceFile(Interface inf) throws IOException { 
		if (inf.signals.size() == 0)
			return;
			
		File targetDirectory = new File(outputFolder, inf.packageName.replace('.', File.separatorChar));
		targetDirectory.mkdirs();
		File file = new File(targetDirectory, inf.className+"Listener.java");
		FileWriter writer = new FileWriter(file);
		System.out.println("writing listener interface: "+file);

		writer.write("package "+inf.packageName+";\n\n\n");
			
		writer.write("public interface "+inf.className+"Listener {\n\n");

		
		for (Signal s : inf.signals) {
			writer.write("    public void "+inf.className+s.name+"(");
			int i = 0;
			for (Parameter param : s.parameters) {
				writer.write(param.getJavaType()+" "+param.name);
				if (++i < s.parameters.size())
					writer.write(", ");				
			}
			writer.write(");\n\n");
		}

		writer.write("}\n");
		writer.close();
	}
	
	public void writeInterfaceFile(Interface inf) throws IOException { 
		File targetDirectory = new File(outputFolder, inf.packageName.replace('.', File.separatorChar));
		targetDirectory.mkdirs();
		File file = new File(targetDirectory, inf.className+".java");
		FileWriter writer = new FileWriter(file);
		System.out.println("writing interface: "+file);

		writer.write("package "+inf.packageName+";\n\n\n");			
		writer.write("import "+inf.getFullJavaName()+";\n");			
		writer.write("public interface "+inf.className+" {\n\n");

		for (Method m : inf.methods) {
			writeMethodDeclaration(writer, m);
		}
		
		// add/remove Listener methods		
		if (inf.signals.size() > 0) {
			writer.write("    public void add"+inf.className+"Listener("+inf.getAssociatedListenerName()+" newListener);\n");
			writer.write("    public void remove"+inf.className+"Listener("+inf.getAssociatedListenerName()+" removeListener);\n");
		}

		writer.write("}\n");
		writer.close();
	}

	public void writeMethodDeclaration(FileWriter writer, Method m) throws IOException {
		writer.write("    public ");
		if (m.outParameter != null)
			writer.write(m.outParameter.getJavaType()+" ");
		else
			writer.write("void ");
		writer.write(m.name+"(");
		Iterator<Parameter> it = m.inParameters.iterator();
		while (it.hasNext()) {
			Parameter param = it.next();
			writer.write(param.getJavaType()+" "+param.name);
			if (it.hasNext())
				writer.write(", ");
		}
		writer.write(") throws Exception;\n\n");
	}

}
