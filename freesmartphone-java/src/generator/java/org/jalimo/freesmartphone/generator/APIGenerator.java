package org.jalimo.freesmartphone.generator;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import javax.xml.stream.FactoryConfigurationError;
import javax.xml.stream.XMLStreamException;

public class APIGenerator {

	String outputFolder;
	
	public static void main(String[] args) throws IOException, XMLStreamException, FactoryConfigurationError {
		if (args.length == 0 || "--help".equals(args[0])) {
			System.out.println("usage: APIGenerator outputFolder SPECFILE...");
			System.exit(0);
		}


		String outputFolder = args[0];

		// parse the interfaces
		List<Interface> allInterfaces = new LinkedList<Interface>();		
		APIParser parser = new APIParser();
		for (int i=1; i<args.length; i++) {
			allInterfaces.addAll(parser.parse(args[i]));
		}
		
		// generate the files
		InterfaceGenerator ifGenerator = new InterfaceGenerator(outputFolder); 
		ImplementationGenerator implGenerator = new ImplementationGenerator(outputFolder); 

		for (Interface inf : allInterfaces) {
			System.out.println("\nhandling interface: "+inf.name);
			ifGenerator.writeInterfaceFile(inf);
			ifGenerator.writeListenerInterfaceFile(inf);
			implGenerator.writeDBusInterfaceFile(inf);
			implGenerator.writeImplementationFile(inf);
		}
		
		// generate a kit file
		new KitWriter(outputFolder).write(allInterfaces);

	}

	public APIGenerator(String outputFolder) {
		this.outputFolder = outputFolder;
	}
}
