/**
 * 
 */
package org.jalimo.freesmartphone.generator;

import java.util.LinkedList;

public class Interface {
	String name;
	String className;
	String packageName;
	String description;
	LinkedList<Method> methods = new LinkedList<Method>();
	LinkedList<Signal> signals = new LinkedList<Signal>();
	
	String getAssociatedListenerName() {
		return packageName+"."+className+"Listener";
	}
	
	String getFullJavaName() {
		return packageName + "." + className;
	}
	
	String getImplPackageName() {
		return packageName.replaceAll("org.freesmartphone", "org.freesmartphone.impl");		
	}
	
	String getFullJavaImplName() {
		return getImplPackageName() + "." + className;
	}
}