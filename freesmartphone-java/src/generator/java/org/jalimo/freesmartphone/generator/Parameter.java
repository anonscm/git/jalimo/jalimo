/**
 * 
 */
package org.jalimo.freesmartphone.generator;

public class Parameter {
	String name;
	String type;
	boolean in = true;
	String description;
			
	public Parameter() {
	}

	public Parameter(String name, String type) {
		this.name = name;
		this.type = type;
	}
	
	public String getJavaType() {
		if ("s".equals(type)) {
			return "String";
		} else if ("i".equals(type)) {
			return "int";
		} else if ("b".equals(type)) {
			return "boolean";
		} else if ("as".equals(type)) {
			return "String[]";
		} 
		// TODO: find a solution for this hack
		else if ("o".equals(type) && "call".equals(name)) {
			return "org.freesmartphone.phone.Call";
		} else {
			return "UNKNOWN_TYPE:"+type;
		}
	}
	
	public String getTypeImpl() {
		if ("o".equals(type) && "call".equals(name))
			return "org.freesmartphone.impl.phone.CallImpl";
		return "UNKNOWN_TYPE_IMPL";
	}
}