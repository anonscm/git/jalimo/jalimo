/**
 * 
 */
package org.jalimo.freesmartphone.generator;

import java.util.LinkedList;

public class Method {
	String name;
	String description;		
	LinkedList<Parameter> inParameters = new LinkedList<Parameter>();
	Parameter outParameter;
}