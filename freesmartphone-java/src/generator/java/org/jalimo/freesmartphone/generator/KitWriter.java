package org.jalimo.freesmartphone.generator;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;


public class KitWriter {

	private String outputFolder;

	public KitWriter(String outputFolder) {
		this.outputFolder = outputFolder;
	}

	public void write(List<Interface> interfaces) throws IOException {
		File targetDirectory = new File(new File(outputFolder, "org"), "freesmartphone");
		targetDirectory.mkdirs();
		File file = new File(targetDirectory, "PhoneKit.java");
		FileWriter writer = new FileWriter(file);
		System.out.println("\nwriting PhoneKit: "+file);

		writer.write("package org.freesmartphone;\n\n\n");			
		writer.write("import org.freedesktop.dbus.DBusConnection;\n");			
		writer.write("import org.freedesktop.dbus.DBusSignal;\n");			
		writer.write("import org.freedesktop.dbus.DBusInterface;\n");			
		writer.write("import org.freedesktop.dbus.exceptions.DBusException;\n\n\n");			
		writer.write("public class PhoneKit {\n\n");
		
		writer.write("    private String serviceToConnect = \"org.freedesktop.Gypsy\";\n\n");
		writer.write("    private DBusConnection connection;\n\n");

		// instance variable for each interface
		for (Interface inf : interfaces) {
			writer.write("    private "+inf.getFullJavaName()+" "+ucFirst(inf.className)+"Instance;\n");
		}
		writer.write("\n");
		
		// constructor
		writer.write("    public PhoneKit() throws DBusException {\n");
		writer.write("        connection = DBusConnection.getConnection(DBusConnection.SYSTEM);\n");
		writer.write("    }\n\n");

		// constructor (String serviceToConnect)
		writer.write("    public PhoneKit(String serviceToConnect) throws DBusException {\n");
		writer.write("        connection = DBusConnection.getConnection(DBusConnection.SYSTEM);\n");
		writer.write("        this.serviceToConnect = serviceToConnect;\n");
			writer.write("    }\n\n");

		// close method
		writer.write("    public void close() {\n");
		writer.write("        connection.disconnect();\n");
		writer.write("    }\n\n");
		
		// factory methods
		for (Interface inf : interfaces) {
			writer.write("    public "+inf.getFullJavaName()+" get"+inf.className+"Instance() throws Exception {\n");
			writer.write("        if ("+ucFirst(inf.className)+"Instance == null)\n");
			writer.write("            "+ucFirst(inf.className)+"Instance = new "+inf.getFullJavaImplName()+"Impl(connection, serviceToConnect, \"/"+inf.name.replace('.', '/')+"\");\n");
			writer.write("        return "+ucFirst(inf.className)+"Instance;\n");
			writer.write("    }\n\n");
		}

		writer.write("}\n");
		
		writer.close();		
	}
	
	private String ucFirst(String s) {
		if (s == null)
			return null;
		if (s.length() == 0)
			return s;
		if (s.length() == 1)
			return s.toLowerCase();
		return s.substring(0, 1).toLowerCase() + s.substring(1);
	}

}
