package org.jalimo.freesmartphone.generator;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Iterator;

public class ImplementationGenerator {
	String outputFolder;
	
	public ImplementationGenerator(String outputFolder) {
		this.outputFolder = outputFolder;
	}
		
	
	public void writeImplementationFile(Interface inf) throws IOException { 

		File targetDirectory = new File(outputFolder, inf.getImplPackageName().replace('.', File.separatorChar));
		targetDirectory.mkdirs();
		File file = new File(targetDirectory, inf.className+"Impl.java");
		FileWriter writer = new FileWriter(file);
		System.out.println("writing implementation: "+file);

		writer.write("package "+inf.getImplPackageName()+";\n\n\n");
		writer.write("import java.util.LinkedList;\n");					
		writer.write("import org.freedesktop.dbus.DBusInterface;\n");
		writer.write("import org.freedesktop.dbus.DBusSignal;\n");
		writer.write("import org.freedesktop.dbus.DBusSignalHelper;\n");
		writer.write("import org.freedesktop.dbus.DBusConnection;\n");		
		writer.write("import org.freedesktop.dbus.exceptions.DBusException;\n");		
		writer.write("import org.freedesktop.dbus.DBusSigHandler;\n\n\n");		
		writer.write("public class "+inf.className+"Impl implements "+inf.getFullJavaName()+", DBusSigHandler {\n\n");

		writer.write("    private "+inf.className+" "+inf.className.toLowerCase()+";\n");
		writer.write("    private DBusConnection conn;\n");
		writer.write("    private String serviceToConnect;\n");
		
		if (inf.signals.size() > 0) {
			writer.write("    private LinkedList<"+inf.getAssociatedListenerName()+"> listener = new LinkedList<"+inf.getAssociatedListenerName()+">();\n\n");
		}

		// the constructor
		writer.write("    public "+inf.className+"Impl(DBusConnection conn, String serviceToConnect, String objectPath) throws DBusException {\n");
		writer.write("        this.conn = conn;\n");
		writer.write("        this.serviceToConnect = serviceToConnect;\n");
		writer.write("        "+inf.className.toLowerCase()+" = conn.getRemoteObject(serviceToConnect, objectPath, "+inf.className+".class);\n\n");		
		for (Signal s : inf.signals) {			
			writer.write("        DBusSignalHelper.addInterfaceMap(\""+inf.getFullJavaImplName()+"\", \""+inf.name+"\");\n");
			writer.write("        conn.addSigHandler("+inf.getFullJavaImplName()+"."+s.name+".class, this);\n");
		}
		writer.write("    }\n\n");
				
		// method delegatees 
		writeMethodDelegatees(inf, writer);


		// add/remove Listener methods
		if (inf.signals.size() > 0) {
			writer.write("    public void add"+inf.className+"Listener("+inf.getAssociatedListenerName()+" newListener) {\n");
			writer.write("        listener.add(newListener);\n");			
			writer.write("    }\n\n");

			writer.write("    public void remove"+inf.className+"Listener("+inf.getAssociatedListenerName()+" removeListener) {\n");
			writer.write("        listener.remove(removeListener);\n");			
			writer.write("    }\n\n");
		}
		
		//dbus event handler
		if (inf.signals.size() > 0) {
			writer.write("    public void handle(DBusSignal s) {\n");
			for (Signal s : inf.signals) {
				writer.write("        if (s instanceof "+inf.className+"."+s.name+") {\n");
				writer.write("            fire"+inf.className+s.name+"(("+inf.className+"."+s.name+") s);\n");
				writer.write("        }\n");
			}
			writer.write("    }\n\n");
		
		    // fire code
			for (Signal s : inf.signals) {
				writer.write("    private void fire"+inf.className+s.name+"("+inf.className+"."+s.name+" state) {\n");
				writer.write("        for ("+inf.getAssociatedListenerName()+" l : listener)\n"); 
				writer.write("            l."+inf.className+s.name+"(");
				int i = 0;
				for (Parameter param : s.parameters) {
					writer.write("state."+param.name);
					if (++i < s.parameters.size())
						writer.write(", ");				
				}
				writer.write(");\n\n");

				writer.write("    }\n\n");
			}
		}
		
		// is remote method
		writer.write("    public boolean isRemote() {\n");
		writer.write("        return false;");
		writer.write("    }");

		writer.write("}\n");
		
		writer.close();
	}


	private void writeMethodDelegatees(Interface inf, FileWriter writer)
			throws IOException {
		for (Method m : inf.methods) {
						
			// writeMethodHead
			writer.write("    public ");
			if (m.outParameter != null)
				writer.write(m.outParameter.getJavaType()+" ");
			else
				writer.write("void ");
			writer.write(m.name+"(");
			Iterator<Parameter> it = m.inParameters.iterator();
			while (it.hasNext()) {
				Parameter param = it.next();
				writer.write(param.getJavaType()+" "+param.name);
				if (it.hasNext())
					writer.write(", ");
			}
			writer.write(") throws Exception {\n");
			
			writer.write("        ");
			if (m.outParameter != null) {
				if (m.outParameter.type.equals("o"))
					writer.write("org.freedesktop.dbus.Path path = ");
				else
					writer.write("return ");
			}
			writer.write(inf.className.toLowerCase()+"."+m.name+"(");
			Iterator<Parameter> it2 = m.inParameters.iterator();
			while (it2.hasNext()) {
				Parameter param = it2.next();
				writer.write(param.name);
				if (it2.hasNext())
					writer.write(", ");
			}
			writer.write(");\n");
			
			if (m.outParameter != null && m.outParameter.type.equals("o")) {
				writer.write("        return new "+m.outParameter.getTypeImpl()+"(conn, serviceToConnect, path.getPath());\n");
			}

			writer.write("    }\n\n");
		}
	}

	public void writeDBusInterfaceFile(Interface inf) throws IOException { 
		File targetDirectory = new File(outputFolder, inf.getImplPackageName().replace('.', File.separatorChar));
		targetDirectory.mkdirs();
		File file = new File(targetDirectory, inf.className+".java");
		FileWriter writer = new FileWriter(file);
		System.out.println("writing interface: "+file);

		writer.write("package "+inf.getImplPackageName()+";\n\n\n");			
		writer.write("import org.freedesktop.dbus.DBusInterface;\n");
		writer.write("import org.freedesktop.dbus.DBusInterfaceName;\n");
		writer.write("import org.freedesktop.dbus.DBusSignal;\n");			
		writer.write("import org.freedesktop.dbus.exceptions.DBusException;\n\n\n");
		writer.write("@DBusInterfaceName(\""+inf.name+"\")\n");
		writer.write("public interface "+inf.className+" extends DBusInterface {\n\n");

		for (Method m : inf.methods) {
			writeDBusMethodDeclaration(writer, m);
			writer.write(";\n\n");
		}

		for (Signal s : inf.signals) {
			writeSignalClass(writer, inf, s);
		}
		
		writer.write("}\n");
		writer.close();
	}
	
	// TODO: find a better solution than public static
	public static void writeDBusMethodDeclaration(FileWriter writer, Method m) throws IOException {
		writer.write("    public ");
		if (m.outParameter != null) {
			if (m.outParameter.type.equals("o"))
				writer.write("org.freedesktop.dbus.Path ");
			else
				writer.write(m.outParameter.getJavaType()+" ");			
		}
		else
			writer.write("void ");
		writer.write(m.name+"(");
		Iterator<Parameter> it = m.inParameters.iterator();
		while (it.hasNext()) {
			Parameter param = it.next();
			writer.write(param.getJavaType()+" "+param.name);
			if (it.hasNext())
				writer.write(", ");
		}
		writer.write(")");
	}

	private void writeSignalClass(FileWriter writer, Interface inf, Signal s) throws IOException {
		// create an internal class
		writer.write("    @DBusInterfaceName(\""+inf.name+"."+s.name+"\")\n");
		writer.write("    public class "+s.name+" extends DBusSignal {\n");
		
		writer.write("\n");
		
		// a field for every argument
		for (Parameter param : s.parameters) 
			writer.write("        public final "+param.getJavaType()+" "+param.name+";\n");

		writer.write("\n");

		// create a constructor
		writer.write("        public "+s.name+"(String path");
		for (Parameter param : s.parameters)
			writer.write(", "+param.getJavaType()+" "+param.name);
		writer.write(") throws DBusException {\n");
		
		// call the super constructor
		writer.write("            super(path");
		for (Parameter param : s.parameters)
			writer.write(", "+param.name);
		writer.write(");\n");

		// assign variables
		for (Parameter param : s.parameters) 
			writer.write("            this."+param.name+" = "+param.name+";\n");
		
		// close the constructor
		writer.write("        }\n");

		// close the internal class
		writer.write("    }\n\n");
	}
}
