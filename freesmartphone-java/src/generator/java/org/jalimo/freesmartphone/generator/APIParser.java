package org.jalimo.freesmartphone.generator;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import javax.xml.stream.FactoryConfigurationError;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

public class APIParser {

	public List<Interface> parse(String specFile) throws IOException, XMLStreamException, FactoryConfigurationError {
		XMLStreamReader parser =   XMLInputFactory.newInstance().createXMLStreamReader(new FileInputStream(specFile));
		List<Interface> interfaces = new LinkedList<Interface>();
		
		while (true) {
		    int event = parser.next();
		    if (event == XMLStreamConstants.END_DOCUMENT) {
		       parser.close();
		       break;
		    }
		    if (event == XMLStreamConstants.START_ELEMENT) {
		    	if ("interface".equals(parser.getLocalName())) {
		    		Interface interfaceNode = createInterfaceNode(parser);
		    		interfaces.add(interfaceNode);
		    	}
		    }
		}
		return interfaces;
	}

	private Interface createInterfaceNode(XMLStreamReader parser) throws IOException, XMLStreamException {
		Interface interfaceNode = new Interface();
		interfaceNode.name = parser.getAttributeValue(null, "name");
		int index = interfaceNode.name.lastIndexOf(".");
		interfaceNode.packageName = interfaceNode.name.substring(0, index).toLowerCase();
		interfaceNode.className = interfaceNode.name.substring(index+1);

		while (true) {
		    int event = parser.next();
		    if (event == XMLStreamConstants.START_ELEMENT) {
		    	if ("method".equals(parser.getLocalName())) {
		    		Method m = createMethodNode(parser);
		    		interfaceNode.methods.add(m);
		    	} 
		    	else if ("signal".equals(parser.getLocalName())) {
		    		Signal s = createSignalNode(parser);
		    		interfaceNode.signals.add(s);
		    	}
		    }
		    if (event == XMLStreamConstants.END_ELEMENT && "interface".equals(parser.getLocalName())) {
		    	break;
		    }
		}			

		return interfaceNode;
	}
			
	private Signal createSignalNode(XMLStreamReader parser) throws XMLStreamException {
		Signal s = new Signal();
		s.name = parser.getAttributeValue(null, "name");
		
		while (true) {
		    int event = parser.next();
		    
		    if (event == XMLStreamConstants.START_ELEMENT) {
		    	if ("arg".equals(parser.getLocalName())) {
		    		Parameter param = new Parameter();
		    		param.name = parser.getAttributeValue(null, "name");
		    		param.type = parser.getAttributeValue(null, "type");
		    		s.parameters.add(param);
		    	}
		    }
		    if (event == XMLStreamConstants.END_ELEMENT && "signal".equals(parser.getLocalName())) {
		    	break;
		    }
		}		

		return s;
	}

	private Method createMethodNode(XMLStreamReader parser) throws XMLStreamException {
		Method m = new Method();
		m.name = parser.getAttributeValue(null, "name");
		
		while (true) {
		    int event = parser.next();
		    
		    if (event == XMLStreamConstants.START_ELEMENT) {
		    	if ("arg".equals(parser.getLocalName())) {
		    		Parameter param = new Parameter();
		    		param.name = parser.getAttributeValue(null, "name");
		    		param.type = parser.getAttributeValue(null, "type");
		    		param.in = "in".equals(parser.getAttributeValue(null, "direction"));
		    		if (param.in)
		    			m.inParameters.add(param);
		    		else
		    			m.outParameter = param;
		    	}
		    }
		    if (event == XMLStreamConstants.END_ELEMENT && "method".equals(parser.getLocalName())) {
		    	break;
		    }
		}
		return m;
	}

}
