/**
 * 
 */
package org.jalimo.freesmartphone.generator;

import java.util.LinkedList;

public class Signal {
	String name;
	String description;
	LinkedList<Parameter> parameters = new LinkedList<Parameter>();		
}