#!/bin/bash

# Usage:
# ./createInterfaces.sh /path/to/xml/files/*.xml

JAVA=java
VERSION=2.5
JARPATH=.

CREATE_CMD="$JAVA -DPid=$$ -DVersion=$VERSION -cp $JARPATH/libdbus-java-2.5.jar:$JARPATH/dbus-java-bin-2.5.jar org.freedesktop.dbus.bin.CreateInterface --create-files"

for xmlfile in $@; do
  echo "Creating interface for: $xmlfile"
  $CREATE_CMD $xmlfile || exit 1
done

